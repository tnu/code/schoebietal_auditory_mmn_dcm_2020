#! /bin/bash

set -e

# Load the matlab module
module load matlab

# Specify configuration
DESIGN=MMN01

# IDs for the preprocessing
IDS="27905 27907 27908 27909 27985 27986 27987 27988 27989 27990"

# Create path to logfiles
LOGPATH="log_ratmpi_master_raw2bma_"$DESIGN"_m01_16"
mkdir logs/$LOGPATH 

# Run the preprocessing. Note: Preprocessing is not hemisphere specific, and ID_LHS contains all subjects that are also contained in ID_RHS
for ID in $IDS
do
	bsub -W 120 -J "job_preproc_$ID" -o logs/$LOPATH/"$LOGPATH"_"$ID" matlab -nodisplay -singleCompThread -r "ratmpi_revision_preproc('$ID', '$DESIGN')"
done

# Run the diagnostics of the preprocessing
bsub -W 60 -w 'ended("job_preproc*")' -o logs/$LOPATH/"$LOGPATH" matlab -nodisplay -singleCompThread -r "ratmpi_revision_preproc_diagnostics('$DESIGN')"

# Export the ERPs from the group to make them available for statistics in R
bsub -W 60 -w 'ended("job_preproc*")' -o logs/$LOGPATH/"$LOGPATH" matlab -nodisplay -singleCompThread -r "ratmpi_revision_data_export4R('$DESIGN')"
