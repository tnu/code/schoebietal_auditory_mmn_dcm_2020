#! /bin/bash

set -e

# Load the matlab module
module load matlab

# Specify configuration
DESIGN=MMN01

# IDs for the preprocessing
IDS="27905 27907 27908 27909 27985 27986 27987 27988 27989 27990"

# Create path to logfiles
LOGPATH="log_ratmpi_master_raw2bma_"$DESIGN"_m01_16"
mkdir logs/$LOGPATH 

# Run the preprocessing. Note: Preprocessing is not hemisphere specific, and ID_LHS contains all subjects that are also contained in ID_RHS
for ID in $IDS
do
	bsub -W 120 -J "job_preproc_$ID" -o logs/$LOPATH/"$LOGPATH"_"$ID" matlab -nodisplay -singleCompThread -r "ratmpi_revision_preproc('$ID', '$DESIGN')"
done

# Run the diagnostics of the preprocessing
bsub -W 60 -w 'ended("job_preproc*")' -o logs/$LOPATH/"$LOGPATH" matlab -nodisplay -singleCompThread -r "ratmpi_revision_preproc_diagnostics('$DESIGN')"

# Export the ERPs from the group to make them available for statistics in R
bsub -W 60 -w 'ended("job_preproc*")' -o logs/$LOGPATH/"$LOGPATH" matlab -nodisplay -singleCompThread -r "ratmpi_revision_data_export4R('$DESIGN')"

# Create a pause
sleep 5

# IDs for the inversions, BMA and statistics
ID_LHS="27905 27907 27908 27909 27985 27986 27987 27989 27990"
ID_RHS="27905 27907 27908 27986 27987 27989 27990"

# Run the DCM inversion for lhs
HEMI=lhs

for ID in $ID_LHS
do
	for model in {01..16}
	do 
		bsub -W 400 -J "job_dcm_invert_lhs_$ID" -w 'ended("job_preproc")' -o logs/$LOGPATH/"$LOGPATH"_"$ID"_"$model" matlab -nodisplay -singleCompThread -r "ratmpi_revision_dcm_invert_BestAndDefault('$ID', '$DESIGN', '$HEMI', '$model')"
	sleep 1
	done
done

# Create a pause
sleep 5

# Compute the BMA for lhs
bsub -W 60 -J "job_bma_lhs" -w 'ended("job_dcm_invert_lhs*")' -o logs/$LOGPATH/"$LOGPATH"_bma matlab -nodisplay -singleCompThread -r "ratmpi_revision_bma('$DESIGN', '$HEMI')" 

# Run the DCM inversion for rhs
HEMI=rhs

for ID in $ID_RHS 
do
	for model in {01..16}
	do 
		bsub -W 400 -J "job_dcm_invert_rhs_$ID" -w 'ended("job_preproc")' -o logs/$LOGPATH/"$LOGPATH"_"$ID"_"$model" matlab -nodisplay -singleCompThread -r "ratmpi_revision_dcm_invert_BestAndDefault('$ID', '$DESIGN', '$HEMI', '$model')"
		sleep 1
	done
done

# Create a pause
sleep 5

# Compute the BMA for rhs
bsub -W 60 -J "job_bma_rhs" -w 'ended("job_dcm_invert_rhs*")' -o logs/$LOGPATH/"$LOGPATH"_bma matlab -nodisplay -singleCompThread -r "ratmpi_revision_bma('$DESIGN', '$HEMI')" 

sleep 5

# Export the BMAs to make them available for statistics in R
bsub -W 60 -w 'ended("job_bma_*")' -o logs/$LOGPATH/"$LOGPATH"_bma matlab -nodisplay -singleCompThread -r "ratmpi_revision_theta_export4R('$DESIGN')" 
