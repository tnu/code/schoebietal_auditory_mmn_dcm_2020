function [ details ] = ratmpi_subjects( id, mid, options )
% [ details ] = ratmpi_subjects(hemi, options)
%
% Returns all filepaths and filenames.
%
% IN
%   id           string           subject identifier
%   mid          string/struct    model identifier (or [])
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   details      struct           
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 3
    options = ratmpi_set_global_options();    
end

[mid, vbsv, s] = ratmpi_unwrap_input(mid, options); 

details.name.id         = ['RATMPI_' id];
details.name.triggers   = [details.name.id '_events_' options.preproc.dataset '.mat'];
details.name.D          = ['Dnew_' details.name.id '_' options.preproc.dataset '.mat'];
details.name.fD         = ['f' details.name.D];
details.name.dfD        = ['df', details.name.D]; 
details.name.fdfD       = ['fdf', details.name.D]; 
details.name.efdfD      = ['efdf', details.name.D];
details.name.cefdfD     = ['cefdf', details.name.D];
details.name.acefdfD    = ['acefdf', details.name.D];
details.name.racefdfD   = ['racefdf', details.name.D];
details.name.macefdfD   = ['macefdf', details.name.D];
details.name.efD        = ['e' details.name.fD];
details.name.cefD       = ['cefDnew_' details.name.id];
details.name.acefD      = ['a' details.name.cefD];
details.name.macefD     = ['m' details.name.acefD];
details.name.cmfD       = [details.name.id '_' options.hemisphere '_' details.name.fD];

details.name.class      = [details.name.id '_' options.class.identifier];

% Preprocessing
details.path.sbjroot    = [options.datadir '/' id];
details.path.rawroot    = [options.rawdir '/' id];
details.path.data       = [details.path.rawroot '/' options.pharma '/'];
details.path.preproot   = [details.path.sbjroot '/spm_prep/'];
details.path.prep       = [details.path.sbjroot '/spm_prep/' options.pharma '/' options.design '/'];

switch id
    case options.subjectIDs.placebo.all
        details.file.ASC        = dir([details.path.data options.dataset{1} '/*ASC']);
        details.file.dataset1   = [details.path.data options.dataset{1} '/' details.file.ASC.name];
        details.file.dataset2   = [details.path.data options.dataset{2} '/' details.file.ASC.name];
    case options.subjectIDs.pharma.all
        details.file.ASC        = dir([details.path.data options.dataset{1} '/*ASC']);
        details.file.dataset1   = [details.path.data options.dataset{1} '/' details.file.ASC.name];
        details.file.dataset2   = [details.path.data options.dataset{2} '/' details.file.ASC.name];
end

details.file.D          = fullfile(details.path.prep, details.name.D);
details.file.fD         = [details.path.prep details.name.fD];
details.file.dfD        = [details.path.prep details.name.dfD];
details.file.fdfD       = [details.path.prep details.name.fdfD];
details.file.efdfD      = [details.path.prep details.name.efdfD];
details.file.cefdfD     = [details.path.prep details.name.cefdfD];
details.file.acefdfD    = [details.path.prep details.name.acefdfD];
details.file.racefdfD    = [details.path.prep details.name.racefdfD];
details.file.macefdfD   = [details.path.prep details.name.macefdfD];
details.file.efD        = [details.path.prep details.name.efD];
details.file.cefD       = [details.path.prep details.name.cefD];
details.file.acefD      = [details.path.prep details.name.acefD];
details.file.macefD     = [details.path.prep details.name.macefD '.mat'];
details.file.prep       = [details.path.prep details.name.id '_' options.hemisphere '_preproc'];
details.file.manualGA   = [details.path.prep '/' details.name.id '_' options.pharma '_' options.hemisphere '_manual_grandAverage.mat'];

details.file.triggers   = [details.path.prep '/' details.name.triggers];

% DCM
details.path.dcmconfig  = [options.rootdir '/dcm_config/'];
details.path.dcm        = [details.path.sbjroot '/spm_dcm/' options.pharma '/' options.design '/'  options.dcm.identifier '/' options.hemisphere '/' s.mid '/'  ];
details.path.dcmroot    = [details.path.sbjroot '/spm_dcm/' options.pharma '/' options.design '/'  options.dcm.identifier '/' options.hemisphere '/' ];

details.file.dcm        = fullfile(details.path.dcm, [details.name.id '_' s.identifier '_' options.hemisphere '_' options.dcm.rid '.mat']);
details.file.multistartSummary = fullfile(details.path.dcm, [details.name.id '_' s.mid '_' options.hemisphere '_' options.dcm.rid '_multistartSummary.mat']);

switch options.dcm.optim.multistart
    case 'P01'
        details.config.vbsv     = fullfile(options.rootdir, 'dcm_config', 'vbsv', ['vbsv_' options.dcm.arch '_' options.dcm.optim.multistart], ['vbsv_' vbsv '.mat']);
    otherwise
        details.config.vbsv       = fullfile(options.rootdir, 'dcm_config', 'vbsv', ['vbsv_' options.dcm.arch '_' options.altnames.design '_' options.dcm.optim.multistart], ['vbsv_' vbsv '.mat']);
end

% Synthetic
% details.path.synthetic.root = fullfile(options.datadir, 'synthetic', id, ...
%     'data', options.pharma, options.design, options.dcm.identifier, ...
%     options.hemisphere);
% details.file.synthetic.root = fullfile(details.path.synthetic.root, ...
%     [details.name.id '_' options.hemisphere '_' options.dcm.rid '.mat']);

details.path.synthetic.root = fullfile(options.datadir, 'synthetic', id, ...
    'data');
details.file.synthetic.root = fullfile(details.path.synthetic.root, ...
    [details.name.id '.mat']);



details.path.synthetic.dcm = fullfile(options.datadir, 'synthetic', id, ...
    'spm_dcm', options.pharma, options.design, options.dcm.identifier, ...
    options.hemisphere, s.mid);
details.file.synthetic.dcm = fullfile(details.path.synthetic.dcm, ...
    [details.name.id '_' s.identifier '_' options.hemisphere '_' options.dcm.rid '.mat']);

% Stats
details.path.stats.root      = fullfile(details.path.sbjroot, 'stats', options.pharma, options.design, 'classical');
        details.path.stats.class = fullfile(details.path.sbjroot, 'stats', options.pharma, options.design, 'classification', details.name.class);

details.path.stats.lvl1.anova1.tone = fullfile(details.path.stats.root, 'anova1', 'tone'); 
details.path.stats.lvl1.anova2.tone_design = fullfile(details.path.sbjroot, 'stats', options.pharma, 'MMN_0.X', 'classical', 'anova2', 'tone_design'); 
details.path.stats.lvl1.anova2.tone_pharma = fullfile(details.path.sbjroot, 'stats', 'all', options.design, 'classical', 'anova2', 'tone_pharma'); 
details.path.stats.lvl1.ttest.mmn = fullfile(details.path.stats.root, 'ttest', 'mmn');
details.path.stats.lvl1.anova2.mmn_design = fullfile(details.path.sbjroot, 'stats', options.pharma, 'MMN_0.X', 'classical', 'anova2', 'mmn_design'); 

details.path.stats.lvl2.Ep = fullfile(details.path.sbjroot, 'stats', 'allPharma', options.design);

details.file.stats.lvl1.anova1.tone = fullfile(details.path.stats.lvl1.anova1.tone, 'stats.mat'); 
details.file.stats.lvl1.anova2.tone_design = fullfile(details.path.stats.lvl1.anova2.tone_design, 'stats.mat'); 
details.file.stats.lvl1.anova2.tone_pharma = fullfile(details.path.stats.lvl1.anova2.tone_pharma, 'stats.mat'); 
details.file.stats.lvl1.ttest.mmn = fullfile(details.path.stats.lvl1.ttest.mmn, 'stats.mat');
details.file.stats.lvl1.anova2.mmn_design = fullfile(details.path.stats.lvl1.anova2.mmn_design, 'stats.mat'); 

details.path.stats.lvl2.conj.anova1.tone = fullfile(details.path.stats.root, 'conj', 'anova1', 'tone'); 
details.path.stats.lvl2.conj.anova2.tone_design = fullfile(details.path.stats.root, 'conj', 'anova2', 'tone_design'); 
details.path.stats.lvl2.conj.ttest.mmn = fullfile(details.path.stats.root, 'conj', 'ttest', 'mmn'); 
details.path.stats.lvl2.conj.anova2.mmn_design = fullfile(details.path.stats.root, 'conj', 'anova2', 'mmn_design'); 
details.path.stats.lvl2.conj.anova2.tone_pharma = fullfile(details.path.sbjroot, 'stats', 'all', options.design,  'classical', 'anova2', 'tone_pharma'); 
details.path.stats.lvl2.mfx.tone_pharma_id = fullfile(details.path.sbjroot, 'stats', 'all', options.design,  'classical', 'mfx', 'tone_pharma_id'); 

details.file.stats.lvl2.conj.anova1.tone = fullfile(details.path.stats.lvl2.conj.anova1.tone, 'stats.mat'); 
details.file.stats.lvl2.conj.anova2.tone_design = fullfile(details.path.stats.lvl2.conj.anova2.tone_design, 'stats.mat'); 
details.file.stats.lvl2.conj.ttest.mmn = fullfile(details.path.stats.lvl2.conj.ttest.mmn, 'stats.mat'); 
details.file.stats.lvl2.conj.anova2.mmn_design = fullfile(details.path.stats.lvl2.conj.anova2.mmn_design, 'stats.mat'); 
details.file.stats.lvl2.conj.anova2.tone_pharma = fullfile(details.path.stats.lvl2.conj.anova2.tone_pharma, 'stats.mat'); 
details.file.stats.lvl2.mfx.tone_pharma_id = fullfile(details.path.stats.lvl2.mfx.tone_pharma_id, 'stats.mat');

details.file.stats.lvl2.Ep.mfx.linear = fullfile(details.path.stats.lvl2.Ep, ['group_pharma_2ndLevel_' options.altnames.design '_Ep_data_bma_' options.dcm.bms.sv '_mfx_linear.mat']);
details.file.stats.lvl2.Ep.mfx.anova = fullfile(details.path.stats.lvl2.Ep, ['group_pharma_2ndLevel_' options.altnames.design '_Ep_data_bma_' options.dcm.bms.sv '_mfx_anova.mat']);


details.file.class = fullfile(details.path.stats.class, ...
    [details.name.class '_' ...
    num2str(options.class.permTest) '_' ... 
    num2str(options.class.seed) '.mat']);

details.path.bms = fullfile(details.path.dcmroot, ['bms_' options.dcm.bms.identifier]);
details.file.bms = fullfile(details.path.dcmroot, ['bms_' options.dcm.bms.identifier], 'BMS.mat');

details.path.bma = fullfile(details.path.dcmroot, ['bma_' options.dcm.bms.identifier]);
details.file.bma = fullfile(details.path.dcmroot, ['bma_' options.dcm.bms.identifier], 'BMA.mat');

% Diagnostics
details.file.diagnostics.preproc = fullfile(details.path.prep, [details.name.id '_diagnostics_preprocReview.mat']);

% Figures
details.fig.diagnostics.plotArtefacts = fullfile(details.path.prep, [details.name.id '_plotArtefacts']);
details.fig.diagnostics.preproc = fullfile(details.path.prep, [details.name.id '_diagnostics_preprocReview']);

details.fig.pharma_erp = [details.path.preproot '/' details.name.id  '_' options.hemisphere '_' options.design '_erp'];
details.fig.pharma_standards = [details.path.preproot '/' details.name.id  '_' options.hemisphere '_' options.design '_standards'];

details.fig.dcm.vbsv.plotF_overModels = fullfile(details.path.dcmroot, [details.name.id '_' options.altnames.design '_' options.hemisphere '_vbsv_plotF_overModels']);

details.fig.stats.lvl1.anova1.tone = fullfile(details.path.stats.lvl1.anova1.tone, [details.name.id '_lvl1_anova1_tone']); 
details.fig.stats.lvl1.anova2.tone_design = fullfile(details.path.stats.lvl1.anova2.tone_design, [details.name.id '_lvl1_anova2_tone_design']); 
details.fig.stats.lvl1.ttest.mmn = fullfile(details.path.stats.lvl1.ttest.mmn, [details.name.id '_lvl1_anova1_ttest_mmn']);
details.fig.stats.lvl1.anova2.mmn_design = fullfile(details.path.stats.lvl1.anova2.mmn_design, [details.name.id '_lvl1_anova2_mmn_design']); 
details.fig.stats.lvl1.anova2.tone_pharma = fullfile(details.path.stats.lvl1.anova2.tone_pharma, [details.name.id '_lvl1_anova2_tone_pharma_' options.hemisphere]); 

details.fig.stats.lvl2.conj.anova1.tone = fullfile(details.path.stats.lvl2.conj.anova1.tone, [details.name.id '_lvl2_conj_anova1_tone']); 
details.fig.stats.lvl2.conj.anova2.tone_design = fullfile(details.path.stats.lvl2.conj.anova2.tone_design, [details.name.id '_lvl2_conj_anova2_tone_design']); 
details.fig.stats.lvl2.conj.ttest.mmn = fullfile(details.path.stats.lvl2.conj.ttest.mmn, [details.name.id '_lvl2_conj_ttest_mmn']); 
details.fig.stats.lvl2.conj.anova2.mmn_design = fullfile(details.path.stats.lvl2.conj.anova2.mmn_design, [details.name.id '_lvl2_conj_anova2_mmn_design']); 
details.fig.stats.lvl2.conj.anova2.tone_pharma = fullfile(details.path.stats.lvl2.conj.anova2.tone_pharma, [details.name.id '_lvl2_conj_anova2_tone_pharma_' options.hemisphere]); 
details.fig.stats.lvl2.mfx.tone_pharma_id = fullfile(details.path.stats.lvl2.mfx.tone_pharma_id, [details.name.id '_lvl2_mfx_tone_pharma_id_' options.hemisphere]);

details.fig.class = fullfile(details.path.dcmroot, [details.name.class]);


% Logfiles
details.logs.ratmpi_preproc_grandAverage = fullfile(details.path.prep, [details.name.id '_logs_ratmpi_preproc_grandAverage_' options.hemisphere]);
details.logs.ratmpi_dcm_invert = fullfile(details.path.dcm, [details.name.id '_logs_' s.identifier '_' options.hemisphere '_' options.dcm.rid]);

end

