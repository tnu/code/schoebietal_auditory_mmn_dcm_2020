function [ s ] = ratmpi_class_permutationTest_summary(options)
% [ s ] = ratmpi_class_permutationTest_summary(options)
%
% Gathers the information from the single files and summarizes the results
% in a summary file.
%
% IN
%
% OPTIONAL
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% OUT
%   s            struct           Summary about parameters, accuracy and
%                                 confusion matriced of permutation test.
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 1
    options = ratmpi_set_global_options(); 
end

% Save the results
sf = 1;

gid = 'group_pharma';
options.pharma = 'allPharma';
% options.hemisphere = 'rhs'; % Files are stored in rhs by design

% Load paths and files
details = ratmpi_subjects(gid, '', options); 


% Create a list of all the files with permuted labels, and load the review
l = dir([details.path.stats.class, '/*.mat']); 

% Allocate space for permuation test
s = initialize_summary(l, details, options);

for i = 1 : length(l)
    
    if mod(i, 50) == 0
        disp(i); 
    end
    
    load([details.path.stats.class '/' l(i).name]); 
    
    [s.accuracy(i), s.confMat.train(:, i), s.confMat.pred(:, i), ...
        s.estim.BoxConstraint(:, i), s.estim.KernelScale(:, i), s.seed(i)] ...
        = return_estim_learner(stats); 
end

if sf
    save([details.path.stats.class '/' details.name.class '_summary.mat'], 's', 'l');
end
 
end

function s = initialize_summary(l, details, options)

load([details.path.stats.class '/' l(1).name]); 
nClasses = length(stats.review.pred.confMat.dat(:)); 

s = struct();
s.accuracy = zeros(1, length(l)); 
s.confMat.train = zeros(nClasses, length(l)); 
s.confMat.pred = zeros(nClasses, length(l)); 
s.estim.BoxConstraint = zeros(7, length(l)); 
s.estim.KernelScale = zeros(7, length(l));
s.seed = zeros(1, length(l)); 

end


function [acc, confMat_train, confMat_pred, estim_BoxConstraint, estim_KernelScale, rngseed] = ...
    return_estim_learner(stats)

for i = 1 : length(stats.inner)
    BoxConstraint(i) = stats.inner{i}.estim.learners.BoxConstraint; 
    KernelScale(i) = stats.inner{i}.estim.learners.KernelScale; 
end

acc = stats.review.accuracy; 
confMat_train = stats.review.train.confMat.dat(:); 
confMat_pred = stats.review.pred.confMat.dat(:); 
rngseed = stats.seed;

estim_BoxConstraint = BoxConstraint(:); 
estim_KernelScale = KernelScale(:);

end

