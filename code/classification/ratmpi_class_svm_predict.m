function [cvModel] = ratmpi_class_svm_predict(svmModel, X, Y, pars)
% [ cvModel ] = ratmpi_class_svm_predict(svmModel, X, Y, pars)
%
% Predicts the class labels for left out data X and Y, with a trained
% classifier svmModel
%
% IN
%   svmModel     object           Classification object
%   X            mat              Data Matrix
%   Y            cell             Label Vector
%   OPTIONAL:
%   pars         struct           Parameters of the svm, f.e. as returned
%                                 by ratmpi_class_opt_learner
%
% OUT
%   cvModel      type             Predicted labels for data in X
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 4
    pars = ratmpi_class_opt_learner(); 
end

cvModel = struct(); 
cvModel.Y = Y;

for i = 1 : size(X, 2)
   cvModel.kfoldPredict = predict(svmModel, X');    
end


end