function [ r ] = ratmpi_class_statistics(gid, sf, options)
% [ r ] = ratmpi_class_statistics(gid, sf, options)
%
% Returns a table from the classification and permuation test statistics
%
% IN
%   gid         string        PLACEHOLDER: HARDCODED as 'group_pharma'
%   sf          bool          PLACEHOLDER
%
% OPTIONAL
%   options      struct       Configuration structure as returned
%                             by ratmpi_set_global_options()
% OUT
%   r           struct        Summary of classification and permutation 
%                             test stats  
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 3
    options = ratmpi_set_global_options();
end

gid = 'group_pharma';
options.pharma = 'allPharma';

options.class.permTest = 0;
options.class.seed = 1;

% Load paths and files
details = ratmpi_subjects(gid, '', options);

% Load the summary
try
load([details.path.stats.class '/' details.name.class '_summary.mat'], 's', 'l');
perms = 1;
catch
    warning('No permutation statistics found')
    perms = 0; 
end

% Load the true permuation test
load([details.file.class], 'stats');

% Fill the struct with the statistics
r = struct();

% Confusion Matrices
r.confMat.train = stats.review.train.confMat.dat;
r.confMat.pred = stats.review.pred.confMat.dat;

% Balanced accuracy
r.accuracy = stats.review.accuracy;

% pValues
if perms
r.pValue = return_pValue(stats, s);
end

% Return the winning learners of the inner loop
r.learners = return_learners(stats);

end


function pValue = return_pValue(stats, s)

% Create the histogram of achieved accuracies when resampling. Remove the
% true value from the permations summary (only one value!!)
trueAccuracy = stats.review.accuracy; 
permAccuracies = s.accuracy; 
permAccuracies(find(permAccuracies == trueAccuracy, 1)) = [];

% Report the p-Value and plot the achieved accuracy
pValue = sum(permAccuracies >= trueAccuracy) / length(permAccuracies);

end

function learners = return_learners(stats)

learners = [];
for i = 1 : length(stats.inner)
   learners = [learners, struct2cell(stats.inner{i}.estim.learners)];   
end

end




