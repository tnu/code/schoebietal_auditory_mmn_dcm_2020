function [X] = ratmpi_class_permute_labels(X, options)
% [ X ] = ratmpi_class_permute_labels(X, options)
%
% Permutes the pharmacological labels in for the classification to compute
% a permuation test.
%
% IN
%   X            struct           Design X as returned by
%                                 ratmpi_dcm_summaryMaster
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   X            struct           As input X, but with permuted pharma
%                                 labels
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 2
    options = ratmpi_set_global_options();
end

rng(options.class.seed);

% Only do it if one actually want to compute the permuation test
if options.class.permTest
    
    for idCell = ratmpi_unique(X.id)'
        for hemiCell = ratmpi_unique(X.hemi)'
            
            % Permute the labels only within subject and within hemisphere
            idx = strcmp(X.id, char(idCell)) & strcmp(X.hemi, char(hemiCell));
            
            % Permuation
            xx = X.pharma(idx);
            xx = xx(randperm(length(xx)));
            X.pharma(idx) = xx;
        end
    end
    
end