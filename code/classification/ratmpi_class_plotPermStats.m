function [pValue] = ratmpi_class_plotPermStats(stats, s)
% [pValue] = ratmpi_plotPermStats(stats, s)
%
% Plots the result from the permutation statistics
%
% IN
%   stats        struct         Structure as returned by ratmpi_class_loso
%   s            struct         Structure as returned by
%                               ratmpi_class_permutationTest_summary.m
%
% OUT
%   pValue       mat            pValue computed as the number of
%                               permutations with higher accuracy
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Create the histogram of achieved accuracies when resampling. Remove the
% true value from the permations summary (only one value!!)
trueAccuracy = stats.review.accuracy; 
permAccuracies = s.accuracy; 
permAccuracies(find(permAccuracies == trueAccuracy, 1)) = [];

[N, x] = hist(permAccuracies, 10); 
normN = N / trapz(x, N); 

create_barplot(x, normN);
config_barplot();

% % Plot the samples with higher accuracy than the test sample
idx = find(x >= trueAccuracy); 
config_pValues(idx);

% Report the p-Value and plot the achieved accuracy
pValue = sum(permAccuracies >= trueAccuracy) / length(permAccuracies); 
disp(pValue);

plot_pValues(trueAccuracy, pValue, normN)

config_figure();

end

function plot_pValues(trueAccuracy, pValue, normN)

plot([trueAccuracy, trueAccuracy], [0, max(normN)], '-r'); 
t = text(1, 2 * normN(end), ...
    sprintf([' Accuracy = %0.3f  \n p = %0.3f  '], trueAccuracy, pValue));
t.Color = [1 0 0];
t.FontWeight = 'bold';
t.HorizontalAlignment = 'right';

end

function config_pValues(idx)

h = findobj(gca, 'type', 'bar'); 

%Invert to get the right ordering
h = flipud(h);

for i = 1 : length(idx)
    h(idx(i)).FaceColor = [1 0 0]; 
end

end


function create_barplot(x, normN)

for i = 1 : length(x)
    bar(x(i), normN(i));
    hold on;
end

end

function config_barplot()

g = gca; 
h = findobj(g, 'type', 'bar'); 
set(h, 'FaceColor', [0, 0.447, 0.741]);
set(h, 'EdgeColor', 'none'); 

% Set the bardwith. Compute the range of x values, and spread the bars
% between
dx = abs(diff([h(1).XData, h(end).XData])); 
set(h, 'BarWidth', dx/(length(h) - 1));

end

function config_figure()

g = gca; 
g.TickDir = 'out'; 
g.XLim = [0, 1]; 
g.XGrid = 'on'; 
g.YGrid = 'on';

axis square
end