function [ optim ] = ratmpi_class_opt_space()
% [ optim ] = ratmpi_class_opt_space() 
%
% Returns the space over which the optimization is computed.
%
% IN
%
% OUT
%   optim       struct      Space over with to optimize
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Define optimization for linear learner
optim = struct();
optim.KernelFunction = {'linear'};
optim.BoxConstraint = 10.^[-3 : 1 : 3];
optim.KernelScale = 10.^[-3 : 1 : 3];

% Prior for the scale and Constraint Parameter
optim.prior = [1, 1]; 

end

