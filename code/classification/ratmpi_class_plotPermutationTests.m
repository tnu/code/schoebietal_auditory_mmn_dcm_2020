function [  ] = ratmpi_class_plotPermutationTests(gid, options)
% [  ] = ratmpi_class_plotPermutationTests(gid, options)
%
% Creates a comparison plot for the permutation tests of all classifications.
%
% IN
%   gid         struct              'group_pharma'
%
% OPTIONAL
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Load options
options = ratmpi_set_global_options();

% Prepare default settings
switch options.design
    case 'MMN_0.1'
        options.hemisphere = 'lhs';
        options.pharma = 'allPharma';
    case 'MMN_0.2'
        options.hemisphere = 'lhs';
        options.pharma = 'allPharma';
        
end
options = ratmpi_set_global_options(options);
              

% Create a placeholder options.class structure for the different
% classifications
popt{1} = struct();
popt{1}.class.drugs = 'all';
popt{1}.class.coding = 'dosage';
popt{2}.class.drugs = '2mgScopo_6mgPilo';
popt{2}.class.coding = 'dosage';
popt{3}.class.drugs = '2mgScopo_Vehicle';
popt{3}.class.coding = 'dosage';
popt{4}.class.drugs = 'Vehicle_6mgPilo';
popt{4}.class.coding = 'dosage';
popt{5}.class.drugs = 'anta_vs_ago';
popt{5}.class.coding = 'effect';

idx = 1;

colorCode = linspecer(20);
xRange = [0, 0.5; repmat([0, 1], 4, 1)];
    
for k = 1 : length(popt)
        
        
        % Important: Here we need to specify which BMA we are running in 'options'
        options.class.drugs = popt{k}.class.drugs;
        options.class.coding = popt{k}.class.coding;
        options = return_identifiers(options);
        
        % Return stats
        details = ratmpi_subjects(gid, '', options);
        load([details.path.stats.class '/' details.name.class '_summary.mat'], 's', 'l');

        % Plot 
        subplot(2, 3, k);
        plot_permstats(s, l, colorCode(k, :), xRange(k, :), popt{k}.class.drugs);
        
    end
    

end


function plot_permstats(s, l, colorCode, xRange, plotTitle)

% Plot histogram
h = histogram(s.accuracy, 'Normalization', 'pdf', 'NumBins', 20); 
set(h, 'FaceColor', 0.6 * ones(1, 3), 'EdgeColor', [1 1 1]);
hold on; 

% Plot gaussian fit to data
mu = mean(s.accuracy); 
sd = std(s.accuracy);
x = linspace(xRange(1), xRange(2), 1000);
pdfit = normpdf(x, mu, sd);
h = plot(x, pdfit);
set(h, 'Color', colorCode, 'LineWidth', 2);

% Plot the accuracy of the non-permuted labels
trueAccuracy = s.accuracy(1);
h = stem(trueAccuracy, 1 * max(pdfit));
set(h, 'Color', colorCode, 'LineStyle', ':', 'MarkerFaceColor', colorCode, 'LineWidth', 1)

% Config axes
config_axes(gca, plotTitle);

end


function config_axes(g, plotTitle)

g.PlotBoxAspectRatio = [1 1 1];
g.XGrid = 'on';
g.YGrid = 'on';
g.TickDir = 'out';
g.Title.String = plotTitle;
g.Title.Interpreter = 'none';
g.XTick = [0 : 0.2 : min(g.XLim(2), 1)];

end



function options =  return_identifiers(options)

switch options.dcm.bms.sv
    case 'best'
        suffix = '';
    case 'default'
        suffix = '_default';
end

options.dcm.bms.identifier = [options.dcm.bms.effects '_' ...
    options.dcm.bms.family '_' ...
    options.dcm.bms.bma suffix];

options.class.identifier  = [options.altnames.design '_' ...
    options.hemisphere '_class_linear_' ...
    options.class.hemi '_' options.class.pars '_' ...
    options.class.drugs '_' options.class.coding  suffix];

end