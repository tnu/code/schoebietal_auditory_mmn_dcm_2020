function [stats, optim] = ratmpi_class_optim(Ep, X, options)
% [ stats, optim ] = ratmpi_class_optim(Ep, X, options)
%
% Optimizes the hyperparameters of the leave-one-subject out classifier
% over the space defined by ratmpi_class_opt_learner, and returns optimized
% hyperparameters wrt to the classification accuracy.
%
% IN
%   Ep          mat              Matrix as returned by
%                                ratmpi_dcm_summaryMaster
%   X           struct           Struct as returned by
%                                ratmpi_dcm_summaryMaster
% OPTIONAL
%   options     struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()%
% OUT
%   stats       struct           Summary of the LOSO classification 
%   optim       struct           Summary of the parameter optimization
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set variable input
if nargin < 3
    options = ratmpi_set_global_options();
end

% Return the linear learner object
pars = ratmpi_class_opt_learner();

% Return optimization object and space
optim = ratmpi_class_opt_space();

parsCell = cell(1, length(optim.KernelFunction) * ...
    length(optim.BoxConstraint) * ...
    length(optim.KernelScale));

% Write input into the cell, such that we have a one dimensional object to
% loop over to use parfor
idx = 1;
for i = 1 : length(optim.KernelFunction)
    for j = 1 : length(optim.BoxConstraint)
        for k = 1 : length(optim.KernelScale)
            parsCell{idx} = {optim.KernelFunction{i}, optim.BoxConstraint(j),...
                optim.KernelScale(k)};
            idx = idx + 1;
        end
    end
end

stats = cell(size(parsCell));

% Gridsearch
tic
optim.estim.accuracy = 0;
optim.estim.complex = Inf;
for i = 1 : length(stats)   
    pars = struct(); 
    pars.learners.KernelFunction = parsCell{i}{1};
    pars.learners.BoxConstraint = parsCell{i}{2};
    pars.learners.KernelScale = parsCell{i}{3};
    
    pars = ratmpi_class_opt_learner(pars);
    
    stats{i} = ratmpi_class_loso(Ep, X, pars, options);
    stats{i}.pars = parsCell{i}; 

    [optim] = return_winnerTakesAll(i, stats{i}, optim);     
end

toc

end

function [optim] =  return_winnerTakesAll(i, stats, optim)

% Initialize new Best
newBest = 'no';

complex = norm(optim.prior - [stats.pars{2 : 3}]); 


% If current crossvalidation stats > previous -> keep
if optim.estim.accuracy < stats.review.accuracy
    optim.estim.accuracy = stats.review.accuracy; 
    optim.estim.complex = complex;
    optim.estim.learners.KernelFunction = stats.pars{1};
    optim.estim.learners.BoxConstraint = stats.pars{2};
    optim.estim.learners.KernelScale = stats.pars{3};
    optim.estim.stats = stats;
    newBest = 'yes';
    
% For equally good score, take the one closer to the prior
elseif optim.estim.accuracy == stats.review.accuracy
    if optim.estim.complex > complex
            optim.estim.accuracy = stats.review.accuracy; 
            optim.estim.complex = complex; 
            optim.estim.stats = stats;
            optim.estim.learners.KernelFunction = stats.pars{1};
            optim.estim.learners.BoxConstraint = stats.pars{2};
            optim.estim.learners.KernelScale = stats.pars{3};
            newBest = 'yes';
    end
end

optim.diagnostics.KernelFunction{i} = stats.pars{1}; 
optim.diagnostics.BoxConstraint(i) = stats.pars{2}; 
optim.diagnostics.KernelScale(i) = stats.pars{3}; 
optim.diagnostics.accuracy(i) = stats.review.accuracy;

print_line(i, stats, complex, newBest);
    
end

function print_line(i, stats, complex, newBest)


formatSpec = '%6s | %10s | %15s | %10s | %10s | %10s | %10s | \n';

if i == 1
    fprintf(formatSpec, 'Step', 'Kernel', 'BoxConstraint', 'Scale', 'Accuracy', 'Complexity', 'new best'); 
    fprintf('----------------------------------------------------------------------------\n')
end

fprintf(formatSpec, ...
    num2str(i), num2str(stats.pars{1}) , num2str(stats.pars{2}), ...
    num2str(stats.pars{3}), num2str(stats.review.accuracy), ...
    num2str(complex), newBest);

end


