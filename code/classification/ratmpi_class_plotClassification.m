function [ stats, s ] = ratmpi_class_plotClassification(gid, sf, options)
% [ stats, s ] = ratmpi_class_plotClassification(gid, sf, options)
%
% Loads the results from classification and permutation tests, and creates
% a figure containing the confusion matrices, a figure containing the
% permutation tests statistics, and a high dimensional tsne plot
%
% IN
%   gid         string          'group_pharma'
%   sf          bool            PLACEHOLDER. HAS NO EFFECT ATM.
%
% OPTIONAL
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% OUT
%   stats       struct          As used to create the figures
%   s           struct          As used to create the figures
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________
   

if nargin < 3
    options = ratmpi_set_global_options();
end

if ~strcmp(gid, 'group_pharma')
    error('This function only works with group_pharma')
end

options.pharma = 'allPharma';
switch options.design
    case 'MMN_0.1'
        options.hemisphere = 'rhs'; % Files are stored in rhs by design
    case 'MMN_0.2'
        options.hemisphere = 'lhs';
end

% Reload options
options = ratmpi_set_global_options(options);

options.class.permTest = 0;
options.class.seed = 1;

% Load paths and files
details = ratmpi_subjects(gid, '', options);

% Load the summary
load([details.path.stats.class '/' details.name.class '_summary.mat'], 's', 'l');

% Load the true permuation test
load([details.file.class], 'stats');

figure();
ratmpi_class_plotConfMat(stats);

figure();
s.pValue = ratmpi_class_plotPermStats(stats, s);

figure();
ratmpi_class_plotTsne(stats);




end

