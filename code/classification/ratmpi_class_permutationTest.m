function [ optim ] = ratmpi_class_permutationTest(options)
% [ optim ] = ratmpi_class_permutationTest 
%
% Computes the permutation test statistics from a relabeling on the class
% labels
%
% IN
%
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 2
    options = ratmpi_set_global_options(); 
end

gid = 'group_pharma';
options.pharma = 'allPharma';
options.hemisphere = 'rhs'; % Files are stored in rhs by design

% Load paths and files
details = ratmpi_subjects(gid, '', options); 

% Create a list of all the files with permuted labels, and load the review
load([details.path.stats.class '/' details.name.class '_summary.mat'], 's'); 

end

