function [] = ratmpi_class_plotConfMat(stats)
% [] = ratmpi_plot_class_confMat(stats)
%
% Plots the confusion matrix of a classification
%
% IN
%   stats        struct         Structure as returned by ratmpi_class_loso
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Plot Training review
% ------------------------------------------------------------------------
confMat = stats.review.train.confMat.dat;
labels = stats.review.train.confMat.label;

subplot(1, 2, 1);
heatmap(confMat, labels, labels, 1, ...
    'Colormap','red', ...
    'ShowAllTicks',1,...
    'UseLogColorMap',true,...
    'Colorbar',true);
title('Training Performance');
axis square

% Plot Prediction review
% ------------------------------------------------------------------------
confMat = stats.review.pred.confMat.dat;
labels = stats.review.pred.confMat.label;

subplot(1, 2, 2);
heatmap(confMat, labels, labels, 1, ...
    'Colormap','red', ...
    'ShowAllTicks',1,...
    'UseLogColorMap',true,...
    'Colorbar',true);
title('Crossvalidation Performance');
axis square

end