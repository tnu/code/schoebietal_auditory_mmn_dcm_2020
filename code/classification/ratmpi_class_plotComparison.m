function [  ] = ratmpi_class_plotComparison(gid, options)
% [  ] = ratmpi_class_plotComparison(gid, options)
%
% Creates a comparison plot for all classifactions.
%
% IN
%   gid         struct              'group_pharma'
%
% OPTIONAL
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Load options
options = ratmpi_set_global_options();

% Create a placeholder options.class structure for the different
% classifications
popt{1} = struct();
popt{1}.class.drugs = 'all';
popt{1}.class.coding = 'dosage';
popt{2}.class.drugs = '2mgScopo_6mgPilo';
popt{2}.class.coding = 'dosage';
popt{3}.class.drugs = '2mgScopo_Vehicle';
popt{3}.class.coding = 'dosage';
popt{4}.class.drugs = 'Vehicle_6mgPilo';
popt{4}.class.coding = 'dosage';
popt{5}.class.drugs = 'anta_vs_ago';
popt{5}.class.coding = 'effect';

% Iterate over design and pharma and display statistics
opt = struct();

s = {};
idx = 1;

for designCell = {options.design}
    opt.design = char(designCell);
    
    switch opt.design
        case 'MMN_0.1'
            opt.priors = 'pX02';
            opt.hemisphere = 'lhs';
        case 'MMN_0.2'
            opt.priors = 'pX06';
            opt.hemisphere = 'lhs';
    end
    
    for k = 1 : length(popt)
        
        % Get adjusted options
        options = ratmpi_set_global_options(opt);
        
        % Important: Here we need to specify which BMA we are running in 'options'
        options.class.drugs = popt{k}.class.drugs;
        options.class.coding = popt{k}.class.coding;
        options = return_identifiers(options);
        
        % Return stats
        r = ratmpi_class_statistics('group_pharma', '', options);
        
        s{idx} = r;
        idx = idx + 1;
        
    end
    
    
    
end

plot_comparison(popt, s)

end


function plot_comparison(popt, s)

for i = 1 : size(s, 2)
   
    accuracy(i) = s{i}.accuracy; 
    
%     pValue(i) = s{i}.pValue; 
    xlabels{i} = popt{i}.class.drugs;
    
end

x = 1 : size(s, 2);
bar(x, accuracy, 'FaceColor', [0.3, 0.3, 0.3], 'EdgeColor', 'none');
g = gca; 
g.YLim = [0, 1];
g.YTick = [0 : 0.05 : 1];
g.XTickLabels = xlabels;
g.XTickLabelRotation = -45;
g.TickLabelInterpreter = 'none'; 
g.XGrid = 'on'; 
g.YGrid = 'on';
g.TickDir = 'out';

g = gcf; 
g.Color = [1 1 1]; 

axis square


end




function options =  return_identifiers(options)

switch options.dcm.bms.sv
    case 'best'
        suffix = '';
    case 'default'
        suffix = '_default';
end

options.dcm.bms.identifier = [options.dcm.bms.effects '_' ...
    options.dcm.bms.family '_' ...
    options.dcm.bms.bma suffix];

options.class.identifier  = [options.altnames.design '_' ...
    options.hemisphere '_class_linear_' ...
    options.class.hemi '_' options.class.pars '_' ...
    options.class.drugs '_' options.class.coding  suffix];

end