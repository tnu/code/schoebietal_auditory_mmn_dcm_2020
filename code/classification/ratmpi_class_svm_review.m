function [stats] = ratmpi_class_svm_review(stats)
% [ stats ] = ratmpi_class_svm_review(stats)
%
% Computes the overall classification performance for a classification
% stored in stats
%
% IN
%   stats        struct         Structure as returned by
%                               ratmpi_class_nloso.m
% 
%   OPTIONAL:
%
% OUT
%   stats        struct         stats with appended .review
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


svmModel = stats.svmModel;
cvModel = stats.cvModel;

% Gather training classification 
% -------------------------------------------------------------------------
[true_label, predicted_label] = return_predictions(svmModel); 

% Get training confusion matrix and labels
[confMat, labels] = confusionmat(true_label, predicted_label); 
stats.review.train.confMat.dat = confMat; 
stats.review.train.confMat.label = labels;

clear true_label predicted_label confMat labels

% Gather crossvalidated simulations and predictions
% -------------------------------------------------------------------------
[true_label, predicted_label] = return_predictions(cvModel); 

% Get prediction confusion matrix and labels
[confMat, labels] = confusionmat(true_label, predicted_label); 
stats.review.pred.confMat.dat = confMat; 
stats.review.pred.confMat.label = labels;

% Return classification accuracy
stats.review.accuracy = sum(diag(stats.review.pred.confMat.dat)) ./ ...
    sum(stats.review.pred.confMat.dat(:));

end


function [true_label, predicted_label] = return_predictions(xModel)

true_label = []; 
predicted_label = [];

for i = 1 : length(xModel)
    
    if isfield(xModel{i}, 'kfoldPredict')
        yp = xModel{i}.kfoldPredict;
    else
        yp = predict(xModel{i}, xModel{i}.X);
    end
    
    true_label = [true_label; xModel{i}.Y]; 
    predicted_label = [predicted_label; yp]; 
end

end