function [ pars ] = ratmpi_class_opt_learner(pars)
% [ pars ] = ratmpi_class_opt_learner(pars)
%
% Configuration of the learner of the classifier
%
% IN
% OPTIONAL 
%   pars       struct      Configuration of learner to overwrite defaults
% OUT
%   pars       struct      Configured learner.
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Define classification optimization prior
if nargin < 1
    pars = struct(); 
end

% Define default parameters which can be overwritten by input
default.learners.KernelFunction = 'linear';
default.learners.Standardize = true;
default.learners.KernelScale = 1;
default.learners.BoxConstraint = 1';

% Set default values, if input doesn't specify learner. 
if ~isfield(pars, 'learners')
    pars.learners = default.learners;
else
    % Iterate over all default fields, and set the default for undefined
    % fields in the input
    for fnameCell = fieldnames(default.learners)'
        if ~isfield(pars.learners, char(fnameCell))
            pars.learners.(char(fnameCell)) = default.learners.(char(fnameCell)); 
        end
    end
end

if isfield(pars.learners, 'template')
    % Remove old learner template
    pars.learners = rmfield(pars.learners, 'template');
end

    
% Classifier Function
pars.classifier = @fitcecoc;

% Unwrap Classifier Settings
sets = {}; 
for fnameCell = fieldnames(pars.learners)'
    fname = char(fnameCell); 
    sets = [sets, fname, pars.learners.(fname)];
end

pars.learners.template = templateSVM(sets{:});

end