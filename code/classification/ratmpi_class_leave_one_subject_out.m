function [tA, tB] = ratmpi_class_leave_one_subject_out(Ep, X, id)
% [ tA, tB ] = ratmpi_class_leave_one_subject_out(Ep, X, id)
%
% Reduces a dataset as returned by ratmpi_dcm_summaryMaster by one subject
% and returns the reduced dataset, and the left out.
% Used in the classification routines
%
% IN
%   Ep           mat            Parameter Matrix
%   X            struct         Structure containing .id, .pharma
%   id           id             subject identifier to be removed
%   OPTIONAL:
%
% OUT
%   tA           struct         Structure containing data for all subjects
%                               except id
%   tB           struct         Structure containing data only for subject id
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

idx = strcmp(X.id, id);

tA.Ep = Ep(:, ~idx);
tA.X.id = X.id(~idx);
tA.X.pharma = X.pharma(~idx);

tB.Ep = Ep(:, idx);
tB.X.id = X.id(idx);
tB.X.pharma = X.pharma(idx);

end