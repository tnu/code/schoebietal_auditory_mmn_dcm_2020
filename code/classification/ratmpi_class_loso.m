function [stats] = ratmpi_class_loso(Ep, X, pars, options)
% [ stats ] = ratmpi_class_loso(Ep, X, pars, options)
%
% Performs a leave one subject out (loso) classification, with the
% hyperparameters and learners specified in pars.
%
% IN
%   Ep          mat              Matrix as returned by
%                                ratmpi_dcm_summaryMaster
%   X           struct           Struct as returned by
%                                ratmpi_dcm_summaryMaster
% OPTIONAL
%   pars        struct           Learner and its hyperparameters
%
%   options      struct          Configuration structure as returned
%                                by ratmpi_set_global_options()
%
% OUT
%   stats       struct           Statistic structure containing info about
%                                trained SVMs, crossvalidation and review.
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set optional input
if nargin < 3
    pars = ratmpi_class_opt_learner();
    options = ratmpi_set_global_options(); 
elseif nargin < 4
    options = ratmpi_set_global_options(); 
end

% Allocate output structure
stats = struct(); 

% Get all ids
ids = ratmpi_unique(X.id)';

idx = 1;
for idCell = ids
    id = char(idCell);
    
    % Remove a test subjects datasets
    [tA, tB] = ratmpi_class_leave_one_subject_out(Ep, X, id);
    
    % Train the classifier on the training dataset
    svmModel = ratmpi_class_svm_train(tA.Ep, tA.X.pharma, pars);
    
    % Predict the test dataset
    cvModel = ratmpi_class_svm_predict(svmModel, tB.Ep, tB.X.pharma, pars);

    % Store the svm and classification results
    stats.data.train{idx} = [tA.X]; 
    stats.data.pred{idx} = [tB.X];
    stats.svmModel{idx} = svmModel;
    stats.cvModel{idx} = cvModel;

    idx = idx + 1;
end

    % Create a review structre in stats
    stats = ratmpi_class_svm_review(stats);

end