function [stats] = ratmpi_class_nloso(ids, sets, options)
% [ stats ] = ratmpi_class_nloso(ids, sets, options)
%
% Outer loop of the nested classification.
% Performs a nested leave one subject out (loso) classification.
% Here, the dataset is split into two parts in a loso fashion
%   set A: Training dataset
%   set B: Testing dataset
% The SVM is trained and the hyperparameters optimized to maximize
% crossvalidation within dataset A. Having selected the best weights and
% Hyperparameters that generalize within A, the classifier is tested on the
% fully left out set B. This is done for all subject, and the summary
% reported in stats.
%
% IN
%   ids         cell             Cell of subject IDs
%   sets        struct           Settings for loading the data as used by
%                                ratmpi_dcm_summaryMaster.m
% OPTIONAL
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats       struct           Statistic structure containing info about
%                                trained SVMs, crossvalidation and review.
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set optional input
if nargin < 3
    options = ratmpi_set_global_options();
end

% Allocate output structure
stats = struct();

sf = 1;


% ids = intersect(options.subjectIDs.pharma.lvl2.(options.altnames.design).lhs, ...
%     options.subjectIDs.pharma.lvl2.(options.altnames.design).rhs);

if isempty(sets)
    sets.fields = {'Ep', 'Cp'};
    sets.varargin = {'A', 'B', 'G', 'T'};
    sets.design = options.design;
end

switch options.class.drugs
    case '2mgScopo_6mgPilo'
        sets.pharma = {'2mgScopo',  '6mgPilo'};
    case 'all'
        sets.pharma = options.drugs;
    case 'anta_vs_ago'
        sets.pharma = {'2mgScopo', '1mgScopo', '3mgPilo', '6mgPilo'};
    case '2mgScopo_Vehicle'
        sets.pharma = {'2mgScopo', 'Vehicle'};
    case 'Vehicle_6mgPilo'
        sets.pharma = {'Vehicle', '6mgPilo'};
end

sets.coding = options.class.coding;

% Set parameters according to options configuration
switch options.class.hemi
    case 'unilat'
        sets.hemi = options.hemisphere;
    case 'bilat'
        sets.hemi = {'lhs', 'rhs'};
end

switch options.class.pars
    case 'bma'
        sets.mids = {''};
        sets.sv = 'bma';
    otherwise
        error('not defined for other settings');
end


% Run the summary Master to return all parameters of interest for all
% subjects, hemispheres and drugs
[s, X] = ratmpi_dcm_summaryMaster(ids, sets, options);

% Permute labels for the permuation test
[X] = ratmpi_class_permute_labels(X, options);

% Remove fixed parameters from the structure
[ Ep ] = prepare_pars(s);


if strcmp(sets.coding, 'effect')
    X.pharma = ratmpi_return_drugEffect(X.pharma);
    sets.pharma =  ratmpi_unique(ratmpi_return_drugEffect(sets.pharma));
end


idx = 1;
for idCell = ids
    id = char(idCell);
    
    % Remove a test subjects datasets
    [tA, tB] = ratmpi_class_leave_one_subject_out(Ep, X, id);
    
    % Train the classifier on the training dataset
    [~, optim] = ratmpi_class_optim(tA.Ep, tA.X, options);
    
    % Get the optimum hyperparameters and load the learner
    stats.inner{idx} = optim;
    pars = ratmpi_class_opt_learner(optim.estim);
    
    % Use the optimized parameters to train the model on the whole set tA
    svmModel = ratmpi_class_svm_train(tA.Ep, tA.X.pharma, pars);
    
    % Predict the test dataset
    cvModel = ratmpi_class_svm_predict(svmModel, tB.Ep, tB.X.pharma, pars);
    
    % Store the svm and classification results
    stats.data.train{idx} = [tA.X];
    stats.data.pred{idx} = [tB.X];
    stats.svmModel{idx} = svmModel;
    stats.cvModel{idx} = cvModel;
    
    idx = idx + 1;
end

% Create a review structre in stats
stats = ratmpi_class_svm_review(stats);
stats.sets = sets;
stats.ids = ids;
stats.permTest = options.class.permTest;
stats.seed = options.class.seed;

% Save the file
if sf
    options.pharma = 'allPharma';
    details = ratmpi_subjects('group_pharma', '', options);
    
    if ~exist(details.path.stats.class)
        mkdir(details.path.stats.class);
    end
    
    save(details.file.class, 'stats');
end

end


function Ep = prepare_pars(s)

Ep = s.Ep;

% Use the set of parameters: all - fixed_by_all
if isfield(s, 'pC')
    P = all((s.pC == 0)')';
else
    P = all((s.Cp == 0)')';
end

Ep(P, :) = [];

end

function [F, xx] = prepare_pars_F(s, X, sets)

% Reshape the Free energies
ids = ratmpi_unique(X.id);
mids = ratmpi_unique(X.mid);
drugs = ratmpi_unique(X.pharma);
hemis = ratmpi_unique(X.hemi);

F = [];
xx = struct('id', [], 'pharma', []);

for idCell = ids'
    id = char(idCell);
    
    for hemiCell = hemis'
        hemi = char(hemiCell);
        
        for pharmaCell = drugs'
            pharma = char(pharmaCell);
            
            idx = strcmp(X.id, id) & strcmp(X.hemi, hemi) & strcmp(X.pharma, pharma);
            
            F = [F; zscore(s.F(idx))];
            xx.id = [xx.id; {id}];
            xx.pharma = [xx.pharma; {pharma}];
            
        end
    end
end

F = F';

end
