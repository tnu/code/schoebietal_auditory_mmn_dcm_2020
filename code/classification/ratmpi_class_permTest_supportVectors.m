function [ beta, bb, p ] = ratmpi_class_permTest_supportVectors(sf, options)
% [ beta, bb, p ] = ratmpi_class_permTest_supportVectors(sf, options)
%
% Gathers the support vector weights over the permutation test, computes a
% null-distribution over weights, and tests the true weights against these
% null distributions
%
% IN
%   sf           bool           Save file
% OPTIONAL
%   options      struct         Configuration structure as returned
%                                 by ratmpi_set_global_options()
% OUT
%   beta         mat            Matrix of all weight of the permuted labels
%   bb           mat            Matrix of the weight of the true lables
%   p            mat            p-Value computed as the percentage of beta,
%                               with higher value than bb.
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 2
    options = ratmpi_set_global_options();
end

gid = 'group_pharma';
options.pharma = 'allPharma';
options.hemisphere = 'lhs'; % Files are stored in rhs by design

% Load paths and files
details = ratmpi_subjects(gid, '', options);

% Try to load the beta file, if it exists
l = dir([details.path.stats.class, '/*weights.mat']);

if ~isempty(l)
    load(fullfile(details.path.stats.class, l(1).name), 'beta');
else
    beta = load_beta(details);
    
    if sf 
        
        switch options.multistart
            case 'best'
                suffix = '_weights.mat'; 
            case 'default'
                suffix = '_default_weights.mat';
        end
        
        save(fullfile(details.path.stats.class, ...
            ['group_pharma_MMN01_lhs_class_linear_bilat_bma_' ...
            options.class.drugs '_' options.class.coding suffix]), 'beta'); 
    end
    
end

% Load the result from the true classification problem
l = dir([details.path.stats.class, '/*_0_1.mat']);
load(fullfile(details.path.stats.class, l(1).name));

% Create a figure with the histograms over betas
% for i = 1 : 18
%     subplot(3, 6, i)
%     histogram(beta(i, :), 20);
% end

% Load the weights from the true classification
bb = [];
for j = 1 : length(stats.svmModel)
    bb = [bb, stats.svmModel{j}.BinaryLearners{1}.Beta];
end

% Take the absolute values of the support vectors and sum
bb = sum(abs(bb), 2);

% Plot the weight from the true classification
% for i = 1 : 18
%     subplot(3, 6, i);
%     hold on;
%     
%     plot([bb(i), bb(i)], [0, 3000], '-r');
%     
% end

p = sum(beta > repmat(bb, 1, size(beta, 2)), 2) / size(beta, 2);

end

function beta = load_beta(details)

% Create a list of all the files with permuted labels
l = dir([details.path.stats.class, '/*_1_*.mat']);

beta = [];
for i = 1 : length(l)
    
    if mod(i, 50) == 0
        disp(i);
    end
    
    load([details.path.stats.class '/' l(i).name]);
        
    % Gather the betas
    bb = [];
    for i = 1 : length(stats.svmModel)
        
        % Square the weights and sum
        bb = [bb, stats.svmModel{i}.BinaryLearners{1}.Beta];
        
    end
    
    beta = [beta, sum(abs(bb), 2)];
    
end

end
