function [svmModel] = ratmpi_class_svm_train(X, Y, pars)
% [ svmModel ] = ratmpi_class_svm_train(X, Y, pars)
%
% Trains an svm classifier on with parameters specified by the input
%
% IN
%   X            mat              Data Matrix
%   Y            cell             Label Vector
%   OPTIONAL:
%   pars         struct           Parameters of the svm, f.e. as returned
%                                 by ratmpi_class_opt_learner
%
% OUT
%   svmModel     object          Trained Classification object
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 3
    pars = ratmpi_class_opt_learner(); 
end

classifier = pars.classifier;

svmModel = classifier(X', Y, 'Coding', 'binarycomplete',...
    'Learners', pars.learners.template); 

end