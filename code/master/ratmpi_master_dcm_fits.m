function [  ] = ratmpi_master_dcm_fits(masterSets)
% [  ] = ratmpi_master_dcm_fits(masterSets)
% 
% Master for preproc review: Plots the average model fit for model 16.
% Average computed as the average signal and average prediction over
% subjects. Shaded area depict Standard deviation over subjects
%  
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
% 
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Display title
endTime = ratmpi_master_print_header('Group Level: Average Fit for m16', 15, @seconds);

% Load options
options = ratmpi_set_global_options();

% Prepare sets
sets.fields = {'H', 'y'};
sets.mids = {'16'};
sets.design = options.design;
sets.pharma = options.drugs;
sets.hemi = {'lhs', 'rhs'}; %options.hemisphere;
sets.factor = 'pharma';

% Start timer
tic;

% Iterate over design and pharma and display statistics
opt = struct();
    
for designCell = masterSets.DESIGNS
    opt.design = char(designCell);
    
    switch opt.design
        case 'MMN_0.1'
            opt.priors = 'pX04'; 
        case 'MMN_0.2'
            opt.priors = 'pX06'; 
    end
    
    % Get adjusted options
    options = ratmpi_set_global_options(opt);
          
    % Display header
    ratmpi_master_print_section_header(...
        ['Plotting Fits for model ' char(sets.mids) '; ' ...
        'design: ' options.design], endTime);
    
    % Set variable settings
    sets.design = options.design;
    sets.pharma = options.drugs;
    
    % Get ids
    ids = options.subjectIDs.pharma.lvl2.(options.altnames.design).xhs;
    
    % Open a new figure and plot
    g = figure(); 
    ratmpi_plot_fitMaster(ids, sets, options);
    
    % Create information about figure
    g.Name = ['Model fits for model ' char(sets.mids) '; ' ...
        'Group: ' masterSets.gid '; ' ...
        'Design: ' options.design];
end

% End Timer
toc;

end