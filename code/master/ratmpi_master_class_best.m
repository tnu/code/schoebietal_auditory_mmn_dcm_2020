function [  ] = ratmpi_master_class_best(masterSets)
% [  ] = ratmpi_master_class_best(masterSets)
%
% Master for preproc review: Plots the classification results, and the
% results from the permutation test
%
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Display title
endTime = ratmpi_master_print_header('Display Results for BMA (best)', 10, @seconds);

% Load options
options = ratmpi_set_global_options();

% Create a placeholder options.class structure for the different
% classifications
popt{1} = struct();
popt{1}.class.drugs = 'all';
popt{1}.class.coding = 'dosage';
popt{2}.class.drugs = '2mgScopo_6mgPilo';
popt{2}.class.coding = 'dosage';
popt{3}.class.drugs = '2mgScopo_Vehicle';
popt{3}.class.coding = 'dosage';
popt{4}.class.drugs = 'Vehicle_6mgPilo';
popt{4}.class.coding = 'dosage';
popt{5}.class.drugs = 'anta_vs_ago';
popt{5}.class.coding = 'effect';

% Start timer
tic;

% Iterate over design and pharma and display statistics
opt = struct();
opt.multistart = 'best';

for designCell = masterSets.DESIGNS
    opt.design = char(designCell);
    
    switch opt.design
        case 'MMN_0.1'
            opt.priors = 'pdcm'; %'pX04';
            opt.hemisphere = 'rhs';
        case 'MMN_0.2'
            opt.priors = 'pX06';
            opt.hemisphere = 'lhs';
    end
    
    for k = 1 : length(popt)
        
        % Get adjusted options
        options = ratmpi_set_global_options(opt);
        
        % Important: Here we need to specify which BMA we are running in 'options'
        options.class.drugs = popt{k}.class.drugs;
        options.class.coding = popt{k}.class.coding;
        options = return_identifiers(options);
        
        % Display header
        ratmpi_master_print_section_header(...
            ['Display Classification results for BMA (best) ' ...
            'design: ' options.design '; ' ...
            'drugs: ' options.class.drugs '; ' ...
            'coding: ' options.class.coding '; '], endTime);
        
        % Return stats
        r = ratmpi_class_statistics('group_pharma', '', options);
        
        % Display
        fprintf('\n Confusion Matrix Training: \n');
        disp(r.confMat.train);
        
        fprintf('\n Confusion Matrix Prediction: \n');
        disp(r.confMat.pred);
        
        fprintf('\n Balanced Accuracy: \n');
        disp(r.accuracy);
        
        fprintf('\n pValue (Permutation statistics): \n');
        disp(r.pValue);
        
        fprintf('\n Optimized Learners: \n');
        disp(r.learners);
        
    end
    
        ratmpi_master_print_section_header(...
            ['Display support vectors discrimination (pValues)'], endTime);
        script_ratmpi_class_permTest_supportVectors();
    
end

% End Timer
toc;

end

function options =  return_identifiers(options)

switch options.dcm.bms.sv
    case 'best'
        suffix = '';
    case 'default'
        suffix = '_default';
end

options.dcm.bms.identifier = [options.dcm.bms.effects '_' ...
    options.dcm.bms.family '_' ...
    options.dcm.bms.bma suffix];

options.class.identifier  = [options.altnames.design '_' ...
    options.hemisphere '_class_linear_' ...
    options.class.hemi '_' options.class.pars '_' ...
    options.class.drugs '_' options.class.coding  suffix];

end