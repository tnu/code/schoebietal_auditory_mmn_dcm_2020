function [  ] = ratmpi_master_dcm_diagnostics_bad_vE(masterSets)
% [  ] = ratmpi_master_dcm_diagnostics_bad_vE(masterSets)
% 
% Master for preproc review: Displays subject codes, models, drug, design
% and hemispheres for inversions with vE scores < 0.6
% 
% 
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
% 
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Display title
endTime = ratmpi_master_print_header('Diagnostics: Bad Variance Explained', 100, @seconds);

% Load options
options = ratmpi_set_global_options();

% Prepare sets
sets = struct();
sets.fields = {'vE'};
sets.factor = 'pharma';
sets.sv = 'best';
sets.mids = options.dcm.models;
sets.pharma = options.drugs;


% Start timer
tic;

% Iterate over design and pharma and display statistics
opt = struct();
    
for designCell = masterSets.DESIGNS
    opt.design = char(designCell);
    
    for hemiCell = masterSets.HEMI
        opt.hemisphere = char(hemiCell);
    
    switch opt.design
        case 'MMN_0.1'
            opt.priors = 'pX04'; 
        case 'MMN_0.2'
            opt.priors = 'pX06'; 
    end
    
    % Get adjusted options
    options = ratmpi_set_global_options(opt);
          
    % Display header
    ratmpi_master_print_section_header(...
        ['Diagnostics: vE < 0.6 for ' ...
        'hemi: ' options.hemisphere '; ' ...
        'design: ' options.design], endTime);
    
    % Set variable settings
    sets.design = options.design;
    sets.hemi = options.hemisphere;

    % Get ids
    ids = options.subjectIDs.pharma.lvl2.(options.altnames.design).(options.hemisphere);
    
    % Open a new figure and plot
    [s, X] = ratmpi_dcm_summaryMaster(ids, sets, options);
    
    % Get indices to the bad inversions and create cell array
    idx = s.vE' < 0.6;
    vE = num2cell(round(s.vE(idx)', 2)); 
    xx = table2cell(X.t(idx, :));
    
    t = cell2table([vE, xx], ...
        'VariableNames', [{'vE'}, X.t.Properties.VariableNames]);
    
    disp(t);
    
    end
    
end

% End Timer
toc;

end