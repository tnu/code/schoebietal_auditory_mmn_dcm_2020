function [  ] = ratmpi_master_excluded_data(masterSets)
% [  ] = ratmpi_master_excluded_data(masterSets)
%
% Master for preproc review: Plots the data of the excluded subjects
%
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version).
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Display title
endTime = ratmpi_master_print_header('Excluded Datasets', 3, @seconds);

% List of excluded subjects
excl = struct();
excl.lhs = {'27988'};
excl.rhs = {'27909', '27985', '27988'};


% Start timer
tic


% Iterate over design and pharma and display statistics
opt = struct();
for designCell = {'MMN_0.1'}
    opt.design = char(designCell);
    
    switch opt.design
        case 'MMN_0.1'
            opt.priors = 'pX04';
        case 'MMN_0.2'
            opt.priors = 'pX06';
    end
    
    for hemiCell = masterSets.HEMI
        opt.hemisphere = char(hemiCell);
        
        for idCell = excl.(char(hemiCell))
            id = char(idCell);
            
            % Get adjusted options
            options = ratmpi_set_global_options(opt);
            
            
            % Display header
            ratmpi_master_print_section_header(...
                ['Preprocessing for id: ' id '; design: ' options.design '; hemi: ' options.hemisphere], ...
                endTime);
            
            % Plot the ERPs with statistics
            figure();
            ratmpi_plot_1stLevel_anova2_tone_pharma(id, 0, options);
            
            % Create information about figure
            g.Name = ['1st Level ERPs ' ...
                'id: ' id '; ' ...
                'Design: ' options.design  '; ' ...
                'hemi: ' options.hemisphere];
            
        end
    end
    
end

% End Timer
toc

end


function t = return_stats(stats)
% Return stats as used in paper

ids = stats.t.total.Properties.RowNames;
varNames = stats.t.total.Properties.VariableNames;

data = stats.total;

% Add up all raw numbers
total = sum(data);

% Recompute percentages
total(4) = total(3) / total(end) * 100;
total(6) = total(5) / total(end) * 100;

% Expand data and add to table
ids = [ids; ['Total: n = ' num2str(length(ids) - 1)]];
data = [data; total];
t = array2table(data, 'RowNames', ids, 'VariableNames', varNames);


end