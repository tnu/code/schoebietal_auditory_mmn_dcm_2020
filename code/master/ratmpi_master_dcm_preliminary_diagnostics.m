function [  ] = ratmpi_master_dcm_preliminary_diagnostics(masterSets)
% [  ] = ratmpi_master_dcm_preliminary_diagnostics(masterSets)
% 
% Master for preproc review: Computes the diagnostics to
% motivate the preliminary analyses, i.e. compares the inversions with 
% Priors motivated from the non-pharmacological dataset, and without.
% Statistics are computed for
%
%   -Variance Explained (incl percentiles)
%   -Free Energy (incl percentiles)
% 
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
% 
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Display title
endTime = ratmpi_master_print_header('Preliminary Validity', 100, @seconds);

% Start timer
tic;

% Iterate over design and pharma and display statistics
opt = struct();
opt.design = 'MMN_0.1'; 

for hemiCell = masterSets.HEMI
    opt.hemisphere = char(hemiCell);
    
    % Get adjusted options
    options = ratmpi_set_global_options(opt);
       
    % Display header
    ratmpi_master_print_section_header(...
        ['Display preliminary Validity for ' ...
        'hemi: ' options.hemisphere '; ' ...
        'design: ' options.design], endTime);
    
    % Get ids
    ids = options.subjectIDs.pharma.lvl2.(options.altnames.design).(options.hemisphere);
    
    % Run routinge
    stats = ratmpi_diagnostics_preliminary_validity(ids, '', options);
    
    % Plot the summary for the free energies
    fprintf('Free Energy ...\n\n');
    disp(stats.F.t); 
    fprintf('\n\n');
    
    % Plot the summary for variance Explained
    fprintf('Variance Explained ...\n\n');
    disp(stats.vE.t); 
    fprintf('\n\n');

end

% End Timer
toc;

end