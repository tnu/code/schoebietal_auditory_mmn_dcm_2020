function [  ] = ratmpi_master_dcm_Ep_bma_best(masterSets)
% [  ] = ratmpi_master_dcm_Ep_bma_best(masterSets)
%
% Master for preproc review: Plots the Posterior BMA estimates from the
% best starting values, averaged over left and right hemisphere. Errorbars
% depict SEM.
%
% Statistics come from two mixed effect models (mfx) with linear effect of
% drug (linear) and a regular one way ANOVA (anova). Statistics are
% Bonferoni Corrected. Only results are displayed that are significant at p
% < 0.05 (red bars).
%
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Display title
endTime = ratmpi_master_print_header('BMA (best): Neuronal Parameters', 15, @seconds);

% Load options
options = ratmpi_set_global_options();

% Prepare sets
sets.fields = {'Ep'};
sets.varargin = {'A', 'B', 'G', 'T'};
sets.mids = {''};
sets.design = options.design;
sets.pharma = options.drugs;
sets.hemi = {'lhs', 'rhs'};
sets.factor = 'pharma';
sets.sv = {'bma'};
sets.plot.dotplot = 0;


% Start timer
tic;

% Iterate over design and pharma and display statistics
opt = struct();
opt.multistart = 'best'; 

for designCell = masterSets.DESIGNS
    opt.design = char(designCell);
    
    switch opt.design
        case 'MMN_0.1'
            opt.priors = 'pX04';
        case 'MMN_0.2'
            opt.priors = 'pX06';
    end
    
    % Get adjusted options
    options = ratmpi_set_global_options(opt);
    
    % Display header
    ratmpi_master_print_section_header(...
        ['Plotting EP (BMA, best) for ' ...
        'design: ' options.design], endTime);
    
    % Set variable settings
    sets.design = options.design;
    sets.pharma = options.drugs;
    
    % Get ids
    ids = options.subjectIDs.pharma.lvl2.(options.altnames.design).xhs;
    
    % Open a new figure and plot
    g = figure();
    ratmpi_plot_EpMaster(ids, sets, options);
    
    % Create information about figure
    g.Name = ['Ep (BMA, best) for ' ...
        'Group: ' masterSets.gid '; ' ...
        'Design: ' options.design];
    
    % Load the statistics file with linear trend
    details = ratmpi_subjects(masterSets.gid, '', options);
    stats = load(details.file.stats.lvl2.Ep.mfx.linear);
    
    g = figure();
    ratmpi_2ndLevel_plot_mfx_theta(stats);
    g.Name = ['Stats (linear) on Ep (BMA, best) for ' ...
        'Group: ' masterSets.gid '; ' ...
        'Design: ' options.design];
   
    clear stats
    
        % Load the statistics file one way anova
    details = ratmpi_subjects(masterSets.gid, '', options);
    stats = load(details.file.stats.lvl2.Ep.mfx.anova);
    
    g = figure();
    ratmpi_2ndLevel_plot_mfx_theta(stats);
    g.Name = ['Stats (anova) on Ep (BMA, best) for ' ...
        'Group: ' masterSets.gid '; ' ...
        'Design: ' options.design];
    
    clear stats details
    
end

% End Timer
toc;

end