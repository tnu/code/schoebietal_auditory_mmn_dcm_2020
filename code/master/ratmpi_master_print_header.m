function [ endTime ] = ratmpi_master_print_header(titleString, expTime, uu)
% [  ] = ratmpi_master_print_header(titleString)
% 
% Creates and prints a section title header in the master scripts
% 
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
% 
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get expected end time
endTime = datetime + uu(expTime);
expDur = uu(expTime);

% Prepare Formatspec
headerLine = repmat('-', 1, 75);
formatSpec = ('%75s \n\n %-39s \n %-50s \n %-46s \n %-38s \n\n %75s \n\n');

% Print
fprintf(formatSpec, ...
    headerLine, ...
    char(datetime), ...
    ['Running ' titleString ' ...'], ...
    ['Expected Computation Time: ' char(expDur)], ...
    ['Expected End Time: ' char(endTime)], ...
    headerLine);

% Pause for 2 second
pause(2)

end
