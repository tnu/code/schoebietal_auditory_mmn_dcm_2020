function [  ] = ratmpi_master_2ndLevel_statistics_erp(masterSets)
% [  ] = ratmpi_master_2ndLevel_statistics_erp(masterSets)
% 
% Master for preproc review: Plots the grand average ERPs with
% corresponding mfx statistics (fdr corrected)
% 
%   y ~ tone + pharma + tone * pharma + (1|id)
% 
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
% 
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Display title
endTime = ratmpi_master_print_header('ERPs with Classical Statistics', 150, @seconds);

% Start timer
tic;

% Iterate over design and pharma and display statistics
opt = struct();

for hemiCell = masterSets.HEMI
    opt.hemisphere = char(hemiCell);
    
for designCell = masterSets.DESIGNS
    opt.design = char(designCell);
    
    switch opt.design
        case 'MMN_0.1'
            opt.priors = 'pX04'; 
        case 'MMN_0.2'
            opt.priors = 'pX06'; 
    end
    
    % Get adjusted options
    options = ratmpi_set_global_options(opt);
       
    % Display header
    ratmpi_master_print_section_header(...
        ['Plotting Classical ERPs for ' ...
        'hemi: ' options.hemisphere '; ' ...
        'design: ' options.design], endTime);
    
    % Open a new figure and plot
    g = figure(); 
    ratmpi_plot_2ndLevel_mfx_tone_pharma_id(masterSets.gid, 0, options);
    
    % Create information about figure
    g.Name = ['ERPs for ' ...
        'Group: ' masterSets.gid '; ' ...
        'Hemi: ' options.hemisphere '; ' ...
        'Design: ' options.design];
end

end

% End Timer
toc;

end