function [  ] = ratmpi_MASTERSCRIPT()
% [  ] = ratmpi_MASTERSCRIPT()
% 
% Masterscript that creates all the tables and figures used in the paper. 
% 
% IN
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Set designs, hemispheres and drugs 
masterSets = struct();
masterSets.gid = 'group_pharma';
masterSets.DESIGNS = {'MMN_0.1'};
masterSets.HEMI = {'lhs', 'rhs'}; 
masterSets.DRUGS = {'2mgScopo', '1mgScopo', 'Vehicle', '3mgPilo', '6mgPilo'};

% -------------------------------------------------------------------------
%                           PREPROCESSING
% --------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_preproc_log'); 
ratmpi_master_preproc(masterSets);
diary off

% -------------------------------------------------------------------------
%                           Classical ERPs
% -------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_2ndLevel_statistics_erp_log'); 
ratmpi_master_2ndLevel_statistics_erp(masterSets);
diary off

% -------------------------------------------------------------------------
%                           Exclusion of Subjects
% -------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_excluded_data_log'); 
ratmpi_master_excluded_data(masterSets);
diary off


% -------------------------------------------------------------------------
%                           DCM Multistart Validation
% -------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_dcm_multistart_diagnostics_log'); 
ratmpi_master_dcm_multistart_diagnostics(masterSets)
diary off



% -------------------------------------------------------------------------
%                          Bayesian Model Selection
% -------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_dcm_bms_log'); 
ratmpi_master_dcm_bms(masterSets)
diary off


% -------------------------------------------------------------------------
%                          Group level variance Explained
% -------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_dcm_vE_log'); 
ratmpi_master_dcm_vE(masterSets)
diary off


% -------------------------------------------------------------------------
%                     Group Level: Average Model fit for m16
% -------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_dcm_fits_log'); 
ratmpi_master_dcm_fits(masterSets)
diary off


% -------------------------------------------------------------------------
%                  BMA (best) Neuronal Parameters with Statistics
% -------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_dcm_Ep_bma_best_log'); 
ratmpi_master_dcm_Ep_bma_best(masterSets)
diary off


% -------------------------------------------------------------------------
%                       Classification on BMA (best)
% -------------------------------------------------------------------------
ratmpi_master_diary('ratmpi_master_class_best_log'); 
ratmpi_master_class_best(masterSets)
diary off




end
