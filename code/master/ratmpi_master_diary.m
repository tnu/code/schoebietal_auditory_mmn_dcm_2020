function [  ] = ratmpi_master_diary(filename)
% [  ] = ratmpi_master_diary(filename)
% 
% Creates and prints a section title header in the master scripts
% 
% IN
%
%   OPTIONAL:
% 
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


options = ratmpi_set_global_options();

% Check if the logfile already exists
l = dir(fullfile(options.rootdir, 'code', 'master', filename));

% Remove old file
if ~isempty(l)
    delete(fullfile(l.folder, l.name)); 
end

% Start diary
diary(fullfile(options.rootdir, 'code', 'master', filename));

end
