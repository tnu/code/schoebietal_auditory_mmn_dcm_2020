function [  ] = ratmpi_master_preproc(masterSets)
% [  ] = ratmpi_master_preproc(masterSets)
%
% Master for preproc review: Display of preproc Summary for all
% pharmacological levels and both designs. Statistics include:
%   -Number of detected Trials
%   -Number of good Trials
%   -Number of bad Trials
%
% IN
%   masterSets   struct           Settings for the masterScript
%
%   OPTIONAL:
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Display title
endTime = ratmpi_master_print_header('Preprocessing Statistics', 3, @seconds);

% Start timer
tic


% Iterate over design and pharma and display statistics
opt = struct();
for designCell = masterSets.DESIGNS
    opt.design = char(designCell);
    
    switch opt.design
        case 'MMN_0.1'
            opt.priors = 'pX04';
        case 'MMN_0.2'
            opt.priors = 'pX06';
    end
    
    for pharmaCell = masterSets.DRUGS
        opt.pharma = char(pharmaCell);
        
        
        % Get adjusted options
        options = ratmpi_set_global_options(opt);
        
        % Get paths and files
        details = ratmpi_subjects(masterSets.gid, '', options);
        
        % Load statistics (stored in variable Stats)
        load(details.file.diagnostics.preproc, 'stats');
        
        % Display header
        ratmpi_master_print_section_header(...
            ['Preprocessing for design: ' options.design '; pharma: ' options.pharma], ...
            endTime);
        
        % Display summary
        t = return_stats(stats);
        disp(t);
        
    end
    
end

% End Timer
toc

end


function t = return_stats(stats)
% Return stats as used in paper

ids = stats.t.total.Properties.RowNames;
varNames = stats.t.total.Properties.VariableNames;

data = stats.total;

% Add up all raw numbers
total = sum(data);

% Recompute percentages
total(4) = total(3) / total(end) * 100;
total(6) = total(5) / total(end) * 100;

% Expand data and add to table
ids = [ids; ['Total: n = ' num2str(length(ids) - 1)]];
data = [data; total];
t = array2table(data, 'RowNames', ids, 'VariableNames', varNames);


end