function [ ids, sets ] = ratmpi_return_sets(options)
% [ ids, sets ] = ratmpi_return_sets(options)
% 
% Returns settings as used in the master functions
% 
% IN
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   ids          cell             Default ids        
%   sets          struct          Default sets        
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get options if not specified
if nargin < 1
    options = ratmpi_set_global_options(); 
end

ids = options.subjectIDs.pharma.lvl2.(options.altnames.design).(options.hemisphere); 

sets = struct(); 
sets.mids = options.dcm.models; 
sets.design = {options.design};
sets.pharma = options.drugs;
sets.fields = {'F'};
% sets.varargin= {'B'}; 
sets.hemi = {options.hemisphere};
sets.sv = {'best'};

end

