function [mid, vbsv, s] = ratmpi_unwrap_input(m, options)
% [ mid, vbsv, s ] = ratmpi_unwrap_input(m, options)
%
% Makes the use of the model identifier 'm' more versatile, to also code
% the specific vbsv inversion.
%
% IN
%   mid          string/struct    Model identifier (string) or model AND
%                                 vbsv identifier (struct)
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   mid          string           Model identifier
%   vbsv         string           VBSV identifier
%   s            string           Name of combination (as used in
%                                 ratmpi_subjects to access files
%
%-------------------------------------------------------------------------
% 
%
% May of the functions (like ratmpi_subjects) take inputs id, m, options. 
% However, for some of the routines it is helpful if one can also
% explicitly address the file from the vbsv inversion. In that situation, m
% can be set as a structure.
% 
%   Examples: 
%       Calling the model 8:            m = '08' or m.mid = '08';
%       Callling model 8 and vbsv 14:   m.mid = '08', 'm.vbsv = '14';
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


mid = ''; 
vbsv = '';
s = struct(); 

if ischar(m)
    mid = m; 
    vbsv = ''; 
    s.mid = ['m' mid]; 
    s.identifier = s.mid; 
elseif isempty(m)
    s.mid = ['m' mid]; 
    s.identifier = s.mid;
end

if isstruct(m)
    if isfield(m, 'mid') & isfield(m, 'vbsv')
        mid = m.mid; 
        vbsv = m.vbsv;
        s.mid = ['m' mid];
        s.vbsv = ['vbsv' vbsv]; 
        s.identifier = [s.mid '_' s.vbsv]; 
    elseif isfield(m, 'mid') & ~isfield(m, 'vbsv')
        mid = m.mid; 
        s.mid = ['m' mid];
        s.identifier = [s.mid];
    elseif ~isfield(m, 'mid') & isfield(m, 'vbsv')
        vbsv = m.vbsv;
        s.vbsv = ['vbsv' vbsv]; 
        s.identifier = [s.vbsv];
    end
end

    

        
end

