function [r] = ratmpi_reshape_eeg_data(D, channels)
% [ r ] = ratmpi_reshape_eeg_data(D, channels)
% 
% Reshapes the EEG data into a structure with separate field for channels
% and conditions.
% 
% IN
%   D            EEG/MEG          EEG/MEG Data structure
%   OPTIONAL:
%   channels     struct           List of channels for which to reshape the
%                                 data
% 
% OUT
%   r            struct           Structure containing reshaped EEG data
%
%--------------------------------------------------------------------------
%
% EXAMPLE
%   r = ratmpi_reshape_eeg_data(D, {'lA1', 'lPAF'};
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 2
    channels = {'lA1', 'rA1', 'lPAF', 'rPAF'};
end

r = struct(); 

for chanCell = channels
    chan = char(chanCell);
    
    for condCell = D.condlist
        cond = char(condCell);
        
        dat = squeeze(selectdata(D, chan, [], cond));
        
        r = setfield(r, chan, cond, dat); 
    
    end
end

M.pst = D.time;
M.chanlabels = channels;
M.nChannels = numel(channels);
M.nConditions = numel(D.condlist);
M.condlist = D.condlist;

r = setfield(r, 'M', M);

end


