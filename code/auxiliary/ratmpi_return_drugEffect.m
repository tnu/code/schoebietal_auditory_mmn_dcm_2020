function [ effectCell ] = ratmpi_return_drugEffect(pharmaCell)
% [ effectCell ] = ratmpi_return_drugEffect(pharmaCell)
% 
% Returns the effect of a drug instead of the speficif dosage. Here, i.e.
% 2mgScopo, 1mgScopo -> antagonist
% Vehicle -> placebo
% 3mgPilo, 6mgPilo -> agonist
% 
% IN
%   
%   pharmaCell   cell            Cell with pharma labels
% 
% OUT
%   effectCell   cell            Cell with effect Labels
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


effectCell = pharmaCell;

effectCell(strcmp(pharmaCell, '2mgScopo')) = {'antagonist'};
effectCell(strcmp(pharmaCell, '1mgScopo')) = {'antagonist'};

effectCell(strcmp(pharmaCell, 'Vehicle')) = {'placebo'};

effectCell(strcmp(pharmaCell, '3mgPilo')) = {'agonist'};
effectCell(strcmp(pharmaCell, '6mgPilo')) = {'agonist'};


end

