function [ options ] = ratmpi_return_dcmIdentifier(options)
% [ options ] = ratmpi_return_dcmIdentifier(hemi, options)
%
% Returns the options structure for the pharma data including the
% appropriate settings for the DCM identifier for a particular design.
%
% IN
%   design       struct           'MMN_0.1' or 'MMN_0.2'
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   options      struct           Updated options structure
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

options.dcm.save = 1;
options.dcm.rid = '1';
options.dcm.models = {'01', '02', '03', '04', '05', '06', '07', '08', ...
    '09', '10', '11', '12', '13', '14', '15', '16'};

options.dcm.arch = 'cmc';
options.dcm.simulate = options.simulate;

options.dcm.pF = 'pF2';

options.dcm.optim.lm.up = 0.5;
options.dcm.optim.lm.down = 2;
options.dcm.optim.Nmax = 200;

switch options.priors
    case 'pX02'
        
        if strcmp(options.design, 'MMN_0.2')
            error('Invalid combination of prior and design');
        end
        options.dcm.priors.neural = 'pX02';
        options.dcm.priors.forward = 'gX02';
        options.dcm.priors.noise = options.hyperpriors;
        options.dcm.optim.multistart = 'P01';
        
    case 'pX03'
        if strcmp(options.design, 'MMN_0.2')
            error('Invalid combination of prior and design');
        end
        options.dcm.priors.neural = 'pX03';
        options.dcm.priors.forward = 'gX03';
        options.dcm.priors.noise = options.hyperpriors;
        options.dcm.optim.multistart = 'P03';
        
        
    case 'pX04'
        
        if strcmp(options.design, 'MMN_0.2')
            error('Invalid combination of prior and design');
        end
        options.dcm.priors.neural = 'pX04';
        options.dcm.priors.forward = 'gX04';
        options.dcm.priors.noise = options.hyperpriors; 
        options.dcm.optim.multistart = 'P04';
        
    case 'pX06'
        
        if strcmp(options.design, 'MMN_0.1')
            error('Invalid combination of prior and design');
        end
        options.dcm.priors.neural = 'pX06';
        options.dcm.priors.forward = 'gX06';
        options.dcm.priors.noise = options.hyperpriors;
        options.dcm.optim.multistart = 'P06';     
        
end

options.dcm.identifier = [options.dcm.arch '_' ...
    options.dcm.simulate '_' ...
    options.dcm.priors.neural '_' ...
    options.dcm.priors.forward '_' ...
    options.dcm.priors.noise '_' ...
    options.dcm.pF '_' ...
    options.dcm.optim.multistart '_' ...
    options.dcm.rid];

end

