function [x] = ratmpi_return_fnames(D)
% [ x ] = ratmpi_return_fnames(D)
% 
% Create list of all fieldnames in a structure. It explicitly names them
% with index for matrices. This is used to label axes for the DCM parameter
% plots nicely.
% 
% IN
%   D            struct           Any structure with only one 'layer' of
%                                 fields, e.g. DCM.Ep
%   OPTIONAL:
%
% OUT
%   x            cell             Cell containing all fieldnames with
%                                 indexing
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


x = {};

for fnameCell = fieldnames(D)'
    fname = char(fnameCell);
    
    if ~iscell(getfield(D, fname))
        
        for i = 1 : size(getfield(D, fname), 1)
            for j = 1 : size(getfield(D, fname), 2)
                x = [x, sprintf('%s%1.0f%1.0f', fname, j, i)];
            end
        end
        
        
    else
        for k = 1 : length(getfield(D, fname))
            y = getfield(D, fname, {k})
            
            for i = 1 : size(y{:}, 1)
                for j = 1 : size(y{:}, 2)
                    x = [x, sprintf('%s%1.0f%1.0f%1.0f', fname, k, j, i)];
                end
            end
            
        end        
    end
    
end

