function [ opharma ] = ratmpi_convert_pharma2ordinal(drugs, ranking)
% [ opharma ] = ratmpi_convert_pharma2ordinal(drugs, ranking)
% 
% Converts a cell of (nominal) pharma levels into an ordinal variable. 
% 
% IN
%   drugs       cell             Cell of nominal pharma levels
%   ranking     mat              Vector of levels
%   
%   OPTIONAL:
%
% OUT
%   opharma      mat             Vector of pharma levels coded as ordinal
%                                variables (mean centered)
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Create ordinal vector
lvl = [1 : length(ranking)]; 
lvl = lvl - mean(lvl); 

% Allocate
opharma = nan(size(drugs)); 

k = 1; 
for pharmaCell = ranking
    pharma = char(pharmaCell);
    
    idx = strcmp(pharma, drugs);
    opharma(idx) = lvl(k); 
    
    k = k + 1;
end

if any(isnan(opharma))
    error('Something went wrong'); 
end
    


end

