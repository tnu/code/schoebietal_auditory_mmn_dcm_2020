function [ subjectIDs ] = ratmpi_return_subjectIDs(options)
% [ subjectIDs ] = ratmpi_return_subjectIDs(options)
%
% Returns all subject IDs as used in ratmpi_set_global_options(). Is meant
% to keep the ratmpi_set_global_options function slimmer.
%
% IN
%   options     struct       as returned by ratmpi_set_global_options 
%   OPTIONAL:
%
% OUT
%   subjectIDs  cell         cell containing subject IDs
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Allocate variables
% Rename just to keep the code more readable
design = options.design; 
hemi = options.hemisphere; 
pharma = options.pharma;

% Some of the objects need the alternative naming
npharma = options.altnames.pharma;
ndesign = options.altnames.design;
nhemi = hemi;



% Set up placebo dataset
% -------------------------------------------------------------------------

subjectIDs.placebo.all = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.placebo.txt']);

subjectIDs.placebo.MMN01.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.placebo.grandAverage.MMN01.lhs.txt']);
subjectIDs.placebo.MMN01.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.placebo.grandAverage.MMN01.rhs.txt']);
subjectIDs.placebo.MMN02.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.placebo.grandAverage.MMN02.lhs.txt']);
subjectIDs.placebo.MMN02.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.placebo.grandAverage.MMN02.rhs.txt']);


% Set up pharmacological dataset
% -------------------------------------------------------------------------

subjectIDs.pharma.all = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.pharma.txt']);

% 2mgScopo
subjectIDs.Scopo2mg.MMN01.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.2mgScopo.grandAverage.MMN01.lhs.txt']);
subjectIDs.Scopo2mg.MMN01.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.2mgScopo.grandAverage.MMN01.rhs.txt']);
subjectIDs.Scopo2mg.MMN02.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.2mgScopo.grandAverage.MMN02.lhs.txt']);
subjectIDs.Scopo2mg.MMN02.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.2mgScopo.grandAverage.MMN02.rhs.txt']);

% 1mgScopo
subjectIDs.Scopo1mg.MMN01.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.1mgScopo.grandAverage.MMN01.lhs.txt']);
subjectIDs.Scopo1mg.MMN01.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.1mgScopo.grandAverage.MMN01.rhs.txt']);
subjectIDs.Scopo1mg.MMN02.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.1mgScopo.grandAverage.MMN02.lhs.txt']);
subjectIDs.Scopo1mg.MMN02.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.1mgScopo.grandAverage.MMN02.rhs.txt']);

% Vehicle
subjectIDs.Vehicle.MMN01.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.Vehicle.grandAverage.MMN01.lhs.txt']);
subjectIDs.Vehicle.MMN01.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.Vehicle.grandAverage.MMN01.rhs.txt']);
subjectIDs.Vehicle.MMN02.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.Vehicle.grandAverage.MMN02.lhs.txt']);
subjectIDs.Vehicle.MMN02.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.Vehicle.grandAverage.MMN02.rhs.txt']);

% 3mgPilo
subjectIDs.Pilo3mg.MMN01.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.3mgPilo.grandAverage.MMN01.lhs.txt']);
subjectIDs.Pilo3mg.MMN01.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.3mgPilo.grandAverage.MMN01.rhs.txt']);
subjectIDs.Pilo3mg.MMN02.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.3mgPilo.grandAverage.MMN02.lhs.txt']);
subjectIDs.Pilo3mg.MMN02.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.3mgPilo.grandAverage.MMN02.rhs.txt']);

% 3mgPilo
subjectIDs.Pilo6mg.MMN01.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.6mgPilo.grandAverage.MMN01.lhs.txt']);
subjectIDs.Pilo6mg.MMN01.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.6mgPilo.grandAverage.MMN01.rhs.txt']);
subjectIDs.Pilo6mg.MMN02.lhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.6mgPilo.grandAverage.MMN02.lhs.txt']);
subjectIDs.Pilo6mg.MMN02.rhs.grandAverage = ratmpi_create_subject_cell([options.rootdir '/dcm_config/ratmpi.subjectIDs.6mgPilo.grandAverage.MMN02.rhs.txt']);



% Create one placeholder for a generic grandaverage as specified by the
% input
% -------------------------------------------------------------------------
subjectIDs.placebo.grandAverage = subjectIDs.placebo.(ndesign).(nhemi).grandAverage;
subjectIDs.pharma.grandAverage = subjectIDs.(npharma).(ndesign).(nhemi).grandAverage;

% For second lvl analyses, we need to be super careful when computing
% statistics with multiple factors
subjectIDs.placebo.lvl2.MMN0X.lhs = ...
    intersect(subjectIDs.placebo.MMN01.lhs.grandAverage, subjectIDs.placebo.MMN02.lhs.grandAverage);
subjectIDs.placebo.lvl2.MMN0X.rhs = ...
    intersect(subjectIDs.placebo.MMN01.rhs.grandAverage, subjectIDs.placebo.MMN02.rhs.grandAverage);

subjectIDs.placebo.lvl2.MMN01.xhs = intersect(subjectIDs.placebo.MMN01.lhs.grandAverage, ...
    subjectIDs.placebo.MMN01.rhs.grandAverage);


subjectIDs.pharma.lvl2.MMN01.lhs = intersect_pharma(subjectIDs, 'lhs', 'MMN01');
subjectIDs.pharma.lvl2.MMN01.rhs = intersect_pharma(subjectIDs, 'rhs', 'MMN01');
subjectIDs.pharma.lvl2.MMN01.xhs = intersect(subjectIDs.pharma.lvl2.MMN01.lhs, subjectIDs.pharma.lvl2.MMN01.rhs); 

subjectIDs.pharma.lvl2.MMN02.lhs = intersect_pharma(subjectIDs, 'lhs', 'MMN02');
subjectIDs.pharma.lvl2.MMN02.rhs = intersect_pharma(subjectIDs, 'rhs', 'MMN02');
subjectIDs.pharma.lvl2.MMN02.xhs = intersect(subjectIDs.pharma.lvl2.MMN02.lhs, subjectIDs.pharma.lvl2.MMN02.rhs); 

end



function pharmaID = intersect_pharma(subjectIDs, hemi, ndesign)
% Creates a cell of subjects IDs, that contains only the subjects where all
% the pharmacological levels are present (No bad data in any
% pharmacological level of that hemisphere and design).

drugs = {'Scopo2mg', 'Scopo1mg', 'Vehicle', 'Pilo3mg', 'Pilo6mg'};
pharmaID = {};
init = 1;

for pharmaCell = drugs
    pharma = char(pharmaCell); 
    
    if init
        pharmaID = subjectIDs.(pharma).(ndesign).(hemi).grandAverage; 
        init = 0;
    end
    
    pharmaID = intersect(pharmaID, ...
        subjectIDs.(pharma).(ndesign).(hemi).grandAverage);
end

end

