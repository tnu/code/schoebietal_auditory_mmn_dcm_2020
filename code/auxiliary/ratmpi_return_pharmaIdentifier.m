function [ options ] = ratmpi_return_pharmaIdentifier(design, options)
% [ options ] = ratmpi_return_pharmaIdentifier(design, options)
%
% Returns the options structure for the pharma data including the 
% appropriate settings for the DCM identifier for a particular design. 
%
% IN
%   design       struct           'MMN_0.1' or 'MMN_0.2'
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   options      struct           Updated options structure
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options();
end

switch design
    case 'MMN_0.1'
        options.dcm.priors.neural = 'pX04'; 
        options.dcm.priors.forward = 'gX04'; 
        options.dcm.priors.noise = 'hX02'; 
        options.dcm.optim.multistart = 'P04'; 
        options.design = 'MMN_0.1';
        
    case 'MMN_0.2'
        options.dcm.priors.neural = 'pX06'; 
        options.dcm.priors.forward = 'gX06'; 
        options.dcm.priors.noise = 'hX02'; 
        options.dcm.optim.multistart = 'P06'; 
        options.design = 'MMN_0.2';
        
end

options.altnames = ratmpi_return_altnames(options);


options.dcm.identifier = [options.dcm.arch '_' ...
    options.dcm.simulate '_' ...
    options.dcm.priors.neural '_' ...
    options.dcm.priors.forward '_' ...
    options.dcm.priors.noise '_' ...
    options.dcm.pF '_' ...
    options.dcm.optim.multistart '_' ...
    options.dcm.rid];

end

