function [ vEp, xTick, xTickLabels ] = ratmpi_return_names_cmc(whichPars)
% [  ] = ratmpi_return_names_cmc()
%
% Returns the fieldnames for labeling of the parameters for a specified input
%
% IN
%
%   OPTIONAL:
%   whichPars    cell           Cell Containing only a subset of the
%                               parameter names
%
% OUT
%   vEp         cell            Vectorized form of parameter names
%   xTick       mat             Location of xTicks
%   xTickLabels mat             Labels for xTicks
% 
%-------------------------------------------------------------------------
% USAGE:
%  [~, xTick, xTickLabels] = ratmpi_return_names_cmc({'A', 'B', 'G'});
% 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 1
    whichPars = '';
end

% Names of the parameters
Ep.M = repmat({'M'}, 2, 2);
Ep.A = repmat({'A'}, 2, 2, 4);
Ep.B = repmat({'B'}, 2, 2);
Ep.N = repmat({'N'}, 2, 2);
Ep.C = repmat({'C'}, 2, 1);
Ep.T = repmat({'T'}, 1, 4);
Ep.G = repmat({'G'}, 2, 3);
Ep.D = repmat({'D'}, 2, 2);
Ep.S = {'S'};
Ep.R = repmat({'R'}, 1, 2);

% Reduce and select only the parameters of interest
if ~isempty(whichPars)
    rEp = struct();
    for fnameCell = whichPars
        fname = char(fnameCell);
        
        rEp.(fname) = Ep.(fname);
    end
else
    rEp = Ep;
end

% Expand the cellstruct with all fields
vEp = [];
for fnameCell = fieldnames(rEp)'
    fname = char(fnameCell);
    
     tmp =  rEp.(fname);
     vEp = [vEp; tmp(:)]; 
end

% Only get the first label and the position for each type
unique_labels = ratmpi_unique(vEp); 

idx = [];
for labelCell = unique_labels'
    idx = [idx; find(strcmp(char(labelCell), vEp), 1)]; 
end

% Sort the indices in ascending order for xTicks
[xTick, sidx] = sort(idx);
xTickLabels = unique_labels(sidx);





