function [ DCM] = ratmpi_dcm_loaddcm(details, path2file)
% [ DCM ] = ratmpi_dcm_loaddcm(details, path2file)
% 
% This function loads a DCM from the path in the *multistartSummary.m.
% There, absolut paths refer to the DCM and if the summary has been created
% on the cluster, the paths are not well specified. 
% Note: this is meant to be a helper function, and should not be used on
% the frontend.
% 
% IN
%   details      struct           As returned by ratmpi_subjects()
%   path2file    string           Path to estimated DCM files
%
%   OPTIONAL:
% 
% OUT
%   DCM          struct           Estimated DCM files
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


[dir_path, file_name, file_ext] = fileparts(path2file);

DCM = load(fullfile(details.path.dcm, [file_name, file_ext]), 'DCM');

end
