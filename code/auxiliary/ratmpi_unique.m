function [ uv ] = ratmpi_unique(v)
% [ uv ] = ratmpi_unique(v)
% 
% Used the Matlab routine 'unique', but sorts the elements in order of
% occurence in the vector
% 
% IN
%   v            mat              Vector / Cell of elements
%   
%   OPTIONAL:
% 
% OUT
%   uv           mat              Vector / Cell of unique elements
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Run unique
[uv, idx] = unique(v);

% Sort indices in ascending order
[~, idx] = sort(idx, 'ascend'); 

% Reorder elements in uv
uv = uv(idx);

end

