function [family, bma] = ratmpi_return_bms_families(options)
% [ family, bma ] = ratmpi_return_bms_families( options)
%
% Returns the bms family definition used for ratmpi_bms_bma
%
% IN
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   family       struct           Family structure as used in matalbbatch
%   bma          struct           BMA structre as used in matlabbatch
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get options if not specified
if nargin < 1
    options = ratmpi_set_global_options();
end

switch options.dcm.bms.family
    case 'winning'
        family(1).family_name = 'Complex';
        family(1).family_models = [12, 16];
        family(2).family_name = 'Rest';
        family(2).family_models = [1 : 11, 13 : 15];
    case 'noFam'
        family = struct('family_name', {}, 'family_models', {});
    case 'allModels'
        family(1).family_name = 'all';
        family(1).family_models = [1 : 16];
end

% Set up the BMA
bma = struct();
switch options.dcm.bms.bma
    case 'famwin'
        bma.bma_yes.bma_famwin = 'famwin';
    case 'no'
        bma.bma_no = {''};
end

end

