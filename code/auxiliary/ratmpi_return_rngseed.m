function [ rngseed ] = ratmpi_return_rngseed(rngid, options)
% [ rngseed ] = ratmpi_return_rngseed(rngid, options)
% 
% Returns the rngseed for a setting in options
% 
% IN
%   
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   rngseed      integer          Seed as used in rng(seed)
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 2
    options = ratmpi_set_global_options(); 
end

% Load mat file of seeds
load(fullfile(options.rootdir, 'dcm_config', 'auxiliary', 'ratmpi_rng_seeds.mat'), 'mySeeds'); 

% Select the n-th seed (n = rngid)
rngseed = mySeeds(rngid);

end

