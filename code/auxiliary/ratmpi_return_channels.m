function [ channels, options ] = ratmpi_return_channels(hemi, options)
% [ channels, options ] = ratmpi_return_channels(hemi, options)
%
% Returns the channels for a particualar hemisphere (or both) and the
% updated options structure.
%
% IN
%   hemi         string/cell      Hemisphere 'lhs', 'rhs' or {'lhs', 'rhs'}
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   channels     cell             Channels in that hemisphere
%   options      struct           Updated options structure
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options();
end

hemi = char(hemi);


switch hemi
    case 'lhs'
        channels = {'lA1', 'lPAF'};
        options.hemisphere = 'lhs';
        options = ratmpi_set_global_options(options);
    case 'rhs'
        channels = {'rA1', 'rPAF'};
        options.hemisphere = 'rhs';
        options = ratmpi_set_global_options(options);
    otherwise
        channels = {'lA1', 'lPAF', 'rA1', 'rPAF'};
end

end

