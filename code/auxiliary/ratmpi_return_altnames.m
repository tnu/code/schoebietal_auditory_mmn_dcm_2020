function [ altnames ] = ratmpi_return_altnames(options)
% [ altnames ] = ratmpi_return_altnames(options)
% 
% Returns the alternative naming used for design and drugs. 
% Note: 
%   The point notation in 'MMN_0.X' not valid for some of the routines.
%   Fields are not allowed to start with numbers (XmgDrugs)
% 
% IN
%   
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   altnames     struct           Alternative names for design and pharma
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Some relabeling for the use in the structures
switch options.design
    case 'MMN_0.1'
        ndesign = 'MMN01';
    case 'MMN_0.2'
        ndesign = 'MMN02';
    otherwise
        error('Design not specified');
end

switch options.pharma
    case '2mgScopo'
        npharma = 'Scopo2mg';
    case '1mgScopo'
        npharma = 'Scopo1mg';
    case 'Vehicle'
        npharma = 'Vehicle';
    case '3mgPilo'
        npharma = 'Pilo3mg';
    case '6mgPilo'
        npharma = 'Pilo6mg';
    case 'allPharma'
        npharma = 'Vehicle';
        case 'all'
        npharma = 'Vehicle';
    otherwise
        error('Pharma not specified');
end

altnames.design = ndesign; 
altnames.pharma = npharma; 

end

