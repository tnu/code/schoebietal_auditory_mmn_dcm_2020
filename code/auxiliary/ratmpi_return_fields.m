function [rD] = ratmpi_return_fields(D, fnames)
% [ rD ] = ratmpi_return_fields(D, fnames)
% 
% Creates a structure, where all the fields are removed except the ones
% specified by fnames. Used for reducing DCM structures
% 
% IN
%   D            struct           Any struct, typically DCM
%   fnames       cell             Cell containing fieldnames
%   OPTIONAL:
%
% OUT
%   rD           struct           Input structure stripped of any fields
%                                 not specified in fnames
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get all fieldnames in the struture
allfields = fieldnames(D); 

% Remove all fields not in fnames (difference between allfields and fnames)
rD = rmfield(D, setdiff(allfields, fnames)); 

end

