function [ subjectIDs ] = ratmpi_create_subject_cell( sbjtxt )
% [ subjectIDs ] = ratmpi_create_subject_cell( sbjtxt)
% 
% Loads the subject IDs from a textfile into a cell. It is assumed that the
% delimiter is a new line.
% 
% IN
%   sbjtxt       string           Path to the textfile
%   
%   OPTIONAL:
%
% OUT
%   subjectIDs    cell            Cell containing subject IDs
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


try
    fileID  = fopen(sbjtxt);
    formatSpec = '%s';
    sbjCell = textscan(fileID, formatSpec, 'Delimiter', '\n');
    subjectIDs =  sbjCell{1}';
    fclose(fileID);
    
catch
    disp('invalid textfile'); 
    subjectIDs = {''}; 
end

end

