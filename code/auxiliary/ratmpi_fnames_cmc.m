function [ fnames ] = ratmpi_fnames_cmc()
% [ fnames ] = ratmpi_fnames_cmc()
% 
% Returns the names of the variables in the parameters structures of the
% DCM. This will primarily used for plotting.
% 
% IN
%   
%   OPTIONAL:
% 
% OUT
%   fnames       struct           Structure containing the names of the
%                                 variables
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

fnames.Ep = {...
    'M11', 'M21', 'M12', 'M22', ...
    'A111', 'A121', 'A112', 'A122', ...
    'A211', 'A221', 'A212', 'A222', ...
    'A311', 'A321', 'A312', 'A322', ...
    'A411', 'A421', 'A412', 'A422', ...
    'B11', 'B21', 'B12', 'B22', ...
    'N11', 'N21', 'N12', 'N22', ...
    'C1', 'C2', ...
    'T11', 'T12', 'T13', 'T14' ...
    'G11', 'G21', 'G12', 'G22', 'G31', 'G32', ...
    'D11', 'D21', 'D12', 'D22', ...
    'S1', ...
    'R1', 'R2'...
    }';

fnames.Eg = {'L1', 'L2', ...
    'J1', 'J2', 'J3', 'J4', 'J5', 'J6', 'J7', 'J8'}';

fnames.U = {'U1', 'U2', 'U3', 'U4'}';

fnames.Eh = {'Eh'}; 

fnames.labels = {'M', 'A1', 'A2', 'A3', 'A4', 'B', 'N', 'C', 'T', 'G', 'D', 'S', 'R', 'L', 'J', 'U', 'Eh'};

end

