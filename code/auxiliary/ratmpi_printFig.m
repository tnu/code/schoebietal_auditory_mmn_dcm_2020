function [  ] = ratmpi_printFig( filename, options )
% [  ] = ratmpi_printFig(filename, options )
% 
% Exporting figures nicely. Uses the settings in options.general.printFig
% for figure export.
% 
% IN
%   filename    string            Path where the figure should be saved (no
%                                 extension)
%
%   OPTIONAL:
%   options     struct            Structure as retuned by
%                                 ratmpi_set_global_options
% 
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set default for optional input
if nargin < 2
    options = ratmpi_set_global_options(); 
end

% Get settings for figure export
sets = options.general.printFig;

gf = gcf;
set(gf, 'units','normalized','outerposition',[0 0 1 1]);
pause(0.05);

if ismember(0, sets)
    savefig([filename, '.fig'])
end

if ismember(1, sets)
    export_fig(filename, '-png');
end

if ismember(2, sets)
    export_fig(filename, '-pdf');
end

if ismember(3, sets)
    print([filename], '-dsvg');
end
