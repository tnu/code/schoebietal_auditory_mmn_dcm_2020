function [  ] = ratmpi_initialize_project()
% [  ] = ratmpi_initialize_project()
%
% Initialized the project RATMPI. Check if all dependencies are here and
% paths defined.
%
% IN
%
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options specified
options = ratmpi_set_global_options();

% Enable warnings
warning('on');

% Placeholder for error Codes
eCode = [];

% Check if DCMCM/code is in the path
l = which('mpdcm_gen_erp_host.m');

if isempty(l)
    warning(...
        ['Please add DCMCM/code to your path ' ...
        'to have full usability of the code'])
    eCode = [eCode, 1];
end

% Check if options.datadir and options.rootdir are defined
if isempty(options.rootdir)
    warning(...
        ['Please define the path of your RATMPI project ' ...
        'in options.rootdir of ratmpi_set_global_options()'])
    eCode = [eCode, 2];
end
if isempty(options.datadir)
    warning(...
        ['Please define the path to the data of your RATMPI project ' ...
        'in options.datadir of ratmpi_set_global_options()'])
    eCode = [eCode, 3];
end

if ~isempty(eCode)
    error(['Initializing encountered a critical error. ' ...
        'Please follow the instructions on the screen.']);
else
    
    % Add the relevant config files
    addpath(genpath(fullfile(options.rootdir, 'dcm_config', 'dcm')));
    addpath(genpath(fullfile(options.rootdir, 'dcm_config', 'priors')));
    addpath(genpath(fullfile(options.rootdir, 'dcm_config', 'scalingParameters')));
    
    disp(['Successfull Initialization of the project']);
    
end




end

