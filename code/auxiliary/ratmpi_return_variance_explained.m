function [ vE ] = ratmpi_return_variance_explained( DCM )
% [ vE ] = ratmpi_return_variance_explained( DCM )
% 
% Returns the variance explained for a given DCM.
% 
% IN
%   DCM          struct           Estimated DCM structure
%   
% OUT
%   vE           mat              Variance Explained
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Data
y = spm_vec(DCM.H) + spm_vec(DCM.R); 

% Prediction
yp = spm_vec(DCM.H);

% vE = 1 - Residual Variance / Data Variance
vE = 1 - var(y - yp) / var(y);

end

