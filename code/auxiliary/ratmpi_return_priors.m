function [ fp, fg, fh ] = ratmpi_return_priors(mid, options)
% [ fp, fg, fh ] = ratmpi_return_priors(mid, options)
%
% Returns the prior handle for a set of handles given by the input
%
% IN
%   mid          string           Model Identifier, eg. '01';
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   options      struct           Updated options structure
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options();
end

pX = options.dcm.priors.neural;
gX = options.dcm.priors.forward;
hX = options.dcm.priors.noise;

switch pX
    case 'pX02'
        fp = str2func([...
            'ratmpi_dcm_m' mid '_shs_priors_'...
            options.dcm.arch '_' ...
            'pX02']);
        
    otherwise
        fp = str2func([...
            'ratmpi_dcm_m' mid '_shs_priors_' ...
            options.dcm.arch '_' ...
            options.altnames.design '_' ...
            options.dcm.priors.neural]);
end

switch gX
    case 'gX02'
        fg = str2func([...
            'ratmpi_dcm_shs_priors_' ...
            options.dcm.arch '_' ...
            'gX02']);
        
    otherwise
        fg = str2func([...
            'ratmpi_dcm_shs_priors_' ...
            options.dcm.arch '_' ...
            options.altnames.design '_' ...
            options.dcm.priors.forward]);    
end

fh = str2func([...
    'ratmpi_dcm_shs_priors_' ...
    options.dcm.arch '_' ...
    options.dcm.priors.noise]);

end

