function [ options, functionHandle ] = ratmpi_return_global_options(design, hemi)
% [ options ] = ratmpi_return_global_options(design, hemi)
% 
% Returns preconfigured global options
% 
% IN
%   design  string      MMN01, MMN02
%   hemi    string      'lhs', 'rhs'
% 
% OUT
%   options struct      ratmpi_set_global_options()
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

switch design
    case 'MMN01'
        ndesign = 'MMN01';
    case 'MMN_0.1'
        ndesign = 'MMN01'; 
    case 'MMN02'
        ndesign = 'MMN02';
    case 'MMN_0.2'
        ndesign = 'MMN02'; 
    case 'pdcm'
        ndesign = 'pdcm';
end

functionHandle = str2func(['ratmpi_global_options_' ndesign '_' hemi]); 

options = functionHandle(); 

end

