function [ data, X ] = ratmpi_return_dataMaster(ids, sets, options)
% [ data, X ] = ratmpi_return_dataMaster(ids, sets, options)
%
% Takes the structure 'sets' to flag specific EEG files, and
% returns a summarized data structure.
%
% IN
%   ids          cell             Cell array of one or multiple subject ids
%   sets         struct           Configuration structure
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   data         struct           data as specified in sets
%   X            struct           Information about the returned data
%
%--------------------------------------------------------------------------
% Optional Flags on 'sets':
%   .mids        cell              String or Cell array of one or multiple
%                                  model identifiers, e.g. {'01', '04'}.
%
%   .cond        cell              Conditions, e.g. {'Standard', 'Deviant'}
%
%   .design      cell              {'MMN_0.1', 'MMN_0.2'}
%
%   .hemi        cell              {'lhs', 'rhs'}
%
%   .Dfile       string             file identifier, e.g. 'acefdfD'
%    
%   .timeWindow  mat                Vector containing start and end of time
%                                   Window in s, e.g. [0 0.25]
%
%   .pharma      cell               Pharma identifier, 
%                                   e.g. {'2mgScopo', 'Vehicle'}
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get options if not specified
if nargin < 3
    options = ratmpi_set_global_options();
end

% Get all variables from the input, or set them to the defaults
% ------------------------------------------------------------------------
if ~isfield(sets, 'design')
    disp(['Design set as in ratmpi_set_global_options: ' options.design]);
    sets.design = {options.design};
elseif ischar(sets.design)
    sets.design = {sets.design};
end

if ~isfield(sets, 'cond')
    disp(['Condition set to {Standard, Deviant}']);
    sets.cond = {'Standard', 'Deviant'};
elseif ischar(sets.cond)
    sets.cond = {sets.cond};
end

if ~isfield(sets, 'hemi')
    disp(['Hemisphere set as in ratmpi_set_global_options: ' options.hemisphere]);
    sets.hemi = {options.hemisphere};
elseif ischar(sets.hemi)
    sets.hemi = {sets.hemi};
end

if ~isfield(sets, 'Dfile')
    disp(['Data loaded from details.file.racefdfD']);
    sets.Dfile = 'racefdfD';
else
    sets.Dfile = sets.Dfile;
end

if ~isfield(sets, 'timeWindow')
    disp(['Loading the full epoch']);
    sets.timeWindow = [];
else
    disp(['Data loaded from ' num2str(sets.timeWindow)]);
end

if ~isfield(sets, 'feature')
    sets.feature = 'none';
elseif strcmp(sets.feature, 'MMN')
    disp(['Feature used: ' sets.feature]);
else
    error(['Unspecified data feature']);
end

if ~isfield(sets, 'pharma')
    disp(['Pharma set as in ratmpi_set_global_options: ' options.pharma]);
    sets.pharma = {options.pharma};
elseif ischar(sets.pharma)
    sets.pharma = {sets.pharma};
end


if ischar(ids)
    ids = {ids};
end


% Check if some of the combinations is not allowed
if strcmp(char(sets.feature), 'MMN') && strcmp(sets.Dfile, 'racefdfD')
    error(['Only sets.Dfile = acefdfD can be used in combination ' ...
        'with sets.cond = MMN']);
end

if length(sets.design) > 1 && length(ids) > 1
    warning(['CAREFUL: Watch out when returning multiple designs for multiple subjects'])
end

if length(sets.hemi) > 1 && length(ids) > 1
    warning(['CAREFUL: Watch out when returning multiple hemispheres for multiple subjects'])
end

% Initialization of large data structures. Note that we will always treat
% channels independently, therefore we initilize each channel individually
[ channels ] = ratmpi_return_channels(sets.hemi, options);
for chanCell = channels
    chan = char(chanCell);
    
    data.(chan) = [];
    X.(chan).id = [];
    X.(chan).design = [];
    X.(chan).cond = [];
    X.(chan).files = [];
    X.(chan).pharma = [];
end


% Iterate over all fields and create a concatenated dataset
for idCell = ids
    id = char(idCell);
    
    for pharmaCell = sets.pharma
        options.pharma = char(pharmaCell);
        options = ratmpi_set_global_options(options);
        
        for designCell = sets.design
            options.design = char(designCell);
            options = ratmpi_set_global_options(options);
            
            
            for hemiCell = sets.hemi
                hemi = char(hemiCell);
                
                [ channels, options ] = ...
                    ratmpi_return_channels(hemi, options);
                
                % Get paths and files. Note: It has to be done here, because
                % for cropped datasets, the file paths will be different
                details = ratmpi_subjects(id, [], options);
                D = spm_eeg_load(details.file.(sets.Dfile));
                
                for condCell = sets.cond
                    cond = char(condCell);
                    
                    
                    for chanCell = channels
                        chan = char(chanCell);
                        
                        if strcmp(sets.feature, 'MMN')
                            [dat, fact] = ...
                                return_mismatch_data(D, id, options.pharma, options.design, cond, chan, sets);
                        else
                            [dat, fact] = ...
                                return_data(D, id, options.pharma, options.design, cond, chan, sets);
                        end
                        
                        
                        % Append data to the large data matrix
                        data.(chan) = [data.(chan); dat];
                        
                        X.(chan).pharma = [X.(chan).pharma; fact.pharma];
                        X.(chan).id = [X.(chan).id; fact.id];
                        X.(chan).design = [X.(chan).design; fact.design];
                        X.(chan).cond   = [X.(chan).cond; fact.cond];
                        X.(chan).files  = [X.(chan).files; fact.files];
                        
                    end
                end
            end
        end
    end
end

display_return(data, X)


end


function [dat, fact] = return_data(D, id, pharma, design, cond, chan, sets)

fact = struct();

dat = selectdata(D, chan, sets.timeWindow, cond);

if length(size(dat)) > 2
    dat = squeeze(dat)';
end

fact.channels = repmat({chan}, size(dat, 1), 1);
fact.pharma   = repmat({pharma}, size(dat, 1), 1);
fact.cond     = repmat({cond}, size(dat, 1), 1);
fact.design   = repmat({design}, size(dat, 1), 1);
fact.id       = repmat({id}, size(dat, 1), 1);
fact.files    = repmat({D.fnamedat}, size(dat, 1), 1);

end


function [dat, fact] = return_mismatch_data(D, id, pharma, design, cond, chan, sets)

fact = struct();

idx = return_mmn_indices(D);

dat = selectdata(D, chan, sets.timeWindow, []);

if length(size(dat)) > 2
    dat = squeeze(dat)';
end

dat = dat(getfield(idx, cond), :);

fact.pharma = repmat({pharma}, size(dat, 1), 1);
fact.channels = repmat({chan}, size(dat, 1), 1);
fact.cond     = repmat({cond}, size(dat, 1), 1);
fact.design   = repmat({design}, size(dat, 1), 1);
fact.id       = repmat({id}, size(dat, 1), 1);
fact.files    = repmat({D.fnamedat}, size(dat, 1), 1);

end



function s = return_mmn_indices(D)
% Returns a structure containing the indices of the last standards before a
% deviant tone, and the corresponding deviant.

standards = find(strcmp(D.conditions, 'Standard'));
deviants = find(strcmp(D.conditions, 'Deviant'));

% If there are two consecutive deviants, remove the second;
consec = intersect(deviants, deviants + 1);
deviants = setdiff(deviants, consec);

% If there is either the standard or the deviant confounded with an
% artefact, remove both
artefact = intersect(deviants, D.badtrials);
deviants = setdiff(deviants, artefact);
artefact = intersect(deviants - 1, D.badtrials);
deviants = setdiff(deviants, artefact + 1);

% If the experiment begins with a deviant, remove it.
if deviants(1) == 1
    deviants(1) = [];
end

% Last standards before deviant
s.Standard = deviants - 1;
s.Deviant = deviants;

end


function display_return(data, X)

disp('Returning the following data:')

for chanCell = fieldnames(data)'
    chan = char(chanCell);
    
    disp(sprintf('Channel %s: \n', chan));
    
    t = struct2table(X.(chan)); 
    disp(t);
    
end

end