function [ options ] = ratmpi_set_global_options(options)
% [ options ] = ratmpi_set_global_options(options)
%
% Returns the options structure for the analyses of the RATMPI dataset. It
% provides a centralized way to change settings.
%
% IN
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   options      struct           Updated options structure
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 1
    options = struct();
elseif isfield(options, 'hemi')
    error('Fatal conflict. Never use options.hemi, this is bound for error');
end

% Set default parameters which can be overwritten by input
default.hemisphere = 'lhs';
default.pharma = 'Vehicle';

if ~isfield(options, 'hemisphere'); options.hemisphere = default.hemisphere; end
if ~isfield(options, 'pharma');     options.pharma = default.pharma; end


% Settings: Directories
options.datadir = [getenv('RATMPI_DATA'), '/data/'];
options.rawdir = [getenv('RATMPI_DATA'), '/raw/'];
options.rootdir = [getenv('RATMPI_ROOT')];
options.study = 'RATMPI';

% Settings: General
options.design = 'MMN_0.1';
options.drugs = {'2mgScopo', '1mgScopo', 'Vehicle', '3mgPilo', '6mgPilo'};
options.altnames = ratmpi_return_altnames(options);
options.subjectIDs = ratmpi_return_subjectIDs(options);

% Settings: Preprocessing
options.dataset = {[options.design '_s7-9_d16-18'], [options.design '_s16-18_d7-9']};
options.preproc.convert.samplingRate = 2000;
options.preproc.convert.triggerThreshold = 3;
options.preproc.downsamplefreq = 1000;
options.preproc.dataset = 'dataset2';
options.fstLvl.window = [0 0.25];

% Settings: Classical Statistics
options.stats.mc_corr = @ratmpi_stats_fdr;
options.stats.lvl2.conjfiles = 'anova2.tone_pharma';
options.stats.lvl2.siglvl = 0.05;

% Settings: DCM
options.priors = 'pX04';
options.multistart = 'best';
options.simulate = 'cmcdde';
options.hyperpriors = 'hX02';

options = ratmpi_return_dcmIdentifier(options);

options.dcm.bms.effects = 'RFX';
options.dcm.bms.family = 'allModels';
options.dcm.bms.bma = 'famwin';
options.dcm.bms.sv = options.multistart;

% Settings: Classification
%   Valid combinations:
%       drugs:  'all', 'Vehicle_6mgPilo', '2mgScopo_Vehicle',
%               '2mgScopo_6mgPilo'
%       coding: 'dosage'
% 
%   OR
%       drugs:  'anta_vs_ago'
%       coding: 'effect'

options.class.pars = 'bma';
options.class.hemi = 'bilat';
options.class.drugs = 'Vehicle_6mgPilo';
options.class.coding = 'dosage';
options.class.permTest = 0;
options.class.seed = 1;

switch options.dcm.bms.sv
    case 'best'
        suffix = '';
    case {'default', 'pE'}
        suffix = '_default';         
end

options.dcm.bms.identifier = [options.dcm.bms.effects '_' ...
    options.dcm.bms.family '_' ...
    options.dcm.bms.bma suffix];

options.class.identifier  = [options.altnames.design '_' ...
    options.hemisphere '_class_linear_' ...
    options.class.hemi '_' options.class.pars '_' ...
    options.class.drugs '_' options.class.coding  suffix];


% Settings: Auxiliary
options.general.printFig = [0 3];
options.revision = 0;

end
