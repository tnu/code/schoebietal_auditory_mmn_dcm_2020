function [ stats ] = ratmpi_diagnostics_return_badTrial_summary( id, options )
% [ stats ] = ratmpi_diagnostics_return_badTrial_summary(id, options)
%
% Returns a structure containing the number of detected artefacts per
% condition.
%
% IN
%   id           string           Subject identifier (e.g. '0001')
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats       struct            Summary over expected and detected events
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options();
end

% Paths and files
details = ratmpi_subjects(id, [], options);

% load dataset
D = spm_eeg_load(details.file.acefdfD);

% get trials indices
allTrials = [1 : D.ntrials];
badTrials = D.badtrials;
goodTrials = setdiff(allTrials, badTrials); 
standards = find(strcmp(D.conditions, 'Standard'));
deviants = find(strcmp(D.conditions, 'Deviant'));

% Get Standards
goodStandards = intersect(goodTrials, standards); 
nGoodStandards = length(goodStandards);
badStandards = intersect(badTrials, standards); 
nBadStandards = length(badStandards);
nStandards = nGoodStandards + nBadStandards; 

% Get Deviants
goodDeviants = intersect(goodTrials, deviants); 
nGoodDeviants = length(goodDeviants);
badDeviants = intersect(badTrials, deviants); 
nBadDeviants = length(badDeviants);
nDeviants = nGoodDeviants + nBadDeviants;

% Create Stats
stats = struct();
stats.standards = [nGoodStandards, nBadStandards, nStandards]';
stats.deviants = [nGoodDeviants, nBadDeviants, nDeviants]';
stats.all = [length(goodTrials), length(badTrials), D.ntrials]';

% Create Table
t = struct2table(stats);
t.Properties.RowNames = {'nGood', 'nBad', 'nAll'};

stats.t = t;

end

