function [ stats] = ratmpi_diagnostics_return_detected_triggers( id, options )
% [ stats ] = ratmpi_diagnostics_return_detected_trigers(id, options)
%
% Returns a structure containing the number of detected and expected
% standard and deviant trigger. Number of expected triggers are computed as
% from the total length of the experiment, time between stimuli and deviant
% probability.
%
% IN
%   id           string           Subject identifier (e.g. '0001')
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats       struct            Summary over expected and detected events
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options();
end

idx = 1;
for datasetCell = {'dataset1', 'dataset2'}
    
    options.preproc.dataset = char(datasetCell);
    
    % Get paths and files
    details = ratmpi_subjects(id, [], options);
    
    % Load raw datafile
    dat = load_file(details.file.(char(datasetCell)));
    
    % Load the trigger file that has been produced during ratmpi_convert
    triggers = load(details.file.triggers);
    triggers = triggers.events{1};
    
    % Compute number of expected trials
    expected(:, idx) = return_expected(dat, options)';
    detected(:, idx) = return_detected(triggers, options)';
    
    idx = idx + 1;
    
end

% Sum over both datasets and create nice table
stats.expected = sum(expected, 2);
stats.detected = sum(detected, 2);

t = struct2table(stats);
t.Properties.RowNames = {'Standards', 'Deviants', 'Total'};

stats.t = t;

end

function x = return_expected(dat, options)

nSamples = size(dat.data, 1);
sFreq = options.preproc.convert.samplingRate;
dT = 0.5; %Time between expected simulation (2 Hz)

% Calculated expected events
tTotal = nSamples / sFreq;
nTotal = tTotal / dT;

switch options.design
    case 'MMN_0.1'
        nDeviants = 100;
        nStandards = nTotal - nDeviants;
    case 'MMN_0.2'
        nDeviants = 200;
        nStandards = nTotal - nDeviants;
end

x(1) = nStandards;
x(2) = nDeviants;
x(3) = nTotal;

end

function x = return_detected(triggers, options)

% Detected events
x(1) = length(triggers.std);
x(2) = length(triggers.dev);
x(3) = length(triggers.dev) + length(triggers.std);

end


function dat = load_file(file)

[~,type] = fileparts(file);
if strcmpi(type,'rawdatanew')
    x = load(file);
    
    % Note: order of dat file has changed since Fabienne Jung to remove
    % redundant columns (see below and textfile
    % Acquired_data_fuer_Tina.rtf for reference).
    
    dat.data = x(:, [7 5 4 3 2 8 9]);
    dat.columns = {'Time', 'lA1', 'rA1',  'lPAF', 'rPAF', 'stdTrigger', 'devTrigger'};
    
    disp(['Data loaded from ''',file,''' using simple format']);
elseif strcmpi(type,'raw data')
    x = importdata(file,'\t',7);
    
    % Note: The following would refer to the same ordering as Fabienne
    % Jung notes in her textfile Acquired_data_fuer_Tina.rtf
    % x = x.data(:,[1 2 3 4 5 6 1 6 7]);
    
    dat.data = x.data(:,[1 5 4 3 2 6 7]);
    dat.columns = {'Time', 'lA1', 'rA1',  'lPAF', 'rPAF', 'stdTrigger', 'devTrigger'};
    disp(['Data loaded from ''',file,''' using header+body format']);
else
    error('unexpected filename');
end

end

