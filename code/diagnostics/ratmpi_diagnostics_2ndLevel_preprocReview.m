function [ stats ] = ratmpi_diagnostics_2ndLevel_preprocReview(gid, sf, options)
% [ stats ] = ratmpi_diagnostics_2ndLevel_preprocReview(gid, sf, options)
%
% Creates a table containing statistics about preprocessing. Summarizes
% information from ratmpi_diagnostics_return_detected_triggers and
% ratmpi_diagnostic_return_badTrial_summary
%
% IN
%   gid           string           group_placebo / group pharma
%   sf            bool             save or not save
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats        struct           Statistic structure containing the
%                                 trigger and bad trials summaries over 
%                                 subjects 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

switch gid
    case 'group_placebo'
        drugs = {'Vehicle'};
        subjectIDs = options.subjectIDs.placebo.all;
    case 'group_pharma'
        drugs = options.drugs;
        subjectIDs = options.subjectIDs.pharma.all;
end

for pharmaCell = drugs
    options.pharma = char(pharmaCell);
    options = ratmpi_set_global_options(options);
    
    % Initialize Variables (sloppily)
    clear st sb stats standards deviants total
    
    i = 1;
    for idCell = subjectIDs
        id = char(idCell);
        
        
        % Load trigger summary
        st = ratmpi_diagnostics_return_detected_triggers(id, options);
        
        % Load badTrial summary
        sb = ratmpi_diagnostics_return_badTrial_summary(id, options);
        
        % Concatenate everyting into a group matrix
        [standards(i, :), ...
            deviants(i, :), ...
            total(i, :)] = return_group_matrix(id, st, sb);
        
        i = i + 1;
        
    end
    
    stats.standards = standards;
    stats.deviants = deviants;
    stats.total = total;
    
    % Create table
    stats = create_table(subjectIDs, stats);
    
        % Save
    if sf
        details = ratmpi_subjects(gid, [], options);
        save(details.file.diagnostics.preproc, 'stats');
    end
end

end

function stats = create_table(ids, stats)

for fnameCell = fieldnames(stats)'
    fname = char(fnameCell);
    
    t = array2table(stats.(fname));
    t.Properties.RowNames = ids;
    t.Properties.VariableNames = ...
        {'Expected', 'Detected', ...
        'Good', 'Good_percent', ...
        'Bad', 'Bad_perc', 'Total'};
    
    stats.t.(fname) = t;
end

end

function [standards, deviants, total] = return_group_matrix(id, st, sb)

standards = [...
    st.expected(1); ...
    st.detected(1); ...
    expand_to_percent(sb.standards)]';

deviants =  [...
    st.expected(2); ...
    st.detected(2); ...
    expand_to_percent(sb.deviants)]';

total = [...
    st.expected(3); ...
    st.detected(3); ...
    expand_to_percent(sb.all)]';

end

function y = expand_to_percent(x)
% Add percentages to absolut values

y = [x(1), ...
    round(x(1) / x(3), 3) * 100, ...
    x(2), ...
    round(x(2) / x(3), 3) * 100, ...
    x(3)]';

end