function [ s ] = ratmpi_diagnostics_plot_2ndLevel_preprocReview(gid, sf, options)
% [ s ] = ratmpi_diagnostics_plot_2ndLevel_preprocReview(gid, sf, options)
%
% Plots the preprocessing Review as returned by
% ratmpi_diagnostics_2ndLevel_preprocReview
%
% See ./examples/ for more information.
%
% IN
%   gid           string           group_placebo / group pharma
%   sf            bool             save or not save
%
%   OPTIONAL:
%   options      struct            Configuration structure as returned
%                                  by ratmpi_set_global_options()
%
% OUT
%   s              struct          Data used to create plot 
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

switch gid
    case 'group_placebo'
        drugs = {'Vehicle'};
        plotfct = @plot_summary_placebo;
    case 'group_pharma'
        drugs = options.drugs;
        subjectIDs = options.subjectIDs.pharma.all;
        plotfct = @plot_summary_pharma;
end


s = {};

% Iterate over drugs and load all drug conditions into a single struct
i = 1;
for pharmaCell = drugs
    options.pharma = char(pharmaCell);
    options = ratmpi_set_global_options(options);
    
    % Get paths and files
    details = ratmpi_subjects(gid, [], options);
    
    % Load the stats file
    load(details.file.diagnostics.preproc, 'stats');
    
    % Attach the drug condition
    stats.pharma = drugs;
    
    s{i} = stats;
    i = i + 1;
end

plotfct(s, options);

if sf
    details = ratmpi_subjects(gid, [], options);
    ratmpi_printFig(details.fig.diagnostics.preproc, options);
end


end

function plot_summary_pharma(s, options)

% Initialize group specific plot settings
x.left = [1, 1.2; 1.5, 1.7; 2, 2.2; 2.5, 2.7; 3 3.2]';
x.right = [4 : 0.3 : 5.2];
x.label = [1.1 1.6 2.1 2.6 3.1, x.right];

for i = 1 : length(s)
    stats = s{i};
    subplot(2, 1, 1); hold on;
    yyaxis left
    plot(x.left(:, i), [stats.standards(:, 1), stats.standards(:, 2)], '-.');
    yyaxis right
    h = plot(x.right(:, i), [stats.standards(:, 4)], '.');
    title('Standards');
        config_pharma_plot();
    
    subplot(2, 1, 2); hold on;
    yyaxis left
    plot(x.left(:, i), [stats.deviants(:, 1), stats.deviants(:, 2)], '-.');
    yyaxis right
    plot(x.right(:, i), [stats.deviants(:, 4)], '.');
    title('Deviants');
        config_pharma_plot();

    
end
subplot(2, 1, 1);
config_pharma_axis(x.label, options.drugs);

subplot(2, 1, 2);
config_pharma_axis(x.label, options.drugs);

end

function config_pharma_axis(xlabel, pharma)

g = gca;
g.XLim = [0.5 5.7];
g.XGrid = 'on';
g.XTick = xlabel;
g.XTickLabel = [pharma, pharma];
g.XTickLabelRotation = -45;

yyaxis left
g.YLim = [0 2000];
g.YLabel.String = 'Expected and detected events';
g.YGrid = 'on';
g.YColor = [0.2 0.2 0.2];

yyaxis right
g = gca;
g.YLim = [50 100];
g.YLabel.String = 'Good Events (%) ';
g.YColor = [0.2 0.2 0.2];

g.TickDir = 'out';

end


function config_pharma_plot()

yyaxis left
g = gca;
cc = linspecer(length(g.Children));

for i = 1 : size(g.Children, 1)
    h(i) = g.Children(i);
    h(i).LineStyle = '-';
    h(i).Color = cc(i, :);
    h(i).Marker = 'o';
    h(i).MarkerFaceColor = cc(i, :);
    h(i).MarkerEdgeColor = cc(i, :);
end

yyaxis right
g = gca;
cc = linspecer(length(g.Children));

for i = 1 : size(g.Children, 1)
    h(i) = g.Children(i);
    h(i).LineStyle = 'none';
    h(i).Color = cc(i, :);
    h(i).Marker = 'o';
    h(i).MarkerFaceColor = cc(i, :);
    h(i).MarkerEdgeColor = cc(i, :);
end


end

function plot_summary_placebo(s, options)

stats = s{1};

% Initialize group specific plot settings
x.left = [1, 2];
x.right = 4;

subplot(2, 1, 1);
yyaxis left
plot(x.left, [stats.standards(:, 1), stats.standards(:, 2)], '-.');
yyaxis right
h = plot(x.right, [stats.standards(:, 4)], '.');
title('Standards');
config_placebo;


subplot(2, 1, 2);
yyaxis left
plot(x.left, [stats.deviants(:, 1), stats.deviants(:, 2)], '.');
yyaxis right
plot(x.right, [stats.deviants(:, 4)], '.');
title('Deviants');
config_placebo;

end

function config_placebo()

g = gca;
g.XLim = [0.5 4.5];
g.XGrid = 'on';
g.XTick = [1.5 4];
g.XTickLabel = {'Vehicle', 'Vehicle'};

yyaxis left
g = gca;
g.YLim = [0 2000];
g.YLabel.String = 'Expected and detected events';
g.YGrid = 'on';
g.YColor = [0.2 0.2 0.2];
cc = linspecer(length(g.Children));

for i = 1 : size(g.Children, 1)
    h(i) = g.Children(i);
    h(i).LineStyle = '-';
    h(i).Color = cc(i, :);
    h(i).Marker = 'o';
    h(i).MarkerFaceColor = cc(i, :);
    h(i).MarkerEdgeColor = cc(i, :);
end

yyaxis right
g = gca;
g.YLim = [50 100];
g.YLabel.String = 'Good Events (%) ';
g.YColor = [0.2 0.2 0.2];
cc = linspecer(length(g.Children));

for i = 1 : size(g.Children, 1)
    h(i) = g.Children(i);
    h(i).LineStyle = 'none';
    h(i).Color = cc(i, :);
    h(i).Marker = 'o';
    h(i).MarkerFaceColor = cc(i, :);
    h(i).MarkerEdgeColor = cc(i, :);
end

g.TickDir = 'out';

end
