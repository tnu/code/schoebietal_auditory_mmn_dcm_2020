function [  ] = ratmpi_diagnostics_plotArtefacts(id, sf, options)
% [  ] = ratmpi_diagnostics_plotArtefacts(id, sf, options)
%
% Create a review plot of GOOD and BAD trials, where BAD refers to trials
% marked by artefact rejection
%
% See ./examples/ for more information.
% IN
%   id           string           Subject identifier (e.g. '0001')
%   sf           bool             Save (1) or don't save (0) figure
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% OUT
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

% Files and paths
details = ratmpi_subjects(id, [], options);

channels = {'lA1', 'rA1', 'lPAF', 'rPAF'};

% Indices of the subplots
idx.good = [1 3 5 7];
idx.bad = [2 4 6 8];
idx.all = [9:10; 11:12; 13:14; 15:16];

% Load preprocessed file after artefact marking
D = spm_eeg_load(details.file.acefdfD);

pst = D.time;

% Plot all good and bad labeled trials shaded area plot
i = 1;
for chanCell = channels
    chan = char(chanCell);
    
    dat = squeeze(selectdata(D, chan, [], []))';
    
    % Get indices of good, bad, standard and deviant trials
    goodTrials = setdiff(1 : D.ntrials, D.badtrials);
    stdTrials = find(strcmp(D.conditions, 'Standard'));
    badTrials = intersect(1 : D.ntrials, D.badtrials);
    devTrials = find(strcmp(D.conditions, 'Deviant'));
    
    % Plot all good trials as a shaded min/max area with a couple of
    % examples
    subplot(4, 4, idx.good(i));
    plot_goodTrials(pst, dat, goodTrials, stdTrials, devTrials);
    title([chan ' Trials (GOOD)'])
    
    % Plot all good trials as a shaded min/max area and all badtrials as
    % single lines
    subplot(4, 4, idx.bad(i));
    plot_badTrials(pst, dat, badTrials, stdTrials, devTrials);
    title([chan ' Trials (BAD)'])
    
    % Plot Averaged ERPs before and after rejection. Shaded areas depict SEM.
    subplot(4, 4, idx.all(i, :));
    plot_erps(pst, dat, goodTrials, stdTrials, devTrials);
    
    i = i + 1;
    
end

g = gcf; 
g.Color = [1 1 1];

if sf
    ratmpi_printFig(details.fig.diagnostics.plotArtefacts, options);
end

end


function plot_erps(pst, dat, goodTrials, stdTrials, devTrials)


% Plot all Standards
trialIdx = stdTrials;
mData = mean(dat(trialIdx, :), 1);
semData = std(dat(trialIdx, :)) ./ sqrt(length(trialIdx));

[ha, hl] = mpdcm_shaded_area_plot(pst, ...
    mData, ...
    mData - semData, ...
    mData + semData);

ha.FaceColor = [0.4 0.4 0.4];
hl.Color = ha.FaceColor;
hl.LineStyle = '--';

% Plot all Deviants
trialIdx = devTrials;
mData = mean(dat(trialIdx, :), 1);
semData = std(dat(trialIdx, :)) ./ sqrt(length(trialIdx));

[ha, hl] = mpdcm_shaded_area_plot(pst, ...
    mData, ...
    mData - semData, ...
    mData + semData);

ha.FaceColor = [1 0.4 0];
hl.Color = ha.FaceColor;
hl.LineStyle = '--';


% Plot good Standards
trialIdx = intersect(goodTrials, stdTrials);
mGood = mean(dat(trialIdx, :), 1);
semGood = std(dat(trialIdx, :)) ./ sqrt(length(trialIdx));

[ha, hl] = mpdcm_shaded_area_plot(pst, ...
    mGood, ...
    mGood - semGood, ...
    mGood + semGood);

ha.FaceColor = [0.2 0.2 0.2];
hl.Color = ha.FaceColor;
hl.LineStyle = '-';

% Plot good Deviants
trialIdx = intersect(goodTrials, devTrials);
mGood = mean(dat(trialIdx, :), 1);
semGood = std(dat(trialIdx, :)) ./ sqrt(length(trialIdx));

[ha, hl] = mpdcm_shaded_area_plot(pst, ...
    mGood, ...
    mGood - semGood, ...
    mGood + semGood);

ha.FaceColor = [1 0 0];
hl.Color = ha.FaceColor;
hl.LineStyle = '-';


% Plot the legend
g = gca;
hh = findobj(g, 'Type', 'Line'); 

l = legend(flipud(hh), {'Standard (all)', 'Deviant (all)', 'Standard (good)', 'Deviant (good)'});
l.Box = 'off';

% Label axis
g.XLabel.String = 'Time [s]'; 

end

function plot_goodTrials(pst, dat, goodTrials, stdTrials, devTrials)

% Plot good Standards
trialIdx = goodTrials;
mGood = mean(dat(trialIdx, :), 1);
[ha, hl] = mpdcm_shaded_area_plot(pst, ...
    mGood, ...
    min(dat(trialIdx, :)), ...
    max(dat(trialIdx, :)));

% Plot a couple of arbitrary good trials
n = 10;
cc = linspecer(n);
goodIdx = trialIdx(floor(linspace(1, length(trialIdx), 10)));
for i = 1 : n
    plot(pst, dat(goodIdx(i), :)', 'Color', cc(i, :), 'LineWidth', 1);
end

end

function plot_badTrials(pst, dat, badTrials, stdTrials, devTrials)


mBad = mean(dat(badTrials, :), 1);
[ha, hl] = mpdcm_shaded_area_plot(pst, ...
    mBad, ...
    min(dat(badTrials, :)), ...
    max(dat(badTrials, :)));

cc = linspecer(length(badTrials));
for i = 1 : length(badTrials)
    plot(pst, dat(badTrials(i), :)', 'Color', cc(i, :), 'LineWidth', 1);
end

end





