function [s] = ratmpi_diagnostics_multistartSummary(ids, sets, options)
% [ s ] = ratmpi_diagnostics_multistartSummary(ids, sets, options)
%
% Checks for consistency between the multistart summary file and the
% designated top 4 and default. Also prints overview summaries.
%
% IN
%   ids          cell             Cell of subject ids           
%   sets         struct           PLACEHOLDER. NO EFFECT ATM.
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 3
    options = ratmpi_set_global_options();
end
 
if ischar(ids)
    ids = {ids};
end

sets = struct(); 
sets.mids = options.dcm.models;
sets.pharma = options.drugs;

for idCell = ids
    id = char(idCell);
    
    for midCell = sets.mids
        mid = char(midCell);
        
        for pharmaCell = sets.pharma
            options.pharma = char(pharmaCell);
            options = ratmpi_set_global_options(options);
            
        % Load multistart Summary
        [r, l] = ratmpi_vbsv_multistartSummary(id, mid, [0 : 99], 0, options);
        
        % Get summary statistics
        s.stats = return_summary_stats(r, l);
        
        % Check the best 4
        issue = check_top4(id, mid, r, l, options);
        
        if issue
            m = ['m' mid];
            sbj = ['ratmpi_' id];
            s.(sbj).(options.altnames.pharma).(m).issue = issue;
        end
        
        end
    end
end

end


function issue = check_top4(id, mid, r, l, options)

details = ratmpi_subjects(id, mid, options);

F = sort(r.F, 'descend'); 
issue = 0;
for i = 1 : 4
    
    try 
    [~, filename] = fileparts(l{r.idx.desc.top(i)});
    load(fullfile(details.path.dcm, filename), 'DCM');
    
    if DCM.F ~= F(i)
        warning(sprintf(['Mismatch between expected and detected free energy: \n '...
            'Subject: ' id '\n' ...
            'Model: ' mid '\n']));
%         keyboard
        issue = 1
    end
    
    catch
        
        warning(sprintf(['Mismatch between expected and detected free energy: \n '...
            'Subject: ' id '\n' ...
            'Model: ' mid '\n']));
%         keyboard
        issue = 1
    end
end



end


function stats = return_summary_stats(r, l)

stats = struct(); 
stats.nFiles = size(l, 1);


end


