function [ stats] = ratmpi_diagnostics_plot_eventDetection( id, sf, options )
% [ stats ] = ratmpi_diagnostics_plot_eventDetection()
% 
% Plots the signal in the trigger channels and the detected trigger times.
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   sf           bool             Save (1) or don't save (0) figure
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   stats        struct           Data used to generate figure
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

% Color code for plotting
cc.std = [0 0.44706 0.74118];
cc.dev = [1 0 0];

idx = 1;
for datasetCell = {'dataset1', 'dataset2'}

    options.preproc.dataset = char(datasetCell);

% Get paths and files
details = ratmpi_subjects(id, [], options); 

% Load datafile directly after converting it into spm compatible format
D = spm_eeg_load(details.file.D); 

% Load the trigger file that has been produced during ratmpi_convert
triggers = load(details.file.triggers); 
triggers = triggers.events{1}; 

% Create a window around the detected events
dW = [-0.1 * D.fsample : 0.2 * D.fsample];

% Get the expected statistics about trial occurence, calculated from total
% experiment time, and deviant probability
[stats] = estimate_stats(D, triggers, options);

% Store the signal of the trigger channel within this window for all events
% Create a shaded errorbar plot, where the shaded area depicts the min /
% max range for all events.
%-------------------------------------------------------------------------

% Standard
dat = [];
for i = 1 : length(triggers.std)
    
    if triggers.std(i) + dW(end) > size(D, 2)
        warning(['Trigger ' num2str(i) ' reached end of file']);
    elseif triggers.std(i) + dW(1) < 1
        warning(['Trigger ' num2str(i) ' too early']);
    else
        try
        dat = [dat; D(5, triggers.std(i) + dW)];
        catch
            keyboard
        end
    end
    
end

subplot(2, 2, idx)
plot_triggerChannel(dW/D.fsample, dat, cc.std);
title(['Standard Trials detection, ' char(datasetCell)]); 
text(0.8, 0.8, ...
    sprintf('Events: \n expected: %2.0f \n detected: %2.0f', stats.expected.std, stats.detected.std), ...
    'Units', 'normalized');


% Deviant
dat = [];
for i = 1 : length(triggers.dev)
    if triggers.dev(i) + dW(end) > size(D, 2)
        warning(['Trigger ' num2str(i) ' reached end of file']);
    elseif triggers.dev(i) + dW(1) < 1
        warning(['Trigger ' num2str(i) ' too early']);
    else
        dat = [dat; D(6, triggers.dev(i) + dW)];
    end
end

subplot(2, 2, idx + 2)
plot_triggerChannel(dW/D.fsample, dat, cc.dev);
title(['Deviant Trials detection, ' char(datasetCell)]); 
text(0.8, 0.8, ...
    sprintf('Events: \n expected: %2.0f \n detected: %2.0f', stats.expected.dev, stats.detected.dev), ...
    'Units', 'normalized');

idx = idx + 1;

end

if sf
ratmpi_printFig([details.path.prep '/' details.name.id ...
    '_diagnostics_eventDetection'], options)
end

end

function plot_triggerChannel(dT, dat, cc)

mdat = mean(dat, 1);

[ha, hl] = mpdcm_shaded_area_plot(dT, mdat, min(dat), max(dat)); 

ha.FaceColor = cc; 
hl.Color = cc; 

hs = stem(0, 3.5, '.k');
hs.LineWidth = 2;
hs.MarkerSize = 16;

l = legend([hl, ha hs], {'Signal Mean', 'Signal Range', 'Detected Event'})
l.Box = 'off';
l.Location = 'northwest';

g = gca; 
g.XLabel.String = 'Time [s] (normalized to detected events)';

end

function [stats] = estimate_stats(D, triggers, options)

% Time between stimuli [s]
dt = 0.5;

% Detected standards and deviants
detected.std = length(triggers.std);
detected.dev = length(triggers.dev);
detected.all = detected.std + detected.dev;

% Total length of experiment
T = size(D, 2) / D.fsample;
expected.all = T / dt;

switch options.design
    case 'MMN_0.1'
        expected.dev = 100;
        expected.std = expected.all - expected.dev;
    case 'MMN_0.2'
        expected.dev = 200;
        expected.std = expected.all - expected.dev;
end

stats.expected = expected;
stats.detected = detected;

end
