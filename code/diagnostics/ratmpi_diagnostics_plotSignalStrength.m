function [  ] = ratmpi_diagnostics_plotSignalStrength( gid, sf, options )
% [  ] = ratmpi_diagnostics_plotSignalStrength(gid, sf, options)
%
% Creates a plot of the signal range after preprocessing. For gid =
% 'group_pharma', the ranges are plotted for all pharmacological levels
% Note: The order or the bars are: lA1, lPAF, rA1, rPAF for Standard and
% Deviant.
%
% also see ./examples
%
% IN
%   gid           string           group_placebo / group pharma
%   sf            bool             PLACEHOLDER. NO EFFECT ATM.
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

switch gid
    case 'group_placebo'
        drugs = {'Vehicle'};
        subjectIDs = options.subjectIDs.placebo.all;
        dim_subplots = [3, 2];
    case 'group_pharma'
        drugs = options.drugs;
        subjectIDs = options.subjectIDs.pharma.all;
        dim_subplots = [5, 2];
end

% Set up sets to use the return_dataMaster
sets = struct();
sets.cond = {'Standard', 'Deviant'};
sets.design = {options.design};
sets.Dfile = 'prep';
sets.timeWindow = options.fstLvl.window;
sets.hemi = {'lhs', 'rhs'};
sets.pharma = drugs;

% Prepare figure
figure
global POPTIONS
POPTIONS.nRows = dim_subplots(1);
POPTIONS.nCols = dim_subplots(2);
POPTIONS.drugs = drugs;

% Iterate over subjects and return the data
%--------------------------------------------------------------------------
idx.id = 1;
for idCell = subjectIDs
    id = char(idCell);
    
    % Load the data for a single subject
    [data, X] = ratmpi_return_dataMaster(id, sets, options);
    
    % Returns a structure containing the range for all channels, conditions
    % and pharmacologies
    s  =  return_signalRange(data, X, sets, options);
    
    % Create the figure for a single subject as a subplot
    plot_signalRange(s, sets, idx.id)
    title(id)
    
    idx.id = idx.id + 1;
end

% Plot legend
config_legend();

end

function s = return_signalRange(data, X, sets, options)
% This function returns the range for all channels, pharmacological levels
% and conditions (3 Factors)

channels = fieldnames(X)';
idx.pharma = 1;

for chanCell = channels
    chan = char(chanCell);
    
    for condCell = sets.cond
        cond = char(condCell);
        
        % Allocate space for a matrix to store the range
        minData = zeros(1, length(sets.pharma));
        maxData = zeros(1, length(sets.pharma));
        
        for pharmaCell = sets.pharma
            pharma = char(pharmaCell);
            
            % Select the data individually for condition and pharma
            % combination
            conVector = ...
                strcmp(X.(chan).cond, cond) & ...
                strcmp(X.(chan).pharma, pharma);
            
            minData(idx.pharma) = min(data.(chan)(conVector, :));
            maxData(idx.pharma) = max(data.(chan)(conVector, :));
            
            idx.pharma = idx.pharma + 1;
        end
        
        % Fill the summaries into a structure
        s.(chan).(cond).range = [minData; maxData];
        
        % Reset counter
        idx.pharma = 1;
        
    end
    
end

clear POPTIONS

end


function plot_signalRange(s, sets, figid)

% Prepare figure
global POPTIONS
subplot(POPTIONS.nRows, POPTIONS.nCols, figid);
hold on;

channels = fieldnames(s)';
nPharma = length(sets.pharma);
nCond = length(sets.cond);

% Set up x-axis
xPharma = linspace(1, 100, nPharma);
xChannels = linspace(-5, 5, nCond * length(channels));
x = xPharma + xChannels';

idx.x = 1;
idx.cond = 1;

for condCell = sets.cond
    cond = char(condCell);
    
    for chanCell = channels
        chan = char(chanCell);
        
        rangePlot(x(idx.x, :), ...
            s.(chan).(cond).range(1, :), range(s.(chan).(cond).range), ...
            cond);
        hold on;
        
        idx.x = idx.x + 1;
        
    end
    
end

config_plot(x);

end

function h = rangePlot(x, y, dy, cond)

offs = 0.5;

for i = 1 : numel(x)
    xx = linspace(x(i) - offs, x(i) + offs, 2);
    ylower = linspace(y(i) - dy(i), y(i) - dy(i), 2);
    yupper = linspace(y(i) + dy(i), y(i) + dy(i), 2);
    yy = [ylower, fliplr(yupper)];
    
    fill([xx, fliplr(xx)], yy, 'b', 'Tag', cond);
    
end

h = gca;


end

function config_plot(x)

global POPTIONS

g = gca;

% Make Standards
h = findobj(g, 'Tag', 'Standard');
for i = 1 : size(h, 1)
    h(i).FaceColor = [0.1 0.1 0.1];
    h(i).EdgeColor = 'none';
end

% Make Deviants red
h = findobj(g, 'Tag', 'Deviant');
for i = 1 : size(h, 1)
    h(i).FaceColor = [1 0 0];
    h(i).EdgeColor = 'none';
end

% Iterate over all boxes, find max of range and set y-axis accordingly
maxRange = 0;
for i = 1 : size(g.Children, 1)
    h = g.Children(i);
    
    if max(abs(h.Vertices(:))) > maxRange
        maxRange = max(abs(h.Vertices(:)));
    end
    
end
g.YLim = 1.1 .* [-maxRange, maxRange];

% Plot Gridlines and XTicks
g.XTick = x(:);
g.XTickLabel = cell(1, length(x(:)));
g.TickDir = 'out';
g.XGrid = 'on';
g.YGrid = 'on';

% Plot Labels for drugs
if numel(POPTIONS.drugs) > 1
    for i = 1 : size(x, 2)
        text(mean(x(1, i)), 0.8 * g.YLim(2), POPTIONS.drugs{i});
    end
end

end


function config_legend()

global POPTIONS
g = subplot(POPTIONS.nRows, POPTIONS.nCols, 1);

h_std = findobj(g, 'Tag', 'Standard');
h_dev = findobj(g, 'Tag', 'Deviant');

l = legend([h_std(1), h_dev(1)], {'Standard', 'Deviant'});
l.Box = 'off';

end
