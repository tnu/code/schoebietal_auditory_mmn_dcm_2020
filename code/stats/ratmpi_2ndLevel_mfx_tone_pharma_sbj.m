function [ stats ] = ratmpi_2ndLevel_mfx_tone_pharma_sbj(gid, sf, options)
% [ stats ] = ratmpi_2ndLevel_mfx_tone_pharma_sbj(gid, sf, options)
%
% Computes a group level mixed effects anova of the following form
%   y = b0 + Tone + Pharma + (1|Subject)
%
%
% IN
%   gid           string          'group_pharma'
%
%   OPTIONAL:
%   sf            bool            Save (1) or don't save (0) stats
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats        struct           Structure containing concatenated data
%                                 and statistics
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

if ~strcmp(gid, 'group_pharma')
    error('This function can only be used with the pharmacological group');
end

% Loading of the data
% -----------------------------------------------------------------------
details = ratmpi_subjects(gid, [], options);

sets = struct();
sets.cond = {'Standard', 'Deviant'};
sets.design = {options.design};
sets.Dfile = 'racefdfD'; 
sets.pharma = options.drugs; 
sets.timeWindow = options.fstLvl.window;

% Iterate over hemispheres (since different subjects can be used for each
% hemisphere) and compute statistics
% -----------------------------------------------------------------------
stats = struct();

for hemiCell = {'lhs', 'rhs'}
    options.hemisphere = char(hemiCell); 
    options = ratmpi_set_global_options(options); 
    sets.hemi = {options.hemisphere};
    channels = ratmpi_return_channels(char(hemiCell), options);
    
    % Load the statistics
    fprintf(['Loading the data for ' char(sets.hemi) '... \n \n']);
    [data, X] = ratmpi_return_dataMaster(...
        options.subjectIDs.pharma.lvl2.(options.altnames.design).(char(sets.hemi)), sets, options);
    
    
for chanCell = channels
    chan = char(chanCell);
    
    Y = getfield(data, chan);
    fact.tone = getfield(X, chan, 'cond');
    fact.pharma = getfield(X, chan, 'pharma');
    fact.id = getfield(X, chan, 'id'); 
      
    % Allocate variables
    astats = cell(size(Y, 2), 1); 
    
    for i = 1 : size(Y, 2)
                
        % Create the table
        yy = Y(:, i);
        t = table(yy, fact.tone, fact.pharma, fact.id, ...
            'VariableNames', {'y', 'tone', 'pharma', 'id'});  
        
        % Fit linear mixed effects model
        lme = fitlme(t, 'y ~ tone + pharma + tone * pharma + (1|id)+(1|id:tone) + (1|id:pharma)');
        
    
        % Store important information
        p(:, i) = lme.anova.pValue;
        [astats{i}.anova] = anova(lme);
        [~, ~, astats{i}.fixedEffect] = fixedEffects(lme);
        [~, ~, astats{i}.randomEffects] = randomEffects(lme);
        astats{i}.criterion = lme.ModelCriterion;
        
    end
    
    stats = setfield(stats, chan, 'p',  p);
    stats = setfield(stats, chan, 'design', lme.designMatrix);
    stats = setfield(stats, chan, 'astats',  astats);
    stats = setfield(stats, chan, 'name', lme.anova.Term);
    stats = setfield(stats, chan, 'factor', fact);
    stats = setfield(stats, chan, 'data', X.(chan));
    
    % Clear the variables
    clear astats p 
    
    
end

% Saving the files
if sf

    if ~exist(details.path.stats.lvl2.mfx.tone_pharma_id)
        mkdir(details.path.stats.lvl2.mfx.tone_pharma_id)
    end
    
    save([details.path.stats.lvl2.mfx.tone_pharma_id '/stats.mat'], 'stats');
end

end



