function [ stats ] = ratmpi_1stLevel_anova2_tone_pharma(id, sf, options)
% [ stats ] = ratmpi_1stLevel_anova2_tone_pharma(id, sf, options)
%
% Computes a Two Way ANOVA on the subject level with factors tone and
% pharma.
%
%
% IN
%   id           string           Subject identifier (e.g. '0001')
%
%   OPTIONAL:
%   sf            bool            Save (1) or don't save (0) stats
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats        struct           Structure containing concatenated data
%                                 and statistics
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

% Loading of the data
% -----------------------------------------------------------------------
details = ratmpi_subjects(id, [], options);
sets = struct();
sets.cond = {'Standard', 'Deviant'};
sets.design = {options.design};
sets.hemi = {'lhs', 'rhs'};
sets.Dfile = 'racefdfD'; 
sets.pharma = options.drugs; 
sets.timeWindow = options.fstLvl.window;

[ data, X ] = ratmpi_return_dataMaster(id, sets, options);


% Specify environment
% -----------------------------------------------------------------------
channels = {'lA1', 'rA1', 'lPAF', 'rPAF'};
stats = struct();

for chanCell = channels
    chan = char(chanCell);
    
    Y = getfield(data, chan);
    fact.tone = getfield(X, chan, 'cond');
    fact.pharma = getfield(X, chan, 'pharma');
    
    
    for i = 1 : size(Y, 2)
        
        [p(:, i), atab{i}, astats{i}] = anovan(Y(:, i), {fact.tone, fact.pharma}, ...
            'model', 'interaction', 'varnames', {'tone', 'pharma'}, ...
            'display', 'off');
    end
    
    stats = setfield(stats, chan, 'p',  p);
    stats = setfield(stats, chan, 'atab',  atab);
    stats = setfield(stats, chan, 'astats',  astats);
    stats = setfield(stats, chan, 'name', {'Tone', 'Pharma', 'Tone * Pharma'});
    stats = setfield(stats, chan, 'factor', fact);
    stats = setfield(stats, chan, 'data', X.(chan));
    
end

% Saving the files
if sf

    if ~exist(details.path.stats.lvl1.anova2.tone_pharma)
        mkdir(details.path.stats.lvl1.anova2.tone_pharma)
    end
    
    save([details.path.stats.lvl1.anova2.tone_pharma '/stats.mat'], 'stats');
end

end



