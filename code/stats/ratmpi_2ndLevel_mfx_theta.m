function [stats] = ratmpi_2ndLevel_mfx_theta(ids, sets, options)
% [ stats ] = ratmpi_2ndLevel_mfx_theta(ids, sets, options)
%
% Computes a second level mixed effects model on the parameters.
%
% IN
%   ids         cell             PLACEHOLDER. NO EFFECT ATM.
%   sets        struct           PLACEHOLDER. NO EFFECT ATM.
% 
% OPTIONAL
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set optional input
if nargin < 3
    options = ratmpi_set_global_options();
end

% Allocate output structure
stats = struct();

sf = 0;

ids = intersect(options.subjectIDs.pharma.lvl2.(options.altnames.design).lhs, ...
    options.subjectIDs.pharma.lvl2.(options.altnames.design).rhs);

sets.fields = {'Ep', 'Cp'};
sets.varargin = {'A', 'B', 'G', 'T'};
sets.design = options.design;

switch options.class.drugs
    case '2mgScopo_6mgPilo'
        sets.pharma = {'2mgScopo',  '6mgPilo'};
    case 'all'
        sets.pharma = options.drugs;
    case 'anta_vs_ago'
        sets.pharma = {'2mgScopo', '1mgScopo', '3mgPilo', '6mgPilo'};
    case '2mgScopo_Vehicle'
        sets.pharma = {'2mgScopo', 'Vehicle'};
    case 'Vehicle_6mgPilo'
        sets.pharma = {'Vehicle', '6mgPilo'};
end

sets.coding = options.class.coding;

% Set parameters according to options configuration
switch options.class.hemi
    case 'unilat'
        sets.hemi = options.hemisphere;
    case 'bilat'
        sets.hemi = {'lhs', 'rhs'};
end

switch options.class.pars
    case '16'
        sets.mids = {'16'};
        sets.sv = {'best'};
    case 'bma'
        sets.mids = {''};
        sets.sv = 'bma';
end

% Run the summary Master to return all parameters of interest for all
% subjects, hemispheres and drugs
[s, X] = ratmpi_dcm_summaryMaster(ids, sets, options);

if strcmp(sets.coding, 'effect')
    X.pharma = ratmpi_return_drugEffect(X.pharma);
    sets.pharma =  ratmpi_unique(ratmpi_return_drugEffect(sets.pharma));
end

% Iterate over hemispheres (since different subjects can be used for each
% hemisphere) and compute statistics
% -----------------------------------------------------------------------
stats = struct();

id = X.id;

% Transform pharma into an ordinal variable
pharma = tranform_pharma(X.pharma, sets);

for i = 1 : size(s.Ep, 1)
       
    yy = s.Ep(i, :)';
    
    t = table(yy, pharma, id, ...
        'VariableNames', {'y', 'pharma', 'id'});
    
    % Fit linear mixed effects model
    lme = fitlme(t, 'y ~ pharma + (1 | id)');
    
    
    % Store important information
    p(:, i) = lme.anova.pValue;
    [astats{i}.anova] = anova(lme);
    [~, ~, astats{i}.fixedEffect] = fixedEffects(lme);
    [~, ~, astats{i}.randomEffects] = randomEffects(lme);
    astats{i}.criterion = lme.ModelCriterion;
    
end

stats = setfield(stats, 'p',  p);
stats = setfield(stats, 'design', lme.designMatrix);
stats = setfield(stats, 'astats',  astats);
stats = setfield(stats, 'name', lme.anova.Term);
stats = setfield(stats, 'sets', sets);

% Clear the variables
clear astats p

% Saving the files
if sf
end

end

function opharma = tranform_pharma(pharma, sets)
% Converts the categorical variable into a ordinal variable

opharma = zeros(size(pharma));

if strcmp(sets.coding, 'dosage')
    opharma(strcmp(pharma, '2mgScopo')) = 1;
    opharma(strcmp(pharma, '1mgScopo')) = 2;
    opharma(strcmp(pharma, 'Vehicle')) = 3;
    opharma(strcmp(pharma, '3mgPilo')) = 4;
    opharma(strcmp(pharma, '6mgPilo')) = 5;
elseif strcmp(sets.coding, 'effect')
    opharma(strcmp(pharma, 'antagonist')) = 1;
    opharma(strcmp(pharma, 'placebo')) = 2;
    opharma(strcmp(pharma, 'agonist')) = 3;
else
    error('Unknown coding');
end

% Subtract the mean
opharma = opharma - mean(opharma);

end


function Ep = prepare_pars(s)

Ep = s.Ep;

% Use the set of parameters: all - fixed_by_all
if isfield(s, 'pC')
    P = all((s.pC == 0)')';
else
    P = all((s.Cp == 0)')';
end

Ep(P, :) = [];

end