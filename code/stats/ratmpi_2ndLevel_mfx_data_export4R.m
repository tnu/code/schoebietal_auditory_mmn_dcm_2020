function [  ] = ratmpi_2ndLevel_mfx_data_export4R(ids, sf, options)
% [  ] = ratmpi_2ndLevel_mfx_data_export4R(ids, sf, options)
%
% Exports the data in an R suitable format for running the second level
% statistics on the ERPs
%
%
% IN
%   ids           cell           Cell of subject Identifiers
%
%   OPTIONAL:
%   sf           bool            Save (1) or don't save (0) stats
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

if ~isempty(ids)
    warning('The subject IDs are not used as an input argument atm.')
end

% Loading of the data
% -----------------------------------------------------------------------
options.pharma = 'all';
details = ratmpi_subjects('group_pharma', [], options);

sets = struct();
sets.cond = {'Standard', 'Deviant'};
sets.design = {options.design};
sets.Dfile = 'racefdfD'; 
sets.pharma = options.drugs; 
sets.timeWindow = options.fstLvl.window;

% Iterate over hemispheres (since different subjects can be used for each
% hemisphere) and compute statistics
% -----------------------------------------------------------------------
stats = struct();

for hemiCell = {'lhs', 'rhs'}
    options.hemisphere = char(hemiCell); 
    options = ratmpi_set_global_options(options); 
    sets.hemi = {options.hemisphere};
    channels = ratmpi_return_channels(char(hemiCell), options);
    
    % Load the data
    fprintf(['Loading the data for ' char(sets.hemi) '... \n \n']);
    [data, X] = ratmpi_return_dataMaster(...
        options.subjectIDs.pharma.lvl2.(options.altnames.design).(char(sets.hemi)), sets, options);
    
    % Split the structure into individual components, and save
    y1 = data.(char(channels(1))); 
    y2 = data.(char(channels(2)));
    id = X.(char(channels(1))).id; 
    cond = X.(char(channels(1))).cond; 
    pharma = X.(char(channels(1))).pharma; 
    design = X.(char(channels(1))).design;
    
    if sf
    if ~exist(details.path.stats.root)
        mkdir(details.path.stats.root);
    end
    save(fullfile(details.path.stats.root, ...
        ['group_pharma_2ndLevel_data_' options.altnames.design '_' options.hemisphere '.mat']), ...
        'y1', 'y2', 'id', 'cond', 'pharma', 'channels')
    end
end

