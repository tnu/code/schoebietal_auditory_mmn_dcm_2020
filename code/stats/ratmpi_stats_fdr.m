function [ stats ] = ratmpi_stats_fdr(stats, options)
% [ stats ] = ratmpi_stats_fdr(stats, options)
%
% Computes FDR corrected p-Values on a statistics structure containing
% stats.p
%
% IN
%   stats           string        Statistic structure as returned by any
%                                 of the statistic routines
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats           string        Statistic structure containing fdr
%                                 corrected statistics
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options();
end

% Set significance level
q = options.stats.lvl2.siglvl;

% Specify environment
% -----------------------------------------------------------------------
channels = {'lA1', 'rA1', 'lPAF', 'rPAF'};

for chanCell = channels
    chan = char(chanCell);
    
    p = getfield(stats, chan, 'p');
    
    for i = 1 : size(p, 2)
        [~, ~, ~, p_corr(:, i)]  = fdr_bh(p(:, i), q, 'dep');
    end
    
    stats = setfield(stats, chan, 'p_corr', p_corr);
    stats = setfield(stats, chan, 'corr', 'fdr'); 
end


end

