function [] = ratmpi_bma(ids, sf, options)
% [] = ratmpi_bma(ids, sf, options)
%
% Creates the input to spm_dcm_bma directly to perform the averaging.
%
% IN
%   ids          cell             list of ids
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Set up optional input.
if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

if ischar(ids)
    ids = {ids};
end

% Troubleshooting for different families
% if ~strcmp(options.dcm.bms.family, 'allModels')
%     error('This function is only implemented yet to use with all models');
% end

% Troubleshoot if one does the revision
if options.revision == 1
    load_model = @ratmpi_revision_load_winning_model;
else
    switch options.multistart
        case 'default'
            load_model = @ratmpi_vbsv_load_default_model;
        case 'best'
            load_model = @ratmpi_vbsv_load_winning_model;
    end
end

switch options.dcm.priors.neural
    case 'pdcm'
        models = {'49', '50', ...
     '51', '52', '53', '54', '55', '56', '57', '58', '59', '60', ...
     '61', '62', '63', '64'}; 
    otherwise
        models = options.dcm.models;
end

% Iterate over subject load DCMs and compute BMA over all models
i = 1;
j = 1;
for idCell = ids
    id = char(idCell);
    
    D = cell(1, length(options.dcm.models));
    
    for midCell = models
        mid = char(midCell);
        
        
        tmp = load_model(id, mid, options);
        
        D{1, j} = tmp;
        
        j = j + 1;
    end
    
    % Set the seed of the rng
    rng(220888); 
    BMA = ratmpi_spm_dcm_bma(D);
    
    s{i, j} = BMA;
    
    i = i + 1;
    j = 1;
    
    if sf
        details = ratmpi_subjects(id, mid, options);
        
        if ~exist(details.path.bma)
            mkdir(details.path.bma);
        end
        
        % Rename DCM to keep generic structure and compatibility with
        % other functions
        clear DCM;
        DCM = BMA;
        
        save(details.file.bma, 'DCM');
    end
    
end




