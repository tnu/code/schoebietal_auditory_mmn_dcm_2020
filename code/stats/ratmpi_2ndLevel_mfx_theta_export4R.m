function [s, X] = ratmpi_2ndLevel_mfx_theta_export4R(options)
% [] = ratmpi_2ndLevel_mfx_theta_export4R(id, options)
%
% Exports the BMAs to be later used in R.
%
% IN
%
%   OPTIONAL
%
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


% Set optional input
if nargin < 1
    options = ratmpi_set_global_options();
end

sf = 1;

% Allocate output structure
stats = struct();

ids = options.subjectIDs.pharma.lvl2.(options.altnames.design).xhs;

sets.fields = {'Ep', 'Cp'};
sets.varargin = {'A', 'B', 'G', 'T'};
sets.design = options.design;

% All drugs
sets.pharma = options.drugs;

% Use dosage coding
sets.coding = 'dosage';

% Set parameters according to options configuration
sets.hemi = {'lhs', 'rhs'};

% Use BMAs
sets.mids = {''};
sets.sv = 'bma';


% Run the summary Master to return all parameters of interest for all
% subjects, hemispheres and drugs
[s, X] = ratmpi_dcm_summaryMaster(ids, sets, options);

% Convert the pharma to double
idx = -2;
tmp = nan(size(X.pharma));
for pharmaCell = {'2mgScopo', '1mgScopo', 'Vehicle', '3mgPilo', '6mgPilo'}
    pharma = char(pharmaCell);
    
    tmp(strcmp(X.pharma, pharma)) = idx;
   
    idx = idx + 1;
end
X.pharma = tmp;

% Export
if sf
    Ep = s.Ep;
    id = X.id;
    pharma = X.pharma;
    
    options.pharma = 'allPharma';
    details = ratmpi_subjects('group_pharma', [], options);
    
    save([details.path.stats.lvl2.Ep, ...
        '/group_pharma_2ndLevel_' options.altnames.design '_Ep_data_bma_' options.multistart '.mat'], 'Ep', 'id', 'pharma');
end