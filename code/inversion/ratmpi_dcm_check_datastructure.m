function [ DCM ] = ratmpi_dcm_check_datastructure(DCM, options)
% [ DCM ] = ratmpi_dcm_check_datastructure(DCM, options)
% 
% Checks the Dimensionality of the starting values.
% 
% IN
%   DCM         struct           Prepared DCM strucutre
%
%   OPTIONAL:
%   options     struct           as returned by ratmpi_set_global_options()
% 
% OUT
%   DCM          struct           Estimated DCM files
%
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 2
    options = ratmpi_set_global_options();
end

% Check the dimensionality of the starting values
if isempty(DCM.M.hist.P)
    DCM.M.P = DCM.M.pE;
    disp('No Starting values assigned. Starting from prior means.');
else
    try
        spm_vec(DCM.M.pE) - spm_vec(DCM.M.P);
        disp('Starting Values loaded successfully');
    catch
        DCM.M.P = DCM.M.pE;
        DCM.M.hist.P = 'Failed assignment of starting values. Started from prior means.';
        warning(DCM.M.hist.P);
    end
end


% Only sample starting values of inverted parameters
vP = spm_vec(DCM.M.P);
vpC = spm_vec(DCM.M.pC);
vpE = spm_vec(DCM.M.pE);
vP(vpC == 0) = vpE(vpC == 0);
DCM.M.P = spm_unvec(vP, DCM.M.P);

end

