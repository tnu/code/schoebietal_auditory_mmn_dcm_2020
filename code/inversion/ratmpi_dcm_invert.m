function [ DCM, options] = ratmpi_dcm_invert(id, mid, vbsv, options)
% [ DCM, options ] = ratmpi_dcm_invert(id, smid, mid, vbsv, options)
% 
% Estimates a specified DCM for subject id, model mid and
% starting value vbsv
%
% IN
%   id      string      Subject Identifier (e.g. '0000')
%   mid     string      Model Identifier (e.g. '16')
%   vbsv    string      starting value identifier (e.g. '0')
%   
%   OPTIONAL
%   options struct      As returned by ratmpi_set_global_options
%
% OUT
%   DCM     struct      Estimated DCM structure
%   options struct      As returned by ratmpi_set_global_options
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 4
    options = ratmpi_set_global_options(); 
end

m = struct('mid', mid, 'vbsv', vbsv); 
    
% Setting the analysis options with the default pharmacology and specific
% starting value. Getting all the paths in details
details = ratmpi_subjects(id, m, options); 

% Setting up the diary and target folder
if options.dcm.save
    if ~exist(details.path.dcm)
        mkdir(details.path.dcm);
    end
%     diary(details.logs.ratmpi_dcm_invert);
end

% Preparing the DCM structure with the setting from 'options'
DCM = ratmpi_dcm_prep(id, m, options); 

% Estimating the model
DCM = ratmpi_dcm_erp(DCM);

if options.dcm.save
   save(details.file.dcm, 'DCM', 'options', 'details'); 
end

% diary OFF
    
end

