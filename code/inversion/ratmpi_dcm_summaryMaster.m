function [s, X] = ratmpi_dcm_summaryMaster(ids, sets, options)
% [ s, X ] = ratmpi_dcm_summaryMaster(ids, sets, options)
%
% Returns a summary over different factors and levels of sets, and
% summarizes them in the structure s. In detail, the function iterates over
% the fields in sets, load the corresponding DCM, and reads out the desired
% fields in the DCM structure.
%
% IN
%   ids          cell             Cell of subject IDs
%   sets         struct           Configurations (factors / levels) to run
%                                 ratmpi_dcm_summaryMaster
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   s            struct           Structure containing all the data
%   X            struct           'Design' specifying the columns of s
%
% ------------------------------------------------------------------------
%
% Possible factors for sets:
%
% Mandatory
%   .mids       cell      Model identifier, e.g. {'01', '04'}
%   .fields     cell      Field identifier of the DCM, e.g. {'Ep'};
%
% Optional
%   .design     cell      Design identifier, e.g. {'MMN_0.1', 'MMN_0.2'}
%   .hemi       cell      Hemisphere, e.g. {'lhs', 'rhs'}
%   .npriors    cell      Neuronal priors identifier, e.g. {'pX02'};
%   .fpriors    cell      Forward priors identifier, e.g. {'gX02'};
%   .Dfile      cell      Identifier for multistart Summaary
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end

if ~isfield(sets, 'mids')
    error('Please specify the desired model');
elseif ischar(sets.mids)
    sets.mids = {sets.mids};
end

if ~isfield(sets, 'design')
    disp(['Design set as in ratmpi_set_global_options: ' options.design]);
    sets.design = {options.design};
elseif ischar(sets.design)
    sets.design = {sets.design};
end

if ~isfield(sets, 'hemi')
    disp(['Hemisphere set as in ratmpi_set_global_options: ' options.hemisphere]);
    sets.hemi = {options.hemisphere};
elseif ischar(sets.hemi)
    sets.hemi = {sets.hemi};
end

if ~isfield(sets, 'pharma')
    disp(['Pharma set as in ratmpi_set_global_options: ' options.dcm.priors.neural]);
    sets.pharma = {options.pharma};
elseif ischar(sets.pharma)
    sets.pharma = {sets.pharma};
end

if ~isfield(sets, 'sv')
    disp(['Load top1 starting values']);
    sets.which_dcm = @ratmpi_vbsv_load_winning_model;
    sets.fileload = 'ratmpi_dcm_return';
elseif strcmp(sets.sv, 'best')
    disp(['Load top1 starting values']);
    sets.which_dcm = @ratmpi_vbsv_load_winning_model;
    sets.fileload = 'ratmpi_dcm_return';
elseif strcmp(sets.sv, 'default')
    disp(['Load default starting values']);
    sets.which_dcm = @ratmpi_vbsv_load_default_model;
    sets.fileload = 'ratmpi_dcm_return';
elseif strcmp(sets.sv, 'pE')
    disp(['Load default starting values']);
    sets.which_dcm = @ratmpi_vbsv_load_default_model;
    sets.fileload = 'ratmpi_dcm_return';
elseif strcmp(sets.sv, 'multistart')
    disp(['Load multistart summaries']);
    sets.which_dcm = @ratmpi_vbsv_multistartSummary;
    sets.fileload = 'ratmpi_vbsv_dcm_return';
elseif strcmp(sets.sv, 'bma')
    disp(['Load BMA']);
    sets.which_dcm = @ratmpi_vbsv_load_bma_model;
    sets.fileload = 'ratmpi_dcm_return';
else
    error('Undefined file');
end

if ischar(ids)
    ids = {ids};
end

% Initialize output
s = initialize_summary(sets);
X = initialize_design(sets);

for idCell =ids
    id = char(idCell);
    
    for midCell = sets.mids
        mid = char(midCell);
        
        for hemiCell = sets.hemi
            options.hemisphere = char(hemiCell);
            
            for designCell = sets.design
                options.design = char(designCell);
                
                
                for pharmaCell = sets.pharma
                    options.pharma = char(pharmaCell);
                    
                    options = ratmpi_set_global_options(options);
                    
                    DCM = load_which_dcm(id, mid, sets, options);
                    
                    s = fill_summary(DCM, s, sets, options);
                    X = fill_design(X, id, mid, ...
                        char(hemiCell), char(designCell), ...
                        char(pharmaCell));
                    
                    
                end
            end
        end
    end
end

% Convert design to a table
X.t = struct2table(X);
end

function DCM = load_which_dcm(id, mid, sets, options)

switch func2str(sets.which_dcm)
    case 'ratmpi_vbsv_load_winning_model'
        if options.revision
            DCM = ratmpi_revision_load_winning_model(id, mid, options);
        else
            DCM = ratmpi_vbsv_load_winning_model(id, mid, options);
        end
    case 'ratmpi_vbsv_load_default_model'
        DCM = ratmpi_vbsv_load_default_model(id, mid, options);
    case 'ratmpi_vbsv_multistartSummary'
        DCM = ratmpi_vbsv_multistartSummary(id, mid, [], 0, options);
    case 'ratmpi_vbsv_load_bma_model'
        DCM = ratmpi_vbsv_load_bma_model(id, '', options);
end

end

function s =  initialize_summary(sets)

s = struct();

for fieldCell = sets.fields
    fname = char(fieldCell);
    s  = setfield(s, fname, []);
end

end


function s =  fill_summary(DCM, s, sets, options)

for fieldCell = sets.fields
    fname = char(fieldCell);
    
    % Create function handle for the return of the field
    f_return = str2func([sets.fileload '_' fname]);
    
    stmp = getfield(s, fname);
    
    % Get the field of the DCM from the function ratmpi_dcm_return_*
    if ~isfield(sets, 'varargin')
        ftmp = f_return(DCM);
    else
        ftmp = f_return(DCM, sets.varargin);
    end
    
    stmp = [stmp, ftmp];
    
    s = setfield(s, fname, stmp);
    
end

end

function X = initialize_design(sets)

X = struct();
X.id = [];
X.mid = [];
X.hemi = [];
X.design = [];
X.pharma = [];

end


function X = fill_design(X, id, mid, hemi, design, pharma)

for fnameCell = fieldnames(X)'
    fname = char(fnameCell);
    
    X.(fname) = [X.(fname); {eval(fname)}];
    
end

end



