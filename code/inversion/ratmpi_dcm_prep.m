function [ DCM ] = ratmpi_dcm_prep(id, m, options)
% [ DCM ] = ratmpi_dcm_prep(id, m, options)
%
% Prepares the DCM structure for the inversion
%
% IN:
%   id          string      Subject Identifier (e.g. '0000')
%   mid         struct      Model Identifier
% OPTIONAL:
%   options     struct      Structure containing configurations
%
% OUTPUT:
%   DCM         struct      DCM structure containing all the settings
%                               for the inversion .
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end

[mid, vbsv, s] = ratmpi_unwrap_input(m, options);

% Load all the path information
details = ratmpi_subjects(id, m, options);


DCM = struct();
DCM.hist.id = id;
DCM.hist.m = m;
DCM.hist.rid = options.dcm.rid;
DCM.hist.datetime = char(datetime);
DCM.hist.spm_version = spm('version'); 
DCM.hist.matlab_version = version; 
DCM.hist.git = ratmpi_get_revision_info(options);


DCM.name = details.file.dcm;

DCM.options = ratmpi_dcm_config_options(id, m, options);
DCM.xY      = ratmpi_dcm_config_xY(id, m, options);
DCM.xU      = ratmpi_dcm_config_xU(id, m, options);
DCM.M       = ratmpi_dcm_config_M(id, m, options);
DCM         = ratmpi_dcm_config_input(DCM, options);
DCM         = prepare_remaining_structure(DCM);

DCM         = ratmpi_dcm_check_datastructure(DCM, options);

ratmpi_dcm_write_header(DCM, options)

% Creating the path to the directory
if options.dcm.save
    if ~exist(details.path.dcm)
        mkdir(details.path.dcm);
    end
end

end

function DCM = prepare_remaining_structure(DCM)
% -- Prepare all other structures ---------------------------------------
%   A
%   B
%   C
%   Sname
%   Lpos
%   name
A = cell(1, 3);
A{1} = [0 0; 1 0];
A{2} = [0 1; 0 0];
A{3} = [0 0; 0 0];

for i = 1 : size(DCM.M.pC.B, 2)
    B{i} = double(DCM.M.pC.B{i} ~= 0);
end

C    = [1; 0];
Sname= {'A1', 'PAF'};
Lpos = zeros(3, 0);


DCM.A = A;
DCM.B = B;
DCM.C = C;
DCM.Sname = Sname;
DCM.Lpos = Lpos;

end

