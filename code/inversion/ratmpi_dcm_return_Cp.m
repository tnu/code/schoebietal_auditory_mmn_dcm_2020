function [ Cp ] = ratmpi_dcm_return_Cp( DCM, whichFields )
% [ Cp ] = ratmpi_dcm_return_Cp( DCM )
%
% Returns the field Cp of an estimated DCM in the desired form to work with
% ratmpi_dcm_summaryMaster.
%
% IN
%   DCM           struct           Estimated DCM structure
%
% OUT
%   Cp             mat              Desired form of field of DCM
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 2
    whichFields = '';
end

if issparse(DCM.Cp)
    Cp = spm_vec(full(DCM.Cp));
    if ~isempty(whichFields)
        error('Undefined selection of fields for sparse Cp');
    end
elseif isstruct(DCM.Cp)
    
    if isempty(whichFields)
        Cp = spm_vec(DCM.Cp);
    else
        Cp = [];
        for fnameCell = whichFields
            fname = char(fnameCell);
            
            Cp = [Cp; spm_vec(DCM.Cp.(fname))];
        end
    end
end

end

