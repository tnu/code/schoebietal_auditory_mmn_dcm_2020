function [ pC ] = ratmpi_dcm_return_pC( DCM, whichFields )
% [ pC ] = ratmpi_dcm_return_pC( DCM, whichFields )
% 
% Returns the field pC of an estimated DCM in the desired form to work with
% ratmpi_dcm_summaryMaster.
% 
% IN
%   DCM           struct           Estimated DCM structure
%
% OPTIONAL:
%   whichFields    cell             Cell containing names of a subset of
%                                  fields
% OUT
%   pC             mat              Desired form of field of DCM
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 2
    whichFields = '';
end

if isempty(whichFields)
    pC = spm_vec(DCM.M.pC);
else
    pC = [];
    for fnameCell = whichFields
        fname = char(fnameCell);
        
        pC = [pC; spm_vec(DCM.M.pC.(fname))];
    end
end

end

