function [u] = ratmpi_erp_u(t, P, M)
% [ u ] = ratmpi_erp_u(t, P, M)
% 
% Greate a gamma pulse input to drive the DCM
% 
% IN
%   t           mat              Vector of timebins
%   P           struct           Parameter structure as in DCM.Ep
%   M           struct           Parameter structure as in DCM.M
%
%   OPTIONAL:
% 
% OUT
%   u           mat               Pulse input
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


try
    if length(M.dur) ~= length(M.ons)
        M.dur = M.dur(1) + M.ons - M.ons;
    end
catch
    M.dur = 32 + M.ons - M.ons;
end

% check sustained input (0,1)
%--------------------------------------------------------------------------
try
    if length(M.sus) ~= length(M.ons)
        M.sus = M.sus(1) + M.ons - M.ons;
    end
catch
    M.sus = 0 + M.ons - M.ons;
end

% stimulus � Gaussian (subcortical) impulse
%--------------------------------------------------------------------------
nu    = length(M.ons);
u     = sparse(length(t),nu);
dt    = t(2) - t(1);
t     = (t - dt) * 1000;
for i = 1:nu
    
    % Gamma bump function
    %----------------------------------------------------------------------
    % Rescale default parameters;
    delay  = M.ons(i) + 128*P.R(i,1);
    scale  = M.dur(i) * exp(P.R(i,2));    
    
    % Compute a and b parameters from mean and variance
    [a, b] = mv2ab(delay, scale);
    
    U      = gampdf(t, a, b);
    u(:,i) = U ./ trapz(t ./ 1000, U);
end


end

function [a, b] = mv2ab(mu, s2)
    a = mu^2 ./ s2 + 1;
    b = s2 ./ mu;
end

