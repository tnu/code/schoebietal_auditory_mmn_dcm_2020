function [ Ep ] = ratmpi_dcm_return_Ep( DCM, whichFields )
% [ Ep ] = ratmpi_dcm_return_Ep( DCM, whichFields )
%
% Returns the field Ep of an estimated DCM in the desired form to work with
% ratmpi_dcm_summaryMaster.
%
% IN
%   DCM           struct           Estimated DCM structure
%
% OPTIONAL:
%   whichFields    cell             Cell containing names of a subset of
%                                  fields
%
% OUT
%   Ep             mat              Desired form of field of DCM
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 2
    whichFields = '';
end

if isempty(whichFields)
    Ep = spm_vec(DCM.Ep);
else
    Ep = [];
    for fnameCell = whichFields
        fname = char(fnameCell);
        
        Ep = [Ep; spm_vec(DCM.Ep.(fname))];
    end
end
end

