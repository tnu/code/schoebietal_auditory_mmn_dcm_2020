function [] = ratmpi_dcm_write_header( DCM, options)
% [  ] = ratmpi_dcm_write_header(DCM, options)
% 
% Prints a header to the command window about the current inversion
% 
% IN
%   DCM          struct           DCM structure
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

id = DCM.hist.id;
m  = DCM.hist.m;
rid = DCM.hist.rid;

details = ratmpi_subjects(id, m, options);

fprintf('\n%s\n', ['---------------------------------------------------']);
fprintf('\n%s\n', ['Start of inverting a DCM for ' details.name.id]);
fprintf('\n%s\n', ['---------------------------------------------------']);

fprintf('%s\n', 'Setting for inversion are as follows: ...');

fprintf('\n%s\n',['Timestamp: ' DCM.hist.datetime]);
fprintf('%s\n', ['Subject: ' id]);
fprintf('%s\n', ['Model: ' m.mid]);
fprintf('%s\n', ['Run: ' rid]);
fprintf('%s\n', ['Architecture: ' DCM.options.model]);
fprintf('%s\n', ['Data: ' DCM.xY.Dfile]);
fprintf('%s\n', ['Savedir: ' DCM.name]);

fprintf('\n%s\n', ['Time Window: [' num2str(DCM.options.Tdcm) ']']);
fprintf('%s\n', ['Design: [' num2str(DCM.xU.X') ']']);

fprintf('\n%s\n', ['Simulator: ' DCM.M.IS]);
fprintf('%s\n', ['Integrator: ' DCM.M.int]);
fprintf('%s\n', ['Dynamics: ' DCM.M.f]);
fprintf('%s\n', ['Forward Model: ' DCM.M.FS]);
fprintf('%s\n', ['Leadfield Model: ' DCM.M.G]);

fprintf('\n%s\n', ['Maximum number of iteration: ' num2str(DCM.M.Nmax)]);
fprintf('%s\n', ['Multistart family: ' DCM.M.optim.multistart]);
fprintf('%s\n', ['Starting value: ' DCM.M.hist.P]);

fprintf('\n%s\n', ['Scaling Parameters: ' func2str(DCM.M.hist.pF)]);
fprintf('%s\n', ['Neuronal Priors: ' func2str(DCM.M.hist.pX)]);
fprintf('%s\n', ['Forward Priors: ' func2str(DCM.M.hist.gX)]);
fprintf('%s\n', ['Noise Priors: ' func2str(DCM.M.hist.hX)]);

fprintf('%s\n', ['Git Hash: ' DCM.hist.git.revhash]);


end

