function [Eh] = ratmpi_return_Eh(DCM)
% [ Eh ] = ratmpi_return_Eh(DCM)
%
% Returns the posterior noise estimation computed from the Noise KL
% expression in the expanded free energy term (DCM.L)
%
%
% IN
%   DCM          struct           Estimated DCM structure
%
%   OPTIONAL:
%
% OUT
%   Eh           mat              Posterior noise precison estimate
%
%__________________________________________________________________________
% Adapted:
% Translational Neuromodeling Unit (TNU) internal solution for estimating
% DCM for EEG.
%
% Dario Schöbi, Translational Neuromodeling Unit (TNU) 
% University of Zurich & ETH Zurich
%__________________________________________________________________________

%Noise Precision, prior mean (default 6)
hE = DCM.M.hE; 

%Noise Precision, prior variance (default 1 / 128)
hC = DCM.M.hC; 

%Precision weighted prediction error of noise
pwPE = -DCM.L(5); 

% pwPE = -(Eh - hE)^2 / (2 * hC)
Eh = hE - sqrt(-2 * DCM.L(5) * hC);

end

