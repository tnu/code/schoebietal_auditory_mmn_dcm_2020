function [ ] = ratmpi_vbsv_create_startingValues( options )
% [] = ratmpi_vbsv_create_startinValues(id, mid, vbsv, options)
% 
% Create starting values for the VB inversion for the neural parameters.
% Priors are loaded as specified in the prior settings defined in options, 
% i.e. options.dcm.priors.neural. 
%
% The resulting starting values folder is specified by the setting in
%   options.dcm.optim.multistart.
% If the folder already exists, the user gets a warning and control of the
% keyboard to prevent accidental overwrites.
% 
% IN
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 1
    options = ratmpi_set_global_options();
end

% Creating the function handles to the priors and loading
switch options.dcm.priors.neural
    case 'pX02'
        f = str2func(...
            ['ratmpi_dcm_m01_shs_priors_cmc_' options.dcm.priors.neural]);
    case {'pX03', 'pX04', 'pX05', 'pX06'}
        f = str2func(...
            ['ratmpi_dcm_m01_shs_priors_cmc_' options.altnames.design ...
            '_' options.dcm.priors.neural]);
end
[pE, pC] = f(); 

% Create directory for storing starting values
filepath = [options.rootdir ...
    '/dcm_config/vbsv/vbsv_' ...
    options.dcm.arch '_' options.dcm.optim.multistart];

if ~exist(filepath, 'dir')
    mkdir(filepath)
else
    disp('Warning: Folder already exists...');
    keyboard
end

% Creation of Starting values
% ------------------------------------------------------------------------

% Number of starting values
n = 100;

% Scaling factor (scales the prior variance to draw samples from)
scaling = 1;

vpE = full(spm_vec(pE));
vpC = full(diag(spm_vec(pC)));

for i = 0 : n
    
    % First starting value always corresponds to default
    if i == 0
        P = pE;
    else
        vP = mvnrnd(vpE, scaling .* vpC);
        P = spm_unvec(vP, pE);
        P.R = [0 0];
        P.B{1} = [0 0; 0 0];
    end
    save([filepath, '/vbsv_' num2str(i) '.mat'], 'P');
end



end

