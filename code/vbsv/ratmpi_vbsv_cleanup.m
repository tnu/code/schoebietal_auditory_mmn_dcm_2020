function [ l, r ] = ratmpi_vbsv_cleanup( id, mid, vbsv, options )
% [] = ratmpi_vbsv_cleanup(id, mid, vbsv, options)
% 
% Cleans up the multistart data. Specifically, it checks if a zip folder of
% the inversions exists. Then it runs multistart summary, to collect the
% important information. Then it removes the individual files for all but
% the default starting value, and the top 4.
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   mid          string/struct    Model identifier or object
%   vbsv         mat              Vector with starting values codes.
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   r            struct           Multistart summary
%   l            cell             Paths to files
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 4
    options = ratmpi_set_global_options();
end

% Paths and files
details = ratmpi_subjects(id, mid, options);

% Loads or creates the multistart Summary    
[r, l] = ratmpi_vbsv_multistartSummary(id, mid, vbsv, 1, options)

% Remove all unneeded files (they are still in the zip folder)
ratmpi_vbsv_remove_redundant(id, mid, options);

end

