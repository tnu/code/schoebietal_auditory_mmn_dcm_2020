function [varargout] = ratmpi_vbsv_load_default_model(id, mid, options)
% [] = ratmpi_vbsv_load_default_model(id, mid, options)
% 
% Loads the defautl model, i.e. the model corresponding to the default
% starting values (prior Means). 
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   mid          string/struct    Model identifier or object
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   DCM          struct           DCM structure
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options(); 
end

mid = struct('mid', mid, 'vbsv', '0');

details = ratmpi_subjects(id, mid, options);


if nargout == 1
    
    % Load winning model
    load(details.file.dcm, 'DCM');
    
    % Only return DCM, iff only one output argument is called
    varargout{1} = DCM; 
end

% Always return the second argument
varargout{2} = details.file.dcm; 


end

