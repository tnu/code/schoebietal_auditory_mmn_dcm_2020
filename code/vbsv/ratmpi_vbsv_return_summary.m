function [ res, l ] = ratmpi_vbsv_return_summary( l )
% [  ] = ratmpi_vbsv_return_summary( l )
%
% Returns a summary of all relevant values steming from a multistart
% approach. The elements are sorted according to the order in the file
% identifier list l. There is a flag, errorFileSize, below which files will
% be considered faulty. ADJUST TO YOUR NEEDS!
%
% IN:
%   l       cell        Cell with all ABSOLUTE paths to the files. 
%
% OUT:
%   res     struct      Summary of all results in a simple structure
%   l       cell        Same as input, but removed possible faulty
%                       inversions where the filesize did not exceed
%                       errorFileSize
%
% ------------------------------------------------------------------------
% EXAMPLE: 
%
%   There are multiple ways to specify the filepaths l. An easy is to use
%   the following commands.
%       1) go to directory of the files
%       2) l = dir('*.mat') % Assuming all mat files are vbsv files.
%       Otherwise, use a unique identifier of the vbsv files
%       3) l = strcat({l.folder}, '/', {l.name})';
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

try
    load(l{1});
catch
    load(l{2});
end

nb = size(DCM.ps.Cb, 1);
Nmax = DCM.M.Nmax;

warning('ADJUST ERRORFILESIZE TO YOUR NEEDS')
errorFileSize = 150E3;

vE = [];
F = [];
hE = [];
Ep = [];
Eg = [];
P = [];
KL = [];
A = [];
L = [];
eVar = [];
diagnostics.ip = [];
diagnostics.dF.pred = [];
diagnostics.dF.true = [];
Cb = zeros(nb, nb, size(l, 1));
Cr = zeros(nb, nb, size(l, 1));

i = 1;
j = 1;
k = []; 
while i <=  size(l, 1)
    file = dir(l{i});
    if ~isempty(file)
        if file.bytes > errorFileSize
            load(l{i});
            F = [F, DCM.F];
            hE = [hE, ratmpi_return_Eh(DCM)];
            vE = [vE, ratmpi_return_variance_explained(DCM)];
            Ep = [Ep, spm_vec(DCM.Ep) - spm_vec(DCM.M.pE)];
            Eg = [Eg, [spm_vec(DCM.Eg.L); DCM.Eg.J'] - [spm_vec(DCM.M.gE.L); DCM.M.gE.J']];
            P = [P, spm_vec(DCM.M.P) - spm_vec(DCM.M.pE)];
            KL = [KL, sum(DCM.L([2 3 4 5 8 9]))];
            A = [A, sum(DCM.L([1 6 7]))];
            Cb(:, :, j) = DCM.ps.Cb;
            Cr(:, :, j) = corrcov(1 / 2 .* ( DCM.ps.Cb + DCM.ps.Cb'));
            Vp(:, :, j) = full(spm_svd(diag(spm_vec(DCM.M.pC), 0)));
            Vg(:, :, j) = full(spm_svd(diag(spm_vec(DCM.M.gC), 0)));
            L = [L, DCM.L'];
            
            eVar = [eVar, var([DCM.R{1}; DCM.R{2}])'];
            
            diagnostics.ip = [diagnostics.ip, DCM.ps.pars];
            diagnostics.dF.pred = [diagnostics.dF.pred, padarray(DCM.ps.T.dF(1, :)', Nmax - DCM.ps.pars + 1, NaN, 'post')];
            diagnostics.dF.true = [diagnostics.dF.true, padarray(DCM.ps.T.dF(2, :)', Nmax - DCM.ps.pars + 1, NaN, 'post')];
            j = j + 1;
            i = i + 1;
        else
            disp(['Filesize too small: ' file.name]);
            k = [k, i]; 
            i = i + 1;
         
        end
    else
        disp(['Missing file: ' file.name])
        k = [k, i];
        i = i + 1;
    end
end

l(k) = []; 

res.vE = vE;
res.F = F;
res.KL = KL;
res.A = A;
res.hE = hE;
res.Ep = Ep;
res.Eg = Eg;
res.Cb = Cb;
res.Cr = Cr;
res.Vp = Vp;
res.Vg = Vg;
res.P = P;
res.L = L;
res.eVar = eVar;
res.l2 = return_l2Norms(Ep, P, F);
res.corr = return_correlation(Ep, P);
res.diagnostics = diagnostics;
res.idx = ratmpi_vbsv_sortModels(l, res);
res.l = l;

end


function l2 = return_l2Norms(Ep, P, F);

dP = zeros(size(P, 2));
dEp = zeros(size(P, 2));
dF = zeros(size(P, 2));

for i = 1 : size(P, 2)
    for j = 1 : size(P, 2)
        dP(i, j) = norm(P(:, i) - P(:, j));
        dEp(i, j) = norm(Ep(:, i) - Ep(:, j));
        dF(i, j) = (F(i) - F(j));
    end
end

l2.dP = dP;
l2.dEp = dEp;
l2.dF = dF;

end


function C = return_correlation(Ep, P)

C = zeros(1, size(P, 2));
for i = 1 : size(P, 2)
    C(i) = Ep(:, i)' * P(:, i) / (norm(Ep(:, i)) * norm(P(:, i)));
end

end