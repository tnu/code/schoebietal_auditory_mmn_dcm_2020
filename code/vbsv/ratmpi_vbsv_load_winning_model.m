function [varargout] = ratmpi_vbsv_load_winning_model(id, mid, options)
% [ varargout ] = ratmpi_vbsv_load_winning_model(id, mid, options)
% 
% Loads the winning model, i.e. the model corresponding to starting values
% resulting in the best (negative) free energy. Only useful if the model
% have actually been computed using the multistart approach.
% Note: 
% This method assumes that the multistart Summary has already been
% computed and saved. 
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   mid          string/struct    Model identifier or object
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   DCM                 struct           DCM structure
%   path2winningModel   path             Path to file
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options(); 
end

details = ratmpi_subjects(id, mid, options);

load(details.file.multistartSummary, 'r', 'l');

path2winningModel = return_filepath(details, l{r.idx.desc.top(1)});

if nargout == 1
    
    % Load winning model
    load(path2winningModel, 'DCM');
    
    % Only return DCM, iff only one output argument is called
    varargout{1} = DCM; 
end

% Always return the second argument
varargout{2} = path2winningModel; 


end


function path2winningModel = return_filepath(details, path2file)
% Load the file from current folder, since absolute filepaths don't work
% when the summaries have been created on the cluster.

[dir_path, file_name, file_ext] = fileparts(path2file);

path2winningModel = fullfile(details.path.dcm, [file_name file_ext]);


end

