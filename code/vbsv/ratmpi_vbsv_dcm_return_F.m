function [ F ] = ratmpi_vbsv_dcm_return_F( r )
% [ F ] = ratmpi_dcm_return_F( r )
% 
% Returns the field F of an estimated DCM under multistart from the
% multistart Summary in the desired form to work with 
% ratmpi_dcm_summaryMaster.
% 
% IN
%   r           struct              Multistart Summary file
% 
% OUT
%   F          mat                 Desired form of field of summary
%

F = r.F';

end

