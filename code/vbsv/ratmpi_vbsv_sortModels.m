function [ idx ] = ratmpi_vbsv_sortModels( l, r, options )
% [] = ratmpi_vbsv_sortModels(id, mid, options)
% 
% From a multistart inversion, it gets the indices of the inversions
% (files) in descending order, gets the indices of non converged models,
% the best 4 inversion and the default. 
% 
% IMPORTANT: 
%       -Specify r.diagnostics.ip
%       -Flag for idx.default
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   mid          string/struct    Model identifier or object
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options(); 
end

idx = struct(); 

% Sort the indices according to their Free Energy
[~, idx.desc.all] = sort(r.F, 'descend');

% Find models that did not converge in certain number of steps
idx.inc = find(r.diagnostics.ip == options.dcm.optim.Nmax); 

% Find model starting from default starting values
% Here, it will be the one ending vsbv_0.mat
idx.default = find(cellfun('length', strfind(l, 'vbsv0')))
idx.desc.i0 = find(idx.desc.all == idx.default);

% Find top 4 models
tmp = idx.desc.all;
idx.desc.top = tmp(1 : 4); 
    
end

