function [  ] = ratmpi_vbsv_remove_redundant(id, mid, options)
% [] = ratmpi_vbsv_remove_redundant(id, mid, options)
% 
% Part of the ratmpi_vbsv_cleanup routine. This function checks, if a zip
% file containing the multistart inversions and the multistart Summary have
% been created. Otherwise, it will return an error. 
% It will then remove all individual files from the multistart, except the
% default model and the top 4.
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   mid          string/struct    Model identifier or object
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end

details = ratmpi_subjects(id, mid, options);

% Make sure everything is in order, otherwise break
cd(details.path.dcm);
tmp = dir([details.name.id '*vbsv.tar.gz'])

if size(tmp, 1) < 1
    cd(options.rootdir);
    error('There is no zip file containing all the data. Deleting is not allowed ...');
else
    load(details.file.multistartSummary, 'r', 'l');
    
    for i = 1 : size(l, 1)
        if i == r.idx.default
            disp([l{i} ' is the default starting value'])
        elseif ismember(i, r.idx.desc.top)
            disp([l{i} ' is one of the top 4 starting values'])
        else
            delete(l{i});
        end
    end
    
    cd(options.rootdir);
    
end


end

