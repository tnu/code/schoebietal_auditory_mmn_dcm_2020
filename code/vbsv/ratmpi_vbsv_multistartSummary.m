function [r, l] = ratmpi_vbsv_multistartSummary(id, mid, vbsv, sf, options)
% [ r, l ] = ratmpi_vbsv_multistartSummary( id, mid, vbsv, options )
%
% Loads or creates the multistart summary for specified input and settings
% ratmpi_set_global_options
%
% IN
%   id           string           Subject identifier (e.g. '0001')
%   mid          string/struct    Model identifier or object
%   vbsv         mat              Vector with starting values codes. NOTE:
%                                 This will matter if the summary doesn't
%                                 exist yet.
%
%   OPTIONAL:
%   sf           bool             Save (1) or don't Save (0) summary
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% OUT
%   r            struct           multistart Summary
%   l            cell             paths to all vbsv files
%
% ------------------------------------------------------------------------
%
% EXAMPLE
% [r, l] =  ratmpi_vbsv_multistartSummary('27120', '08', [0:99], 1);
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Define optional input
switch nargin
    case [0, 1]
        error('not enough input arguments');
    case 2
        vbsv = [0 : 99];
        sf = 0;
        options = ratmpi_set_global_options();
    case 3
        if isempty(vbsv)
            vbsv = [0 : 99];
        end
        sf = 0;
        options = ratmpi_set_global_options();
    case 4
        if isempty(vbsv)
            vbsv = [0 : 99];
        end
        
        if isstruct(sf)
            error('Please initialize sf = 0 / 1'); 
        end
        
        options = ratmpi_set_global_options();
end

% Load Paths and files
details = ratmpi_subjects(id, mid, options);

if exist(details.file.multistartSummary)
    load(details.file.multistartSummary, 'l', 'r');
else
    l = return_list_of_path2files(id, mid, vbsv, options);
    [r, l] = ratmpi_vbsv_return_summary(l);
end

% Save summary
if sf
    save(details.file.multistartSummary, 'l', 'r');
end

end


function l = return_list_of_path2files(id, mid, vbsv, options)

l = cell(length(vbsv), 1);

for i = 1 : length(vbsv)
    m = struct('mid', mid, 'vbsv', num2str(vbsv(i)));
    details = ratmpi_subjects(id, m, options);
    l{i, 1} = details.file.dcm;
end

end