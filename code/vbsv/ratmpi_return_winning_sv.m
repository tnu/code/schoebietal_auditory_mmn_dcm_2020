function [s] = ratmpi_return_winning_sv(id, mid, options)
% [ s ] = ratmpi_return_winning_sv(id, mid, options)
%
% Returns the index of the winning model, i.e. the model corresponding to
% starting valuesresulting in the best (negative) free energy.
% Only useful if the model have actually been computed using the multistart approach.
% Note:
% This method assumes that the multistart Summary has already been
% computed and saved.
%
% IN
%   id           string           Subject identifier (e.g. '0001')
%   mid          string/struct    Model identifier or object
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   winningModelIndex   double      index of the winning starting value
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end

s = struct();

for idCell = options.subjectIDs.pharma.all
    id = char(idCell);
    
    for pharmaCell = options.drugs
        opt.pharma = char(pharmaCell);
        
        for hemiCell = {'lhs', 'rhs'}
            opt.hemisphere = char(hemiCell);
            
            % Reinitialize options because we changed drug and hemi
            options = ratmpi_set_global_options(opt);
            
            % Prepare a temporary vector to store the indices
            modelTmp = [];
            
            for midCell = options.dcm.models
                mid = char(midCell);
                
                % Get paths and files
                details = ratmpi_subjects(id, mid, options);
                
                if exist(details.file.multistartSummary)
                    DCM = ratmpi_vbsv_load_winning_model(id, mid, options);
                    
                    nid = ['id_' id];
                    m = ['m' mid];
                    
                    modelTmp = [modelTmp, {DCM.hist.m.vbsv}];
                    
                else
                    % Do nothing
                end
            end
            
            if ~isempty(modelTmp)                
                % Fill the starting values into the structure
                s.(nid).(options.altnames.pharma).(options.hemisphere) = ...
                    modelTmp;
            end
        end
    end
end
