function [xtick, xticklabel] = ratmpi_return_labelIndices(flabel, labels)
% [xtick, xticklabel] = ratmpi_return_labelIndices(flabel, labels)
% 
% Takes two cell arrays with labels, and returns the positions where 'labels'
% contains 'flabel'. This is used to create nice axes lablings with a
% reduced number of labels. 
% 
% IN
%   flabel       cell             Cell array with reduced labels, f.e.
%                                 {'A', 'R'}
%   labels       cell             Cell array with explicit labels, f.e.
%                                 {'A11', 'A12', 'A21', 'A22', 'R11'
%                                 ,'R12'}
%   
%   OPTIONAL:
% 
% OUT
%   xtick        mat               xticks of flabel
%   xticklabels  cell              xtick labels of flabel
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

labelIdx = []; 
for i = 1 : numel(labels)
    idx = find(contains(flabel, labels{i}), 1); 
    if ~isempty(idx)
        labelIdx = [labelIdx, idx];       
    else
        labelIdx = [labelIdx, NaN];
    end
end

[xtick, I] = sort(labelIdx, 'ascend'); 

xtick(isnan(xtick)) = []; 
I(isnan(xtick)) = []; 

xticklabel = labels(I);

end


