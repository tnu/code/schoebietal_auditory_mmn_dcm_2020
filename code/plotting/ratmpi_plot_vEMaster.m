function [s] = ratmpi_plot_vEMaster(ids, sets, options)
% [ s ] = ratmpi_plot_vEMaster(ids, sets, options)
%
% Creates plots of the variance Explained for multiple factors defined in
% sets and ids.
%
% IN
%   ids          cell             Cell of subject IDs
%   sets         struct           Configurations (factors / levels) to run
%                                 ratmpi_dcm_summaryMaster
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   argout       type
%
% ------------------------------------------------------------------------
%
% Possible factors for sets:
%
% Mandatory
%   .mids       cell      Model identifier, e.g. {'01', '04'}
%   .fields     cell      Field identifier of the DCM, e.g. {'Ep'};
%
% Optional
%   .design     cell      Design identifier, e.g. {'MMN_0.1', 'MMN_0.2'}
%   .hemi       cell      Hemisphere, e.g. {'lhs', 'rhs'}
%   .npriors    cell      Neuronal priors identifier, e.g. {'pX02'};
%   .fpriors    cell      Forward priors identifier, e.g. {'gX02'};
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end

% Default Settings for sets
if isempty(sets)
sets.fields = {'vE'};
sets.mids = options.dcm.models;
sets.design = options.design;
sets.pharma = {'Vehicle'}; %options.drugs;
sets.hemi = options.hemisphere;
sets.factor = 'pharma';
sets.sv = {'best'};
end

% Iterate over pharma and load only those
offs = zeros(1, length(sets.(sets.factor))); 
idx = 1;
for factorCell = sets.(sets.factor)
    tmp = sets;
    tmp.(sets.factor) = char(factorCell);
    
    % Run the summary Master to return all fields of interest
    [s, X] = ratmpi_dcm_summaryMaster(ids, tmp, options);
    
    % Create the plot of the Parameters
    vE = reshape_vE(s.vE, X, sets.mids);
    xx = [1 : length(sets.mids)] + offs(idx);
    
    subplot(1, length(sets.(sets.factor)),  idx)
    plotvE_multiplot(xx, vE, tmp, char(factorCell));
    config_plotvE(sets);
    
    idx = idx + 1;
end

config_figure(sets);

end


function plotvE_multiplot(x, vE, tmp, lvlName)
% Create barplot for free energy (fixed effect)

ff = mean(vE, 2);
h = bar(x, ff, 'Tag', lvlName);
hold on;

% Plot single dots for individual subjects
plot(vE, '.k');

end

function config_plotvE(sets)
% Config the figure using unique on the axes

g = gca;
handle_barplots = findobj(g, 'Type', 'bar');

levels = ratmpi_unique({handle_barplots.Tag});


cc = linspecer(length(sets.(sets.factor)));
cc = cc(strcmp(levels{:}, sets.(sets.factor)), :);

idx = 1;
for lvlCell = levels
    
    hline = findobj(handle_barplots, 'Tag', char(lvlCell));
    
    set(hline, 'FaceColor', cc(idx, :));
    set(hline, 'EdgeColor', cc(idx, :));
    
    idx = idx + 1;
end

g.XGrid = 'on';
g.YGrid = 'on';

axis square

end


function config_figure(sets)

% Configure figure
g = gcf;
g.Color = [1 1 1];

% Iterate over all subplots and configure axes
for i = 1 : size(g.Children, 1)
    h = g.Children(i);
    h.TickDir = 'out';
    
    h.XLim = [0, length(sets.mids) + 1];
    h.XTick = [1 : 2 : length(sets.mids)];
    h.XTickLabel = sets.mids(1 : 2 : length(sets.mids));
    h.XLabel.String = 'Models';
    
    hh = findobj(h, 'Type', 'bar'); 
    
    h.Title.String = hh.Tag;
    
end

% Only label the y axis once
h = g.Children(end);

h.YLabel.String = 'Variance Explained';
end

function vE = reshape_vE(vF, X, mids)
% Separates the free energy values for the models, and pools over all other
% variables

vE = []; 
for midCell = mids
    mid = char(midCell); 
    
    vE = [vE; vF(strcmp(X.mid, mid))]; 
end
        
end