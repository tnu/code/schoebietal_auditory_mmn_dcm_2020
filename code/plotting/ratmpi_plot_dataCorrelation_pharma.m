function r = ratmpi_plot_dataCorrelation_pharma(gid, sf, options)
% [ r ] = ratmpi_plot_dataCorrelation_pharma(gid, sf, options)
%
% Returns the data correlation over pharmacological manipulations
% (compared to the 2mgScopo condition)
%
% Factors: tone = {'Standard', 'Deviant'}
%          pharma = {'2mgScopo', '1mgScopo', 'Vehicle', '3mgPilo','6mgPilo}
%
% IN
%   gid           string           group_placebo / group_pharma
%
%   OPTIONAL:
%   sf           bool             Don't save (0) or save (1) figure.
%                                 (Default = 0);
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats        struct           Structure containing concatenated data
%                                 and statistics
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end


% Settings
[channels, ~] = ratmpi_return_channels(options.hemisphere, options);
ndesign = 'MMN01';
pharmalab = {'M--', 'M-', 'M0', 'M+', 'M++'};
cc = linspecer(5);

% Set up structure stats to run ratmpi_return_dataMaster
sets = struct();
sets.cond = {'Standard', 'Deviant'};
sets.design = {options.design};
sets.pharma = options.drugs;
sets.Dfile = 'prep';
sets.timeWindow = options.fstLvl.window;


% Set up subjects. Here, we need to be a little bit less careful, because
% we are plotting for lhs and rhs independently, thus we don't need to
% reload the structure.
ids = options.subjectIDs.pharma.lvl2.(ndesign).xhs;

% Load the data
[data, X] = ratmpi_return_dataMaster(ids, sets, options);

% Iterate over ids, conditions and channels and reorganise the data
fidx = 1;
for chanCell = channels
    chan = char(chanCell);
    
    r{1} = []; r{2} = []; r{3} = [];
    
    for idCell = ids
        id = char(idCell);
        
        y{1} = []; y{2} = [];
        
        cidx = 1;
        for condCell = sets.cond
            cond = char(condCell);
            
            % Get the design matrix for this channel
            XX = X.(chan);
            
            % Get the indices of the factors
            iX = strcmp(XX.id, id) & strcmp(XX.cond, cond);
            
            % Get the data
            y{cidx} = data.(chan)(iX, :);
            
            cidx = cidx + 1;
        end
        
        % Get the correlations for standard, deviant and MMN
        tmp = corrcoef(y{1}');
        r{1} = [r{1}; tmp(1, :)];
        
        tmp = corrcoef(y{2}');
        r{2} = [r{2}; tmp(1, :)];
        
        tmp = corrcoef((y{2}-y{1})');
        r{3} = [r{3}; tmp(1, :)];
        
    end
    
    
    subplot(3, 2, fidx);
    plot_correlation(r{1}', pharmalab, 'Standard');
    plot_trend(r{1}');
    
    subplot(3, 2, fidx + 2);
    plot_correlation(r{2}', pharmalab, 'Deviant');
    plot_trend(r{2}');
    
    subplot(3, 2, fidx + 4);
    plot_correlation(r{3}', pharmalab, 'MMN');
    plot_trend(r{3}');
    
    r = [];
    cidx = cidx + 1;
    fidx = fidx + 1;
    
end

end


function plot_trend(r)

tstat = 2.447;

errorbar(mean(r'), tstat * std(r')/sqrt(size(r, 2)), 'k-', 'LineWidth', 2)

end

function plot_correlation(r, pharma, cond)

plot(r, '.-', 'LineWidth', 1);
hold on;
set(gca, ...
    'PlotBoxAspectRatio', [1 1 1], ...
    'YGrid', 'on', 'XGrid', 'on');

g = gca;
g.Title.String = cond;
g.XLabel.String = 'Pharma';
g.YLabel.String = 'correlation';
g.XTick = [1:length(pharma)];
g.XTickLabel = pharma;
g.XLim = [-0.25, +0.25] + [min(g.XTick), max(g.XTick)];

end
