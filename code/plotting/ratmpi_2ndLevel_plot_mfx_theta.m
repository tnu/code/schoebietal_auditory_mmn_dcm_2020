function [ stats ] = ratmpi_2ndLevel_plot_mfx_theta(stats)
% [ stats ] = ratmpi_2ndLevel_plot_mfx_theta(stats)
%
% Plots the mixed effect model statistics on the parameters
%
% IN
%   stats       struct       Plots the mixed effect models statistics,
%                            returned from the R scripts
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


sets.varargin = {'A', 'B', 'G', 'T'};

% Code all fixed parameters as nan's to keep figure nice and clearn
stats = prepare_pars(stats);

% Prepare plot settings
sets.nTests = sum(~isnan(stats.ffx.pValues));

if sets.nTests ~= 18
    warning('Are you sure the MC correction is correct? Expected 18 Parameters.'); 
end

sets.pThreshold = 0.05;
xx = [1 : size(stats.Y, 2)];

% Add the 

% Plot the Grand Mean
subplot(2, 2, 1);
bar(xx, stats.ffx.beta(:, 1)); 

% Plot the random effect intercepts
subplot(2, 2, 3);
plot(xx, stats.rfx.intercepts, '.');

% Plot the fixed effect beta and stats
subplot(2, 2, [2, 4]); 
yyaxis left
bar(xx, stats.ffx.beta(:, 2:end), 'Tag', 'beta'); 

yyaxis right
hold on;
% Bonferoni Correction
idx = sets.nTests * stats.ffx.pValues < sets.pThreshold;
bar(xx(idx), sets.nTests * stats.ffx.pValues(idx), 'Tag', 'pVal'); 
plot([xx(1)-2, xx(end)+2], sets.pThreshold * [1 1], '--r'); 


config_average(sets)
config_rfx(sets)
config_stats(sets);

g = gcf;
g.Color = [1 1 1];


end

function stats = prepare_pars(stats)
% A parameter is considered not to be in the model, if the pValue is
% EXACTLY 1

idx = stats.ffx.pValues == 1; 

stats.ffx.beta(idx, 1) = NaN;
stats.ffx.beta(idx, 2) = NaN;
stats.rfx.beta(idx) = NaN;
stats.ffx.pValues(idx) = NaN;
stats.rfx.intercepts(idx, :) = NaN;

end

function config_average(sets)

% % Configure the plots
g = subplot(2, 2, 1);
g.YGrid = 'on';
g.XGrid = 'on';
create_labels(sets);
g.YLim = [-3, 3];
title('Grand Average')

end

function config_rfx(sets)

g = subplot(2, 2, 3);
g.YGrid = 'on';
g.XGrid = 'on';
create_labels(sets);
g.YLim = [-3, 3];
title('Random Effect intercepts')

end

function config_stats(sets)

g = subplot(2, 2, [2 4]);
axis square

yyaxis left
h = findobj(g, 'Tag', 'beta');
set(h, 'FaceColor', 0.1 * ones(1, 3));
set(h, 'EdgeColor', 'none'); 
g = gca;
g.YLim = max(abs(g.YLim)) * [-1 1];
g.YLabel.String = 'Estimates';
g.YColor = 0.1*ones(1, 3);

yyaxis right
h = findobj(g, 'Tag', 'pVal');

if ~isempty(h.YData)
ymin = 1.1 * min(log10(h.YData));
h.BarWidth = 0.2;
h.BaseValue = 10^(ymin);
h.FaceColor = [1 0 0];
h.EdgeColor = 'none';
g = gca; 
g.YColor = [1 0 0];
g.YScale = 'log';
g.YLabel.String = 'p Values (Bonferoni corrected)';
g.YLim = [10^(2 * ymin), 1]; 
% Set the axis ticks
g.YTick = 10.^[ceil(ymin): 0];
g.YMinorTick = 'off'; 
end

h = findobj(g, 'Type', 'line'); 
g.YGrid = 'on';
g.XGrid = 'on';
g.XLim = [h.XData(1), h.XData(2)];
create_labels(sets);

title('Estimates and Statistics')

end

function create_labels(sets)

if ~isfield(sets, 'varargin')
    [~, xTicks, xTickLabels] = ratmpi_return_names_cmc();
else
     [~, xTicks, xTickLabels] = ratmpi_return_names_cmc(sets.varargin);
end

g = gca; 
g.XTick = xTicks;
g.XTickLabel =  xTickLabels;
g.TickDir = 'out'; 

end


