function [s] = ratmpi_plot_FMaster(ids, sets, options)
% [ s ] = ratmpi_plot_FMaster(ids, sets, options)
%
% Creates plots of the free energy for multiple factors defined in
% sets and ids.
%
% IN
%   ids          cell             Cell of subject IDs
%   sets         struct           Configurations (factors / levels) to run
%                                 ratmpi_dcm_summaryMaster
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   argout       type
%
% ------------------------------------------------------------------------
%
% Possible factors for sets:
%
% Mandatory
%   .mids       cell      Model identifier, e.g. {'01', '04'}
%   .fields     cell      Field identifier of the DCM, e.g. {'Ep'};
%
% Optional
%   .design     cell      Design identifier, e.g. {'MMN_0.1', 'MMN_0.2'}
%   .hemi       cell      Hemisphere, e.g. {'lhs', 'rhs'}
%   .npriors    cell      Neuronal priors identifier, e.g. {'pX02'};
%   .fpriors    cell      Forward priors identifier, e.g. {'gX02'};
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end

singlePlot = 0;

% Default Settings for sets
if isempty(sets)
    sets.fields = {'F'};
    sets.mids = options.dcm.models;
    sets.design = options.design;
    sets.pharma = options.drugs;
    sets.hemi = {'lhs', 'rhs'};
    sets.factor = 'pharma';
    sets.bms = 'rfx_pxp';
end

% Iterate over pharma and load only those
if ~singlePlot
    offs = zeros(length(sets.(sets.factor)));
else
    offs = [0 : length(sets.(sets.factor)) - 1] * (length(sets.mids) + 2);
end

idx = 1;
for factorCell = sets.(sets.factor)
    tmp = sets;
    tmp.(sets.factor) = char(factorCell);
    
    % Run the summary Master to return all fields of interest
    [s, X] = ratmpi_dcm_summaryMaster(ids, tmp, options);
    
    % Create the plot of the Parameters
    F = reshape_F(s.F, X, sets.mids);
    xx = [1 : length(sets.mids)] + offs(idx);
    
    if ~singlePlot
        subplot(1, length(sets.(sets.factor)),  idx)
        plotF_multiplot(xx, F, tmp, char(factorCell));
        
        config_plotF(sets);
    else
        plotF_multiplot(xx, F, tmp, char(factorCell));
    end
    %
    
    idx = idx + 1;
end

if ~singlePlot
    config_figure(sets);
else
    config_singleFigure(sets);
end


end


function plotF_multiplot(x, F, tmp, lvlName)
% Create barplot for free energy (fixed effect)

switch tmp.bms
    case 'ffx'
        ff = sum(F, 2);
    case 'rfx_exp_r'
        [~, ff] = spm_BMS(F');
    case 'rfx_xp'
        [~, ~, ff] = spm_BMS(F');
    case 'rfx_pxp'
        [~, ~, ~, ff] = spm_BMS(F');
end

if strcmp(tmp.bms, 'ffx')
    h = bar(x, ff, 'BaseValue', min(ff), 'Tag', lvlName);
    hold on;
else
    h = bar(x, ff, 'Tag', lvlName);
    hold on;
end

end

function config_plotF(sets)
% Config the figure using unique on the axes

g = gca;
handle_barplots = findobj(g, 'Type', 'bar');

levels = ratmpi_unique({handle_barplots.Tag});


cc = linspecer(length(sets.(sets.factor)));
cc = cc(strcmp(levels{:}, sets.(sets.factor)), :);

idx = 1;
for lvlCell = levels
    
    hline = findobj(handle_barplots, 'Tag', char(lvlCell));
    
    set(hline, 'FaceColor', cc(idx, :));
    set(hline, 'EdgeColor', cc(idx, :));
    
    idx = idx + 1;
end

g.XGrid = 'on';
g.YGrid = 'on';

axis square

end


function config_figure(sets)

% Configure figure
g = gcf;
g.Color = [1 1 1];

% Iterate over all subplots and configure axes
for i = 1 : size(g.Children, 1)
    h = g.Children(i);
    h.TickDir = 'out';
    
    h.XLim = [0, length(sets.mids) + 1];
    h.XTick = [1 : 2 : length(sets.mids)];
    h.XTickLabel = sets.mids(1 : 2 : length(sets.mids));
    h.XLabel.String = 'Models';
    h.Title.String = h.Children.Tag;
    
    if ~strcmp(sets.bms, 'ffx')
    h.YLim = [0, 1];
    end
    
end

% Only label the y axis once
h = g.Children(end);

switch sets.bms
    case 'ffx'
        h.YLabel.String = 'Free Energy';
    case 'rfx_exp_r'
        h.YLabel.String = 'Expected Probability';
    case 'rfx_xp'
        h.YLabel.String = 'Exceedance Probability';
    case 'rfx_pxp'
        h.YLabel.String = 'Protected Exceedance Probability';
end

end


function config_singleFigure(sets)

% Config Figure
g = gcf;
g.Color = [1 1 1];

% Get color code
cc = linspecer(length(sets.(sets.factor)));

% Get placeholder for x label
xx = [];

idx = 1;
for factorCell = sets.(sets.factor)
    factor = char(factorCell);
    
    gg = g.Children(1);
    
    h = findobj(gg, 'Type', 'bar', 'Tag', factor);
    h.EdgeColor = 'none';
    h.FaceColor = cc(idx, :);
    
    xx = [xx, h.XData];
    
    idx = idx + 1;
end

h = gca;
xLabel = repmat(sets.mids, 1, length(sets.(sets.factor)));
h.TickDir = 'out';
h.XTick = xx(1 : 4 : end);
h.XTickLabel = xLabel(1 : 4 : end);
h.XGrid = 'on';
h.YGrid = 'on';

% l = legend(sets.(sets.factor));
% l.Box = 'off';

% h.XLabel.String = 'Models';
% h.YLabel.String = '(negative) Free Energy';


end

function F = reshape_F(vF, X, mids)
% Separates the free energy values for the models, and pools over all other
% variables

F = [];
for midCell = mids
    mid = char(midCell);
    
    F = [F; vF(strcmp(X.mid, mid))];
end

end