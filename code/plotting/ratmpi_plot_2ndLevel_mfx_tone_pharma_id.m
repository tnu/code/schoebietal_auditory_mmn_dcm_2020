function [ stats ] = ratmpi_plot_2ndLevel_mfx_tone_pharma_id( gid, sf, options )
% [ stats ] = ratmpi_plot_2ndLevel_mfx_tone_pharma_id(gid, sf, options)
%
% All means displayed are averaged signals (over trials), then averaged
% over subjects. Shaded areas depict SEM (over subjects).
%
% Alternatively, one can illustrate the average over all trials and
% subjects, by setting sets.Dfile = 'racefdfD';
%
% Left column (5 rows) shows standard and deviants in A1, for all
% pharmacological levels.
% Right column (5 rows) shows standard and deviants in PAF, for all
% pharmacological levels
% Middle column shows a different ordering of left and right, showing
% all standards in a single subplot, all deviants in a single subplot, all
% MMNs
%
% Statistics correspond to a full conjunction analysis on the first level
% corrected (fdr) statistics.
%
% Factors: tone = {'Standard', 'Deviant'}
%          pharma = {'2mgScopo', '1mgScopo', 'Vehicle', '3mgPilo','6mgPilo}
%
% See ./examples/ for more information.
%
% IN
%   gid           string           group_placebo / group_pharma
%
%   OPTIONAL:
%   sf           bool             Don't save (0) or save (1) figure.
%                                 (Default = 0);
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats        struct           Structure containing concatenated data
%                                 and statistics
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end

[channels, ~] = ratmpi_return_channels(options.hemisphere, options);

% Set up handle for group subject ids handle .(gid).grandAverage
switch gid
    case 'group_placebo'
        gidname = 'placebo';
    case 'group_pharma'
        gidname = 'pharma';
end

switch options.design
    case 'MMN_0.1'
        ndesign = 'MMN01';
    case 'MMN_0.2'
        ndesign = 'MMN02';
    otherwise
        error('Design not specified');
end

ileft = [1, 5, 9, 13, 17];
iright = [4 8 12 16 20];
imiddle =[2; 3; 6; 7; 10; 11; 14; 15];
nl = 5;
nc = 4;

% This global variable is to make configuring the plots later much easier
global POPTIONS
POPTIONS = struct();
POPTIONS.ileft = ileft;
POPTIONS.cleft = ileft(end);
POPTIONS.iright = iright;
POPTIONS.cright = iright(end);
POPTIONS.imiddle = imiddle;
POPTIONS.nl = nl;
POPTIONS.nc = nc;
POPTIONS.drugs = options.drugs;
POPTIONS.cc = linspecer(5);
POPTIONS.channels = channels;
POPTIONS.threshold = 0.05;

% Set up structure stats to run ratmpi_return_dataMaster
sets = struct();
sets.cond = {'Standard', 'Deviant'};
sets.design = {options.design};
sets.pharma = options.drugs;
sets.Dfile = 'prep';
% sets.Dfile = 'racefdfD';
sets.timeWindow = options.fstLvl.window;

% Set up subjects. Here, we need to be a little bit less careful, because
% we are plotting for lhs and rhs independently, thus we don't need to
% reload the structure.
ids = options.subjectIDs.pharma.lvl2.(ndesign).(options.hemisphere);

% Load the data
[data, X] = ratmpi_return_dataMaster(ids, sets, options);

% Load the statistics
details = ratmpi_subjects(gid, [], options);
stats = load_stats(gid, details, options);


i = 1;
for pharmaCell = options.drugs
    options.pharma = char(pharmaCell);
    
    subplot(nl, nc, ileft(i));
    hold on;
    plot_tone(data.(channels{1}), X.(channels{1}), options.pharma, options);
    config_axes()
    
    subplot(nl, nc, iright(i));
    hold on;
    plot_tone(data.(channels{2}), X.(channels{2}), options.pharma, options);
    config_axes()
    
    subplot(nl, nc, imiddle(1, :));
    hold on;
    plot_drug(data.(channels{1}), X.(channels{1}), options.pharma, 'Standard', options);
    
    subplot(nl, nc, imiddle(2, :));
    hold on;
    plot_drug(data.(channels{2}), X.(channels{2}), options.pharma, 'Standard', options);
    
    subplot(nl, nc, imiddle(3, :));
    hold on;
    plot_drug(data.(channels{1}), X.(channels{1}), options.pharma, 'Deviant', options);
    
    subplot(nl, nc, imiddle(4, :));
    hold on;
    plot_drug(data.(channels{2}), X.(channels{2}), options.pharma, 'Deviant', options);
    
    subplot(nl, nc, imiddle(5, :));
    hold on;
    plot_mmn(data.(channels{1}), X.(channels{1}), options.pharma, options);
    
    subplot(nl, nc, imiddle(6, :));
    hold on;
    plot_mmn(data.(channels{2}), X.(channels{2}), options.pharma, options);
        
    i = i + 1;
end

subplot(nl, nc, imiddle(7, :));
hold on;
plot_stats(stats.(channels{1}).ffx, options);

subplot(nl, nc, imiddle(8, :));
hold on;
plot_stats(stats.(channels{2}).ffx, options);


config_lhs_rhs();
config_middle();


gf = gcf;
gf.Color = [1 1 1];


clear POPTIONS;

if sf
    ratmpi_printFig(details.fig.stats.lvl2.mfx.tone_pharma_id, options);
end

end

function plot_tone(data, X, pharma, options)

% x (time) axis in the figures
timeWindow = options.fstLvl.window;
pst = [timeWindow(1) :  1/options.preproc.downsamplefreq : timeWindow(2)];

for condCell = {'Standard', 'Deviant'}
    cond = char(condCell);
    
    fact = strcmp(X.cond, cond) & strcmp(X.pharma, pharma);
    D = data(fact, :);
    
    meanData = mean(D, 1);
    semData = std(D, 1) / sqrt(size(D, 1));
    
    meanPlusSem = meanData + semData;
    meanMinusSem = meanData - semData;
    
    [hp, hl] = mpdcm_shaded_area_plot(pst, meanData', meanPlusSem', meanMinusSem');
    
    config_shaded_area_plot(hl, hp, cond, pharma)
    
end



end


function plot_drug(data, X, pharma, cond, options)

% x (time) axis in the figures
timeWindow = options.fstLvl.window;
pst = [timeWindow(1) :  1/options.preproc.downsamplefreq : timeWindow(2)];

fact = strcmp(X.cond, cond) & strcmp(X.pharma, pharma);
D = data(fact, :);

meanData = mean(D, 1);
semData = std(D, 1) / sqrt(size(D, 1));

meanPlusSem = meanData + semData;
meanMinusSem = meanData - semData;

[hp, hl] = mpdcm_shaded_area_plot(pst, meanData', meanPlusSem', meanMinusSem');
config_shaded_area_plot(hl, hp, cond, pharma);

end

function plot_stats(stats, options)

global POPTIONS

% x (time) axis in the figures
timeWindow = options.fstLvl.window;
pst = [timeWindow(1) :  1/options.preproc.downsamplefreq : timeWindow(2)];

hl = plot(pst, stats.p_fdr, 'Tag', 'pValues');
ht = plot([pst(1), pst(end)], POPTIONS.threshold * [1, 1], 'Tag', 'threshold');

% Get timepoints of significant statistics
barStats = 1 .* (stats.p_fdr < POPTIONS.threshold); 
barStats(barStats == 0) = NaN;

% Rescale for plotting
barStats = barStats .* repmat([0.004, 0.011, 0.018], size(barStats, 1), 1);
hb = plot(pst, barStats, 'LineWidth', 10, 'Tag', 'pValues_bar'); 



config_stats(hl, hb, ht);

l = legend([hl(1), hl(2), hl(3)], stats.name);
l.Box = 'off';

end

function config_stats(hl, hb, ht)

cc = diag([0.1 0.3 0.5])' * ones(3, 3);
lineSt = {':', '-.', '--', '-', '-', '-'};
for i = 1 : size(hl, 1)
    hl(i).Color = cc(i, :);
    hb(i).Color = cc(i, :);
    hl(i).LineWidth = 1;
    hl(i).LineStyle = '-'; %lineSt{i};
end

set(hl, 'Visible', 'on'); 
ht.Color = [1 0 0];
ht.LineWidth = 1;
ht.LineStyle = '-';
ht.Visible = 'on';



end


function config_shaded_area_plot(hl, hp, cond, pharma)

cc = return_color(pharma);

% Config Line plot
hl.Color = cc;
hl.LineWidth = 1;
switch cond
    case 'Deviant'
        hl.LineStyle = '--';
    case 'Standard'
        hl.LineStyle = '-';
end

% Config Patches plot
hp.FaceAlpha = 0.3;
hp.FaceColor = cc;
hp.EdgeColor = 'none';

end


function plot_mmn(data, X, pharma, options)

% x (time) axis in the figures
timeWindow = options.fstLvl.window;
pst = [timeWindow(1) :  1/options.preproc.downsamplefreq : timeWindow(2)];

fact_std = strcmp(X.cond, 'Standard') & strcmp(X.pharma, pharma);
fact_dev = strcmp(X.cond, 'Deviant') & strcmp(X.pharma, pharma);
D_std = data(fact_std, :);
D_dev = data(fact_dev, :);

meanData = mean(D_dev, 1) - mean(D_std, 1);

hl = plot(pst, meanData');
config_plot_mmn(hl, pharma);

end

function config_plot_mmn(hl, pharma)

cc = return_color(pharma);

% Config Line plot
hl.Color = cc;
hl.LineStyle = '-';
hl.LineWidth = 2;

end

function config_axes()

h = gca;
h.XColor = 'none';

h.TickDir = 'out';
h.YGrid = 'on';
h.XGrid = 'on';
h.GridColorMode = 'manual';

h.YLim = [-80, 80];
h.YTick = linspace(-50, 50, 5);
end



function config_lhs_rhs()

global POPTIONS

ileft = POPTIONS.ileft;
nl = POPTIONS.nl;
nc = POPTIONS.nc;

h = subplot(nl, nc, 1);
h.Title.String = POPTIONS.channels{1};

h = subplot(nl, nc, nc);
h.Title.String = POPTIONS.channels{2};

h = subplot(nl, nc, POPTIONS.cleft);
h.XColor = [0 0 0];
h.XLabel.String = 'Time [s]';

h = subplot(nl, nc, POPTIONS.cright);
h.XColor = [0 0 0];
h.XLabel.String = 'Time [s]';

for i = 1 : size(ileft, 2)
    h = subplot(nl, nc, ileft(i));
    h.YLabel.String = POPTIONS.drugs{i};
end

h = subplot(nl, nc, ileft(1));
l = legend([h.Children(3), h.Children(1)], 'standard', 'deviant');
l.Box = 'off';

end


function config_middle()

global POPTIONS

ileft = POPTIONS.ileft;
imiddle = POPTIONS.imiddle;
nl = POPTIONS.nl;
nc = POPTIONS.nc;

pos = [0.336, 0.723; ...
    0.542, 0.723; ...
    0.336, 0.519; ...
    0.542, 0.519; ...
    0.336, 0.3; ...
    0.542, 0.3];

g = gcf;

titleString = {'Standard', 'Standard', ...
    'Deviant', 'Deviant', ...
    'Difference Wave', 'Difference Wave'};


for i = [5 6 3 4 1 2]
    h = subplot(nl, nc, imiddle(i, :));
    
    % Settings for all subplots
    h.YLim = [-60 60];
    h.YTick = linspace(-50, 50, 5);
    h.TickDir = 'out';
    h.YGrid = 'on';
    h.XGrid = 'on';
    h.XLabel.String = 'Time [s]';
    h.XColor = 'none';
    
    if mod(i, 2)~=0
        h.Title.String = [titleString{i} ' '  POPTIONS.channels{1}];
    else
        h.Title.String = [titleString{i} ' '  POPTIONS.channels{2}];
    end
    
end

% Display x-Axis on statistics plot
h = subplot(nl, nc, imiddle(end -1));
h.YLim = [0 0.06];
h.XColor = [0 0 0];
title(['Statistics ' POPTIONS.channels{1}])

h = subplot(nl, nc, imiddle(end));
h.XColor = [0 0 0];
h.YLim = [0 0.06];
title(['Statistics ' POPTIONS.channels{2}])

end

function cc = return_color(pharma)

global POPTIONS
cc = POPTIONS.cc;

% Specify colors
switch pharma
    case '2mgScopo'
        cc = cc(1, :);
    case '1mgScopo'
        cc = cc(2, :);
    case 'Vehicle'
        cc = cc(3, :);
    case '3mgPilo'
        cc = cc(4, :);
    case '6mgPilo'
        cc = cc(5, :);
end

end


function stats = load_stats(gid, details, options)

s = load(fullfile(details.path.stats.lvl2.mfx.tone_pharma_id, ...
    [gid '_2ndLevel_data_' options.altnames.design '_' options.hemisphere '_stats.mat'])); 

% Reorganize the structure
channels = ratmpi_return_channels(options.hemisphere);

stats = struct();
stats.(channels{1}).ffx = s.A1_ffx1; 
stats.(channels{1}).rfx = s.A1_rfx1; 
stats.(channels{2}).ffx = s.PAF_ffx2; 
stats.(channels{2}).rfx = s.PAF_rfx2; 
stats.(channels{1}).ffx.name = {'Tone', 'Pharma', 'Tone * Pharma'};
stats.(channels{2}).ffx.name = {'Tone', 'Pharma', 'Tone * Pharma'}; 

% FDR Correction on stats
[stats.(channels{1}).ffx.p_fdr] = ratmpi_fdr(stats.(channels{1}).ffx.pValues);
[stats.(channels{2}).ffx.p_fdr] = ratmpi_fdr(stats.(channels{2}).ffx.pValues);

end