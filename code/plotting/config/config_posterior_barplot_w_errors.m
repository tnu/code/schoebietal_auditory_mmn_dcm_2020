function [ output_args ] = config_posterior_barplot_w_errors( h )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    
global Epnames

    for i = 1 : size(h, 2)

        b = h(i);
        b.FaceColor = config_barColor(i); 
        b.EdgeColor = 'none';
        b.BarWidth = 0.05;
        %b.BarWidth = 0.3;
        
        axTick(i) = b.XData(end);
        
    end
    set(gcf, 'Position', get(0, 'Screensize'));
    ax = b.Parent;
    ax.XTick = axTick;
    ax.XGrid = 'on';
    ax.YGrid = 'on';
    ax.XTickLabel = [];
    config_legend();
end

function  faceColor = config_barColor(i)

    barColor = [...
        linspace(0, 0, 9);...
        linspace(0, 1, 4), linspace(1, 0, 5); ...
        linspace(1, 1, 4), linspace(0.6, 0.6, 5)];
    
    faceColor = barColor(:, i);
        
end

function config_legend()

    global Epnames;
    lgd = legend('show');
    %lgd.String = {'var', Epnames{:}};
    lgd.String = {Epnames{:}};
    lgd.Box = 'off';
    lgd.Location = 'southoutside';
    lgd.Orientation = 'horizontal';
    
end