function [ output_args ] = config_save_figures( fh, diraddress )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

    figure(fh.Number);
    set(gcf, 'Position', get(0, 'Screensize'));
    saveString = create_padded_string(fh.Name);
    pause(1);
    print(fh, [diraddress 'figures/' saveString] ,'-dpng', '-r300');

end

function tString = create_padded_string(tString);

    blankpos = find(isspace(tString));
    tString(blankpos) = '_';    

end

