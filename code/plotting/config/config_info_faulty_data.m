function config_plot_infoText_faulty_data(id, mid, hs, fh)

    eCode = ratmpi_gen_eCode_for_badData(id, hs)
    
    if eCode == 2             
        figure(fh.Number);
        ax = fh.Children(2);
        eTextString = 'DATA QUALITY IMPAIRED';
        th = text(mean(ax.XLim), mean(ax.YLim), 0, eTextString);
        th.Color = 'r';
        th.Rotation = 30;
        th.FontSize = 30;
        th.FontWeight = 'bold';
        th.HorizontalAlignment = 'center';
    end


        

end