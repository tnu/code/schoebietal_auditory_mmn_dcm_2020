function [] = ratmpi_config_boxplot()
% [ r, l ] = ratmpi_config_boxplot()
% 
% Creates a nice boxplot for the current axis
% 
% IN
%   
%   OPTIONAL:
%
% OUT
%
%-------------------------------------------------------------------------

cc = linspecer();
% cc = flipud(linspecer());
% cc = flipud(linspecer(size(h, 1)));

g = gca; 
h = findobj(g.Children, 'Tag', 'Box'); 
for i = 1 : size(h, 1)
    h(i).Color = cc(i, :); 
    h(i).LineWidth = 8;
end

h = findobj(g.Children, 'Tag', 'Whisker'); 
for i = 1 : size(h, 1)
    h(i).Color = cc(i, :); 
end

h = findobj(g.Children, 'Tag', 'Outliers'); 
for i = 1 : size(h, 1)
    h(i).MarkerEdgeColor = cc(i, :); 
end

h = findobj(g.Children, 'Tag', 'Median'); 
for i = 1 : size(h, 1)
    h(i).Color = cc(i, :); 
end

end