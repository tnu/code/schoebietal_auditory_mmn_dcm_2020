function [s] = ratmpi_plot_fitMaster(ids, sets, options)
% [ s ] = ratmpi_plot_fitMaster(ids, sets, options)
%
% Creates average signal and fit over multiple factors.
%
% IN
%   ids          cell             Cell of subject IDs
%   sets         struct           Configurations (factors / levels) to run
%                                 ratmpi_dcm_summaryMaster
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   argout       type
%
% ------------------------------------------------------------------------
%
% Possible factors for sets:
%
% Mandatory
%   .mids       cell      Model identifier, e.g. {'01', '04'}
%   .fields     cell      Field identifier of the DCM, e.g. {'Ep'};
%
% Optional
%   .design     cell      Design identifier, e.g. {'MMN_0.1', 'MMN_0.2'}
%   .hemi       cell      Hemisphere, e.g. {'lhs', 'rhs'}
%   .npriors    cell      Neuronal priors identifier, e.g. {'pX02'};
%   .fpriors    cell      Forward priors identifier, e.g. {'gX02'};
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end

if isempty(sets)
    sets.fields = {'H', 'y'};
    sets.mids = {'16'}; %options.dcm.models;
    sets.design = options.design;
    sets.pharma = {'Vehicle'}; %options.drugs;
    sets.hemi = {'lhs'}; %options.hemisphere;
    sets.factor = 'pharma';
end

idx = 1;
for factorCell = sets.(sets.factor)
    tmp = sets;
    tmp.(sets.factor) = char(factorCell);
    
    % Get prediction and data seperately, since the data only needs to be
    % gotten once per subject and pharma
    tmp_y = tmp; 
    tmp_y.mids = tmp.mids(1);
    tmp_y.fields = {'y'};
    [sy] = ratmpi_dcm_summaryMaster(ids, tmp_y, options);

    
    % Run the summary Master to return all fields of interest
    tmp_h = tmp; 
    tmp_h.fields = {'H'};
    [sh] = ratmpi_dcm_summaryMaster(ids, tmp_h, options);
    
    % Plot the fit and configure
    subplot(length(sets.(sets.factor)), 1, idx)
    plot_average_fit(sy.y, sh.H, options);
    
    
    idx = idx + 1;
end

config_figure(sets, options)
rescale_subplots()

end

function plot_average_fit(y, yp, options)

mY = mean(y, 2);
semY = std(y')'; % ./ size(y, 2);

[ha, hp] = mpdcm_shaded_area_plot([], mY, mY + semY, mY - semY);
ha.Tag = 'sem_y';
hp.Tag = 'mean_y';

mYp = mean(yp, 2);
semYp = std(yp')'; % ./ size(mYp, 2);

[ha, hp] = mpdcm_shaded_area_plot([], mYp, mYp + semYp, mYp - semYp);
ha.Tag = 'sem_yp';
hp.Tag = 'mean_yp';

g = gca;
config_axes(g, options);

% Display Variance Explained
vE = return_vE(mY, mYp); 
text(900, 0.8 * g.YLim(2), sprintf('vE = %2.2f', vE)); 

end


function config_figure(sets, options)

% Config Figure
g = gcf;
g.Color = [1 1 1];

% Colorscale
cc = linspecer(length(sets.(sets.factor)));

idx = 1;
for i = 1 : size(g.Children, 1)
    gg = g.Children(end - i + 1);
    
    hy = findobj(gg, 'Type', 'line', 'Tag', 'mean_y');
    hy.Color = cc(idx, :);
    
    hya = findobj(gg, 'Type', 'patch', 'Tag', 'sem_y');
    hya.FaceColor = cc(idx, :);
    hya.FaceAlpha = 0.5;
    
    hy = findobj(gg, 'Type', 'line', 'Tag', 'mean_yp');
    hy.Color = [0 0 0];
    
    hya = findobj(gg, 'Type', 'patch', 'Tag', 'sem_yp');
    hya.FaceColor = 'none';
    hya.EdgeColor = [ 0 0 0]; 'none'; %[0 0 0];
    hya.LineStyle = '--';
    
    
    if i ~= size(g.Children, 1)
        gg.XAxis.Color = 'none';
    end
    
    idx = idx + 1;
end

end

function config_axes(h, options)

xx = [0 : 50 : 1000];
xxLabel = [repmat([0 : 50 : 200], 1, 4), 250]; 

% Create Vertical lines
xLine = linspace(h.XLim(1), h.XLim(2), 5);
% Remove 0 and endline
xLine = xLine(2 : 4);

for i = 1 : length(xLine)
    plot([xLine(i), xLine(i)], h.YLim, ...
        'Color', 0.2 * ones(1, 3), ...
        'LineWidth', 1, ...
        'LineStyle', ':');
end

h.XTick = xx;
h.XTickLabel = xxLabel;

h.XGrid = 'on'; 
h.YGrid = 'on';
h.TickDir = 'out';

% Label the condition and region
% -----------------------------------------------------------------------
% Get x-size of subplot, and find the 4 centers
x0 = [h.XLim(1), h.XLim(2)];
xc = linspace(x0(1), x0(2), 9);
xc = xc(2 : 2 : end); 

% Get y-location from the normal location of the x labels
yc = h.XLabel.Position(2);

xLab = {'A1, Standard', 'PAF, Standard', 'A1, Deviant', 'PAF, Deviant'};


for i = 1 : length(xc)
    text(xc(i) - 50, yc, xLab{i}); 
end
    
end


function rescale_subplots()

% Get figure handle
g = gcf;

% Number of subplots
n = size(g.Children, 1);

% Offset
offs = 0.01;

% Get lowest Y point (last subplot)
ymin = g.Children(1).Position(2);

% Divide remaining space into n subplots
height = (0.95 - ymin - (n-1) * offs) / n;

% Adjust position
y0 = ymin;
for i = 1 : n
    h = g.Children(i);
    h.Position(2) = y0;
    h.Position(4) = height;
    h.Position(3) = 0.6;
    y0 = y0 + height + offs;
end



end


function vE = return_vE(y, yp)

vE = 1 - var(y - yp) / var(y);

end








