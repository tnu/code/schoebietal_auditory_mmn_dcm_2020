function [ g ] = ratmpi_plot_config_boxplot(g)
% [  ] = ratmpi_plot_config_boxplot(g)
%
% Configures a boxplot
%
% IN
%   g           axis handle       g = gca;
%
%   OPTIONAL:
%
% OUT
%   argout       type
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


boxColor = 0.6 * ones(1, 3);

h = findobj(g, 'Tag', 'Box'); 
set(h,...
    'LineWidth', 6, ...
    'Color', boxColor);

h = findobj(g, 'Tag', 'Whisker');
set(h,...
    'LineWidth', 1, ...
    'Color', boxColor);

h = findobj(g, 'Tag', 'MedianOuter');
set(h,...
    'Visible', 'off');

h = findobj(g, 'Tag', 'MedianInner');
set(h,...
    'MarkerSize', 8);

h = findobj(g, 'Tag', 'Outliers');
set(h,...
    'Marker', 'x', ...
    'MarkerEdgeColor', 0 * ones(1, 3));