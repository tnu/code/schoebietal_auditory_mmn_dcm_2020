function [s] = ratmpi_plot_EpMaster(ids, sets, options)
% [ s ] = ratmpi_plot_EpMaster(ids, sets, options)
%
% Creates plots of the posterior estimates for multiple factors defined in
% sets and ids.
%
% IN
%   ids          cell             Cell of subject IDs
%   sets         struct           Configurations (factors / levels) to run
%                                 ratmpi_dcm_summaryMaster
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   argout       type
%
% ------------------------------------------------------------------------
%
% Possible factors for sets:
%
% Mandatory
%   .mids       cell      Model identifier, e.g. {'01', '04'}
%   .fields     cell      Field identifier of the DCM, e.g. {'Ep'};
%
% Optional
%   .design     cell      Design identifier, e.g. {'MMN_0.1', 'MMN_0.2'}
%   .hemi       cell      Hemisphere, e.g. {'lhs', 'rhs'}
%   .npriors    cell      Neuronal priors identifier, e.g. {'pX02'};
%   .fpriors    cell      Forward priors identifier, e.g. {'gX02'};
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end
%
% ids = intersect(options.subjectIDs.pharma.lvl2.(options.altnames.design).lhs, ...
%     options.subjectIDs.pharma.lvl2.(options.altnames.design).rhs);

if isempty(sets)
    sets.fields = {'Ep', 'pC'};
    sets.varargin = {'A', 'B', 'C', 'T', 'G', 'D', 'S', 'R'};
    sets.mids = {'16'}; %options.dcm.models;
    sets.design = options.design;
    sets.pharma = {'Vehicle'}; %options.drugs;
    sets.hemi = {'lhs', 'rhs'}; %options.hemisphere;
    sets.factor = 'pharma';
    sets.sv = {'bma'}; %bma best default
    sets.plot.dotplot = 0;
end

if ~isfield(sets, 'plot')
    sets.plot.dotplot = 0;
end


% Iterate over pharma and load only those
offs = linspace(-0.2, 0.2, length(sets.(sets.factor)));
idx = 1;
for factorCell = sets.(sets.factor)
    tmp = sets;
    
    % Relabel if using drug effects coding
    switch char(factorCell)
        case 'antagonist'
            tmp.(sets.factor) = {'2mgScopo', '1mgScopo'};
        case 'placebo'
            tmp.(sets.factor) = {'Vehicle'};
        case 'agonist'
            tmp.(sets.factor) = {'3mgPilo', '6mgPilo'};
        otherwise
            tmp.(sets.factor) = char(factorCell);
    end
    
    % Run the summary Master to return all fields of interest
    s = ratmpi_dcm_summaryMaster(ids, tmp, options);
    
    Ep = s.Ep;
    red_idx = zeros(size(Ep, 1), 1);
    
    % Reduce parameters
    [Ep, red_idx] = reduce_pars(Ep);
    
    % Create the plot of the Parameters
    x = linspace(1, size(Ep, 1), size(Ep, 1)) + offs(idx); 
    plotEp_multiplot(x, Ep, tmp, char(factorCell));
    idx = idx + 1;
end

% Configure the plot
config_plotEp(sets);

% Plot the labels
create_labels(sets, red_idx);

% add_interaction(sets);

% Explicitly plot the zero line
g = gca;
plot([g.XLim(1), g.XLim(2)], [0 0], 'k');


g = gcf;
g.Color = [1 1 1];

end


function plotEp_multiplot(x, Ep, tmp, lvlName)
% Create Error bar plot with SEM

errorbar(x, mean(Ep, 2), std(Ep')' ./ sqrt(sum(~isnan(Ep')))', 's', ...
    'Tag', lvlName);

hold on;

% Plot the parameters and tag them with the drug label
plot(x, Ep', 'o', 'Tag', lvlName);
hold on;
g = gca;
g.TickDir = 'out';
g.XLim = [0, size(Ep, 1) + 1];
g.YLim = [-2.5, 2.5];
g.XGrid = 'on';
g.YGrid = 'on';

end

function config_plotEp(sets)
% Config the figure using unique on the axes

g = gca;
handle_lineplots = findobj(g, 'Type', 'line');
handle_errorbar = findobj(g, 'Type', 'errorbar');

levels = ratmpi_unique({handle_lineplots.Tag});

cc = linspecer(length(levels));
cc = flipud(cc);

idx = 1;
for lvlCell = levels
    
    hline = findobj(handle_lineplots, 'Tag', char(lvlCell));
    heb = findobj(handle_errorbar, 'Tag', char(lvlCell));
    
    set(hline, 'MarkerFaceColor', cc(idx, :));
    set(hline, 'MarkerEdgeColor', cc(idx, :));
    set(hline, 'MarkerSize', 3);
    
    set(heb, 'Color', cc(idx, :));
    set(heb, 'LineWidth', 1);
    set(heb, 'MarkerFaceColor', cc(idx, :));
    
    
    idx = idx + 1;
end

if ~sets.plot.dotplot
    h = findobj(g, 'Type', 'line');
    set(h, 'Visible', 'off');
end


end


function add_interaction(sets)

g = gcf;

% Get handles to all dots
h = findobj(g.Children.Children, 'Type', 'line');

% Reshape the handle array, such that the factor (f.e. drugs, is in columns
% and the other factor, f.e. subjects, is in rows)
q = [];
for factorCell = sets.(sets.factor)
    factor = char(factorCell);
    
    q = [q, findobj(h, 'Tag', factor)];
end

% Iterate over all points, starting with the first one, and plot a line to
% the next value for that subject
nPars = length(q(1, 1).XData);

for iRows = 1 : size(q, 1)
    
    for iCols = 1 : size(q, 2) - 1
        
        for iPars = 1 : nPars
            
            % Subject iRows, factor level iCols, parameter iPars
            x1 =  q(iRows, iCols).XData(iPars);
            x2 =  q(iRows, iCols + 1).XData(iPars);
            y1 =  q(iRows, iCols).YData(iPars);
            y2 =  q(iRows, iCols + 1).YData(iPars);
            
            plot([x1, x2], [y1, y2], '-k', 'Color', [0.7 0.7 0.7]);
        end
    end
    
end



end


function create_labels(sets, red_idx)

if ~isfield(sets, 'varargin')
    [~, xTicks, xTickLabels] = ratmpi_return_names_cmc();
else
    [~, xTicks, xTickLabels] = ratmpi_return_names_cmc(sets.varargin);
end

if sum(red_idx) == 0
    g = gca;
    g.XTick = xTicks;
    g.XTickLabel =  xTickLabels;
    g.TickDir = 'out';
end

end


function [Ep, idx] = reduce_pars(Ep)

idx = ~any((Ep - Ep(:, 1))')';

Ep(idx, :) = [];

end



