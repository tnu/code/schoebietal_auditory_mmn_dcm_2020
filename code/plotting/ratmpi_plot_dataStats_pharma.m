function [ t ] = ratmpi_plot_dataStats_pharma(gid, sf, options)
% [ r ] = ratmpi_plot_dataStats_pharma(gid, sf, options)
%
% Returns the data correlation over pharmacological manipulations
% (compared to the 2mgScopo condition)
%
% Factors: tone = {'Standard', 'Deviant'}
%          pharma = {'2mgScopo', '1mgScopo', 'Vehicle', '3mgPilo','6mgPilo}
%
% IN
%   gid           string           group_placebo / group_pharma
%
%   OPTIONAL:
%   sf           bool             Don't save (0) or save (1) figure.
%                                 (Default = 0);
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
%
% OUT
%   stats        struct           Structure containing concatenated data
%                                 and statistics
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 2
    options = ratmpi_set_global_options();
    sf = 0;
elseif nargin < 3
    options = ratmpi_set_global_options();
end


% Settings
[channels, ~] = ratmpi_return_channels(options.hemisphere, options);
ndesign = 'MMN01';
pharmalab = {'M--', 'M-', 'M0', 'M+', 'M++'};
cc = linspecer(5);

% Set up structure stats to run ratmpi_return_dataMaster
sets = struct();
sets.cond = {'Standard', 'Deviant'};
sets.design = {options.design};
sets.pharma = options.drugs;
sets.Dfile = 'prep';
sets.timeWindow = options.fstLvl.window;


% Set up subjects. Here, we need to be a little bit less careful, because
% we are plotting for lhs and rhs independently, thus we don't need to
% reload the structure.
ids = options.subjectIDs.pharma.lvl2.(ndesign).xhs;

% Load the data
[data, des] = ratmpi_return_dataMaster(ids, sets, options);

% Iterate over ids, conditions and channels and reorganise the data
X.id = [];
X.tone = [];
X.lpharma = [];
lpharma = [-1, 0, 1, 2] - mean([-1, 0, 1, 2]);
X.pharma = [];
r = [];

fidx = 1;
for chanCell = channels
    chan = char(chanCell);
    
    for idCell = ids
        id = char(idCell);
        
        y = [];
        
        for condCell = sets.cond
            cond = char(condCell);
            
            % Get the design matrix for this channel
            XX = des.(chan);
            
            % Get the indices of the factors
            iX = strcmp(XX.id, id) & strcmp(XX.cond, cond);
            
            % Get the data
            y = data.(chan)(iX, :);
            
            % Get the correlations
            tmp = corrcoef(y');
            r = [r; tmp(1, 2:end)'];
            
            % Augment the design matrix
            X.id = [X.id; repmat({id}, 4, 1)];
            X.tone = [X.tone; repmat({cond}, 4, 1)];
            X.lpharma = [X.lpharma; lpharma'];
            X.pharma = [X.pharma; sets.pharma(2:end)'];
                      
        end
         
    end
       
end

t = struct2table(X); 
t.y = r;

end



