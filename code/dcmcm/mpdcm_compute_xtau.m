function [xx, xt] = mpdcm_compute_xtau(tn, xhist, itau)
% [xx, xt] = tnudcm_compute_xtau(tn, xhist, itau)
%
% Computes the delayed state values as a multidimensional cell.
%
% INPUT
%   tn   - current sample (integer)
%   xhist- matrix of history of states
%   itau - delayed sample indices as returned from tnudcm_compute_itau
% 
%_________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


xt = cell(size(itau));
nStates = size(xhist, 1);
nSources = size(itau, 1);
nx = nStates / nSources;

for i = 1 : size(itau, 1)
    for j = 1 : size(itau, 2)
        
        lxt = zeros(size(xhist, 1), 1);
        uxt = zeros(size(xhist, 1), 1);
        uitau = ceil(itau);
        litau = floor(itau);
        tp = mod(itau, 1);
        
        if tn - uitau(j, i) <= 0
            uxt = zeros(nStates, 1);
            lxt = zeros(nStates, 1);
        else
            lxt = xhist(:, tn - uitau(j, i));
            uxt = xhist(:, tn -litau(j, i));
        end
        
        xt{j, i} = spm_unvec(lxt + tp(j, i) .* (uxt - lxt), zeros(nSources, nx));
        
    end
        
end

for i = 1 : nSources
    for j = 1 : nSources
        xx{i}(j, :) = xt{i, j}(j, :);
    end
end

end

