function [ v, ff ] = mpdcm_update_dde_euler( v, u, P, M, xtau, dt, N )
% [ v, ff ] = mpdcm_update_dde_euler( v, u, P, M, xtau, dt, N )
% 
% Computes an update step for the delayed euler integrator, i.e.
%
%    x(t + dt) = x(t) + dt * f(t-tau)
%
% Input:
%           v       vectorized state value x(t) at time t
%           u       value u(t) from driving input at time t
%           P       model parameters
%           M       model structure
%           xtau    delayed state matrix as returned from
%                   tnudcm_compute_xtau
%           dt      sampling rate of signal
%           N       used as dt / N as step size of integrator
%
% Output:
%           v       vectorized state value x(t + dt) at time t + 1
%           ff      f(t-tau) computed for delayed states
%
% The Implementation right now also computes redundant states, and is
% particular for a two region DCM. This will be made more flexible in the
% future.
%__________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

f = str2func(M.f);

for k = 1 : N
    fx = []; 
    ff = []; 
    
for j = 1 : numel(xtau)
    fx = full(f(v, u, P, M, xtau{j}));
    ff(j, :) = fx(j, :);
end

ff = spm_vec(ff);
v = v + dt / N * ff;

end

end


