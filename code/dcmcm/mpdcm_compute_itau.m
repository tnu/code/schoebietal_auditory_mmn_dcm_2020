function [iD] = mpdcm_compute_itau(P, M, U)
% [iD] = mpdcm_compute_itau(P, M, U)
%
% Computes the delayed state samples as a matrix, i.e. the 
%
%   Sample at time t - tau = Delay / sampling Rate
%
% Since every delay is with respect to a particular state variable, the
% square matrix is of dimension 
%
% and is read FROM (column) TO (row)
%
% FORMAT [y] = tnudcm_int_dde_euler(P,M,U)
% P   - model parameters
% M   - model structure
% U   - input structure or matrix
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


D = M.pF.D;
n = size(M.x, 1); 
dt = U.dt;

De = (ones(n, n) - eye(n)) .* D(2).*exp(P.D)/1000;
Di = eye(n) .* D(1)/1000;

D = De + Di; 


iD = D ./ dt;

end
