function [ options ] = ratmpi_global_options_MMN01_lhs(options)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    options = struct();
end

% Set default parameters which can be overwritten by input
default.hemisphere = 'lhs';
default.pharma = 'Vehicle';
default.design = 'MMN_0.1';
default.priors = 'pX04';
default.bms = 'allModels'; % 'none', 'factorial', 'tone', 'allModels'
default.multistart = 'best'; %'best';
default.simulate = 'cmcdde';
default.hyperpriors = 'hX02'; %'hX01', 'hX02'


% Set optional options paramaters or default values
if isfield(options, 'hemi')
    error('Fatal conflict. Never use options.hemi, this is bound for error');
end

if ~isfield(options, 'hemisphere'); options.hemisphere = default.hemisphere; end
if ~isfield(options, 'pharma');     options.pharma = default.pharma; end
if ~isfield(options, 'design');     options.design = default.design; end
if ~isfield(options, 'priors');     options.priors = default.priors; end
if ~isfield(options, 'bms');        options.bms = default.bms; end
if ~isfield(options, 'multistart'); options.multistart = default.multistart; end
if ~isfield(options, 'simulate');   options.simulate = default.simulate; end
if ~isfield(options, 'hyperpriors');options.hyperpriors = default.hyperpriors; end

% These are intended for code revision
options.revision = 1;

% Return alternative naming for pharma and design
options.altnames = ratmpi_return_altnames(options);

% Set all fixed parameters with dependencies on input
options.datadir = fullfile(getenv('RATMPI_DATA'), 'data/');
options.rawdir = fullfile(getenv('RATMPI_DATA'), 'raw/');
options.rootdir = getenv('RATMPI_ROOT');
options.study = 'RATMPI';
options.drugs = {'2mgScopo', '1mgScopo', 'Vehicle', '3mgPilo', '6mgPilo'};
options.dataset = {[options.design '_s7-9_d16-18'], [options.design '_s16-18_d7-9']};
options.general.printFig = [0 1 2];

% Set all subject IDs
options.subjectIDs = ...
    ratmpi_return_subjectIDs(options);

options.preproc.convert.samplingRate = 2000;
options.preproc.convert.triggerThreshold = 3;
options.preproc.downsamplefreq = 1000;
options.preproc.dataset = 'dataset2';


options.fstLvl.window = [0 0.25];

% Set up general statistics settings
options.stats.mc_corr = @ratmpi_stats_fdr;
% options.stats.lvl2.conjfiles = 'anova1.tone';
% options.stats.lvl2.conjfiles = 'anova2.tone_design';
options.stats.lvl2.conjfiles = 'anova2.tone_pharma';
options.stats.lvl2.siglvl = 0.05;

% Set up DCM configuration
options = ratmpi_return_dcmIdentifier(options);

options.dcm.bms.effects = 'RFX';
options.dcm.bms.family = 'allModels'; %winning
options.dcm.bms.bma = 'famwin';
options.dcm.bms.sv = options.multistart;

% Set up classification
options.class.pars = 'bma';
options.class.hemi = 'bilat';
% options.class.drugs = 'anta_vs_ago';
% options.class.drugs = 'all';
% options.class.drugs = 'Vehicle_6mgPilo';
options.class.drugs = '2mgScopo_Vehicle';
% options.class.drugs = '2mgScopo_6mgPilo';
% options.class.coding = 'effect';
options.class.coding = 'dosage';
options.class.permTest = 0;
options.class.seed = 1;

switch options.dcm.bms.sv
    case 'best'
        suffix = '';
    case 'default'
        suffix = '_default'; 
end

options.dcm.bms.identifier = [options.dcm.bms.effects '_' ...
    options.dcm.bms.family '_' ...
    options.dcm.bms.bma suffix];

options.class.identifier  = [options.altnames.design '_' ...
    options.hemisphere '_class_linear_' ...
    options.class.hemi '_' options.class.pars '_' ...
    options.class.drugs '_' options.class.coding  suffix];

end


