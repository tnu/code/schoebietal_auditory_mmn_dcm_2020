function [ D, events ] = ratmpi_convert( id, options )
% [ D, events ] = ratmpi_convert(id, options)
% 
% Converts the data files from ascii to the spm type .dat .mat EEG objects.
% Detection of the events using a peakfinder algorithm on the 'trigger
% channels' of the raw data. Detected events are saved and returned by the
% function.
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   D            EEG              SPM/EEG data object 
%   events       struct           Structure with detected standard and
%                                 deviant trigger times
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options();
end

% Get paths and files
details = ratmpi_subjects(id, [], options);

% Load raw data files
switch options.preproc.dataset
    case {'dataset1'}
        disp(['Loading the raw data from ' details.file.dataset1 ' ...']);
        dat{1} = load_file(details.file.dataset1);
    case {'dataset2'}
        disp(['Loading the raw data from ' details.file.dataset2 ' ...']);
        dat{1} = load_file(details.file.dataset2);
end

% Find the Events: they are coded in the amplitude of the columns
% 'stdTrigger' and 'devTrigger', whenever it exceeds a certain voltage
% level.
events{1} = find_events(dat{1}, options);
save(details.file.triggers, 'events'); 

% Preparation of the conversion from fieldtrip to spm.
disp(['Constructing FieldTrip struct...']);
ftdata = struct();

% Sampling rate (Hz)
ftdata.fsample = options.preproc.convert.samplingRate;

% The columns in dat{1}.data are:
% 'Time', 'lA1', 'rA1',  'lPAF', 'rPAF', 'stdTrigger', 'devTrigger'
% Set up fieldtrip data structure with the full time x channel matrix. 
% Channel 'Time' is not stored. 
nChannels = 6;
nBins = size(dat{1}.data,1);
ftdata.trial = cell(1,1);
ftdata.time = cell(1,1);
ftdata.trial{1} = dat{1}.data(:, [2, 3, 4, 5, 6, 7])';
ftdata.time{1} = [0:nBins-1]/ftdata.fsample;


% Set up channel labels
for c = 1:nChannels
    ftdata.label{c} = [dat{1}.columns{c + 1}];
end


% Convert the ftdata struct to SPM M/EEG dataset and save to disk.
fileD = details.name.D;
file = fullfile(details.path.prep, fileD);
D = spm_eeg_ft2spm(ftdata, file);

end


function dat = load_file(file)

[~,type] = fileparts(file);
if strcmpi(type,'rawdatanew')
    x = load(file);
    
    % Note: order of dat file has changed since Fabienne Jung to remove
    % redundant columns (see below and textfile
    % Acquired_data_fuer_Tina.rtf for reference).
    
    dat.data = x(:, [7 5 4 3 2 8 9]);
    dat.columns = {'Time', 'lA1', 'rA1',  'lPAF', 'rPAF', 'stdTrigger', 'devTrigger'};
       
    disp(['Data loaded from ''',file,''' using simple format']);
elseif strcmpi(type,'raw data')
    x = importdata(file,'\t',7);
     
    % Note: The following would refer to the same ordering as Fabienne
    % Jung notes in her textfile Acquired_data_fuer_Tina.rtf
    % x = x.data(:,[1 2 3 4 5 6 1 6 7]);
    
    dat.data = x.data(:,[1 5 4 3 2 6 7]);
    dat.columns = {'Time', 'lA1', 'rA1',  'lPAF', 'rPAF', 'stdTrigger', 'devTrigger'};
    disp(['Data loaded from ''',file,''' using header+body format']);
else
    error('unexpected filename');
end

end


function events = find_events(dat, options)
% Here, we use the default routine to detect peaks by Kai Brodersen. In
% comparison to ratmpi_peakFinder, based on a MATLAB routine, Kais
% implementation detects the peak during the rise in the trigger signal as
% opposed to peakfinder, where we detect the middle of the peak.

triggerThreshold = options.preproc.convert.triggerThreshold;
% events.std = ratmpi_peakFinder(double(dat.data(:, 6) > triggerThreshold)');
% events.dev = ratmpi_peakFinder(double(dat.data(:, 7) > triggerThreshold)');

events.std = peakFinder(dat.data(:, 6) > triggerThreshold);
events.dev = peakFinder(dat.data(:, 7) > triggerThreshold);

end


function onsets = peakFinder(x)
i=0;
z=0;
onsets = [];
while z<length(x)
    zz = find(x(z+1:end)==1,1) + z;
    i=i+1;
    onsets = [onsets, zz];
    z = zz+round(0.100*2000)+10;
end
end
