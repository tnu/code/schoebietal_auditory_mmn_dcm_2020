function [ D ] = ratmpi_create_events( id, options )
% [ D ] = ratmpi_create_events()
% 
% Stores the event trigger times (detected in ratmpi_convert) in the
% dataset as standard and deviant events. 
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   D            EEG/MEG          SPM/EEG data object 
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options(); 
end

% Get paths and files
details = ratmpi_subjects(id, [], options); 

% Load the file containing the trigger events (created during
% ratmpi_convert)
triggers = load(details.file.triggers, 'events');

% Load the SPM EEG/MEG file
D = spm_eeg_load(details.file.D);

% Allocate variables
samplingRate = options.preproc.convert.samplingRate;
nEvents.std = length(triggers.events{1}.std);
nEvents.dev = length(triggers.events{1}.dev);

% Iterate over all standard triggers, and store the events at the
% respective times.
i = 1;
while i <= nEvents.std
    res(i).type = 'Stimulus';
    res(i).value = 'stdTrigger';
    res(i).time = triggers.events{1}.std(i) * 1 / samplingRate;
    res(i).duration = 0;
    i = i + 1;
end

% Iterate over all deviant triggers, and store the events at the respective
% times.
j = 1;
while j <= nEvents.dev
    res(i).type = 'Stimulus';
    res(i).value = 'devTrigger';
    res(i).time = triggers.events{1}.dev(j) * 1 / samplingRate;
    res(i).duration = 0;
    j = j + 1;
    i = i + 1;
end

% Create the events in the EEG datafile
D = events(D, 1, spm_cat_struct([], res));

% Save the data file
save(D);

end

