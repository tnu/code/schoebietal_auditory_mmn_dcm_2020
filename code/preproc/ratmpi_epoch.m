function [ D ] = ratmpi_epoch( id, options )
% [ D ] = ratmpi_epoch(id, options)
% 
% Epochs the data around the standard and deviant events and performs
% baseline correction.
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   D            EEG/MEG         SPM/EEG data object 
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options(); 
end

% Get paths and files
details = ratmpi_subjects(id, [], options); 

% Prepare the config structure fo the spm routine.
S = [];
S.D = details.file.fdfD;
S.timewin = [-100 500];

S.trialdef(1).conditionlabel = 'Standard';
S.trialdef(1).eventtype = 'Stimulus';
S.trialdef(1).eventvalue = 'stdTrigger';
S.trialdef(1).trlshift = 0;

S.trialdef(2).conditionlabel = 'Deviant';
S.trialdef(2).eventtype = 'Stimulus';
S.trialdef(2).eventvalue = 'devTrigger';
S.trialdef(2).trlshift = 0;

S.bc = 1;
S.prefix = 'e';
S.eventpadding = 0;

% Run the routine. The epoched dataset is automatically stored.
D = spm_eeg_epochs(S); 

end

