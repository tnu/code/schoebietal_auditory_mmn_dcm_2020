function [] = ratmpi_preproc( id, options)
% [] = ratmpi_preproc(id, options)
% 
% Preprocesses the raw data from ascii to the final spm compatible
% datasets. 
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options(); 
end 

% We marginalize over the two datasets, where low and high tone are 
% standard and deviant, and vice versa.
datasets = {'dataset1', 'dataset2'};

for dCell = datasets
    options.preproc.dataset = char(dCell);
    
    % Get paths and files
    details = ratmpi_subjects(id, [], options);
    
    % Create folder if necessary
    if ~exist(details.path.prep)
        mkdir(details.path.prep);
    end
    
    % Convert the dataset from raw to SPM EEG objects
    ratmpi_convert(id, options);
    disp(['finished conversion for subject ' id]);
    
    % Save the detected events in the EEG object
    ratmpi_create_events(id, options);
    disp(['finished setting of triggers for subject ' id]);
    
    % Set the (6) channel types to 'LFP' and 'other' (Trigger channels)
    ratmpi_set_chantype(id, options);
    disp(['finished set channel type subject ' id]);
    
    % Highpass filter the data
    ratmpi_highpass_filter(id, options);
    disp(['finished filtering subject ' id]);
    
    % Downsample the data (a lowpass filter will be automatically applied)
    ratmpi_downsample(id, options);
    disp(['finished filtering subject ' id]);

    % Lowpass filter the data
    ratmpi_lowpass_filter(id, options);
    disp(['finished filtering subject ' id]);
    
    % Epoch the data around the detected events
    ratmpi_epoch(id, options);
    disp(['finished epoching for subject ' id]);
    
end

% Merge the intermediate preprocessed files into a single dataset
ratmpi_merge(id, options);
disp(['finished merging datasets for subject ' id]);

% Reject further artefacts
ratmpi_reject_artefacts(id, options)
disp(['finished artefact rejection for subject ' id]);

% Create a dataset with removed artefacts (This will only be used for later
% diagnostics and manual averages)
ratmpi_preproc_remove_artefacts(id, options)
disp(['finished artefact removal for subject ' id]);

% Average over trials to create mean ERPs
ratmpi_average(id, options);
disp(['finished averaging for subject ' id]);

% Crop the dataset into two individual datasets for each hemispheres
ratmpi_crop(id, options);
disp(['finished cropping subject ' id]);

end

