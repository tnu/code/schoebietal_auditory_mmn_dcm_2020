function [ D ] = ratmpi_merge( id, options )
% [ D ] = ratmpi_merge(id, options)
% 
% Merges the two datasets (for flipped standard / deviant tone frequencies)
% into a single dataset.
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   D            EEG/MEG         SPM/EEG data object 
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options(); 
end

% Get paths and files
details = ratmpi_subjects(id, [], options); 

% Create a list with the paths to the two files
listOfFiles = createFileList(id, options); 

% Prepare the config structure fo the spm routine.
S = []; 
S.D = listOfFiles;
S.recode(1).file     = '.*';
S.recode(1).labelorg = '.*';
S.recode(1).labelnew = '#labelorg#';
S.prefic = 'c'; 

% Run the routine. The merged dataset is automatically stored.
D = spm_eeg_merge(S); 

% Rename to drop the flag 'dataset1'
Dnew = copy(D, details.file.cefdfD); 
delete(D); 

end

function l =  createFileList(id, options)

datasets = {'dataset1', 'dataset2'}; 
l = []; 
for dCell = datasets
    options.preproc.dataset = char(dCell); 
    details = ratmpi_subjects(id, [], options);
    l = [l; details.file.efdfD]; 
end

end