function [] = ratmpi_crop( id, options )
% [] = ratmpi_crop(id, options)
% 
% Creates two individual datasets for the two hemispheres
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Get options if not specified
if nargin < 2
    options = ratmpi_set_global_options(); 
end

% Get paths and files
details = ratmpi_subjects(id, [], options); 

% Prepare the config structure fo the spm routine.
S = []; 
S.D = spm_eeg_load(details.file.macefdfD); 
S.timewin = [-Inf Inf]; 
S.freqwin = [-Inf Inf];
S.channels = {'lA1', 'lPAF'};
S.prefix = 'c'; 

% Run the routine. The croped dataset is automatically stored.
D = spm_eeg_crop(S); 

% Rename to desired output name
Dnew = copy(D, [details.path.prep details.name.id '_lhs_preproc']); 
delete(D); 

% Prepare the config structure fo the spm routine.
S = []; 
S.D = spm_eeg_load(details.file.macefdfD); 
S.timewin = [-Inf Inf]; 
S.freqwin = [-Inf Inf];
S.channels = {'rA1', 'rPAF'};
S.prefix = 'c'; 

% Run the routine. The croped dataset is automatically stored.
D = spm_eeg_crop(S); 

% Rename to desired output name
Dnew = copy(D, [details.path.prep details.name.id '_rhs_preproc']); 
delete(D); 

end