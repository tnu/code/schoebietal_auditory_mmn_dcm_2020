function [s] = ratmpi_get_revision_info( options )
% [ s ] = ratmpi_get_revision_info(directory)
%
% Gets the git hash of the code 
%
% IN
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT     
%
% adapted from: tapas_get_revision_info.m
% aponteeduardo@gmail.com
% copyright (C) 2016
%

if nargin < 1
    options = ratmpi_set_global_options();
end

directory = options.rootdir;

% Try to get the Hash. If not sucessfull, store the error message. 
try
    [branch, revhash] = get_hash(directory); 
    s = struct('branch', branch, 'revhash', revhash);
catch ME
    warning('Storing Hash was not successful');
    s = struct('ME', ME, 'branch', 'none', 'revhash', 'none');
end

end

function [branch, revhash] = get_hash(directory)

if ~(exist(directory) == 7)
    error('tapas:get_revision_info', 'no directory');
end

gitroot = fullfile(directory, '.git');

if ~(exist(gitroot) == 7)
     error('tapas:get_revision_info', 'no repository');
end

githead = fullfile(gitroot, 'HEAD');

if ~(exist(githead) == 2)
     error('tapas:get_revision_info', 'no head');
end

fp = fopen(githead, 'r');

if fp == -1
    error('tapas:get_revision_info', 'Could not read the head');
end

head = fgetl(fp);
fclose(fp);
head = regexprep(head, 'ref: ', '');
reffile = fullfile(gitroot, head);

[~, branch, ~] = fileparts(head);

if ~(exist(reffile) == 2)
    error('tapas:get_revision_info', 'Could not read the refs');
end

fp = fopen(reffile, 'r');

if fp == -1
    error('tapas:get_revision_info', 'Could not read the refs');
end

revhash = fgetl(fp);
fclose(fp);

end
