function [] = ratmpi_revision_class_nloso(design, class_drugs, permTest)
% [] = ratmpi_revision_class_nloso(design, pharma)
% 
% Runs the leave one subject out crossvalidation using the BMA parameters,
% including permutation test, for
%
% IN
%   design      string      MMN01, MMN02
%   class_drugs string      Classification problem (see options.class.drugs)
%   permTest    double      Seed of rng (0, ..., 1000)
%   
%   OPTIONAL
%
% OUT
% 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% This doesn't have any effect
hemi = 'lhs';

% Return predefined global options
[opt, optionsHandle] = ratmpi_return_global_options(design, hemi); 
    
% Re-evaluate options (stored in the functionHandle fh)
options = optionsHandle(opt); 

% Specify the classification 
options = return_classification_options(options, class_drugs);

% Specify permutation test
options = return_permTest_options(options, permTest);

% Specify the subjects
ids = options.subjectIDs.pharma.lvl2.(options.altnames.design).xhs;

% Compute the classification
if strcmp(design, 'pdcm')
    [~] = ratmpi_pdcm_class_nloso(ids, [], options);
else
    [~] = ratmpi_class_nloso(ids, [], options);
end
    

end

function options = return_permTest_options(options, permTest)

if permTest == 0
    options.class.permTest = 0; 
    options.class.seed = 1; 
else
    options.class.permTest = 1; 
    options.class.seed = ratmpi_return_rngseed(permTest, options);
end

end


function options = return_classification_options(options, class_drugs)

options.class.drugs = class_drugs; 

if strcmp(options.class.drugs, 'anta_vs_ago')
    options.class.coding = 'effect';
else
    options.class.coding = 'dosage';
end

switch options.dcm.bms.sv
    case 'best'
        suffix = '';
    case 'default'
        suffix = '_default'; 
end

options.dcm.bms.identifier = [options.dcm.bms.effects '_' ...
    options.dcm.bms.family '_' ...
    options.dcm.bms.bma suffix];

options.class.identifier  = [options.altnames.design '_' ...
    options.hemisphere '_class_linear_' ...
    options.class.hemi '_' options.class.pars '_' ...
    options.class.drugs '_' options.class.coding  suffix];

end