function [] = ratmpi_revision_bma(design, hemi)
% [] = ratmpi_revision_bma(design, hemi)
% 
% Based on ratmpi_bma.
% Computes the single subject BMA.
%
% IN
%   design  string      MMN01, MMN02
%   hemi    string      'lhs', 'rhs'
%   
%   OPTIONAL
%
% OUT
% 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Return predefined global options
[opt, optionsHandle] = ratmpi_return_global_options(design, hemi); 

% Iterate over all drugs, and invert the DCM for that model, subject and
% starting values
for pharmaCell = opt.drugs
    opt.pharma = char(pharmaCell); 
    
    % Re-evaluate options (stored in the functionHandle fh)
    options = optionsHandle(opt); 
    
    % Get the subject IDs
    ids = options.subjectIDs.pharma.lvl2.(design).(hemi); 

    % Compute BMA
    ratmpi_bma(ids, 1, options);
    
end

end

