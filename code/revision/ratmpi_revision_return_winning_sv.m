function vbsv = ratmpi_revision_return_winning_sv(id, mid, options)
% [vbsv] = ratmpi_revision_return_winning_sv(id, mid, options)
% 
% Based on ratmpi_dcm_invert.
% Estimates a specified DCM for subject id, model mid and
% starting value vbsv, and uses the options config file specified by 
% design and hemisphere.
%
% IN
%   id      string      Subject Identifier (e.g. '0000')
%   mid     string      Model Identifier (e.g. '16')
%   options struct      Configuration structure as returned
%                       by ratmpi_set_global_options()
%   OPTIONAL
%
% OUT
% 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Load winning starting value
load(fullfile(options.rootdir, 'dcm_config', 'vbsv', ...
'ratmpi_vbsv_winning_sv.mat'), 's');

nid = ['id_' id]; 
m = str2double(mid);

% Starting Values for all models
startingValues = s.(nid).(options.altnames.pharma).(options.hemisphere);

% Starting values for 
vbsv = startingValues(m);

end