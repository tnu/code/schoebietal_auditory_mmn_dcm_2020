function [] = ratmpi_revision_dcm_invert_BestAndDefault(id, design, hemi, mid)
% [] = ratmpi_revision_dcm_invert_BestAndDefault(id, design, hemi, mid)
% 
% Based on ratmpi_dcm_invert.
% Estimates a specified DCM for subject id, model mid and
% starting value vbsv, and uses the options config file specified by 
% design and hemisphere.
%
% IN
%   id      string      Subject Identifier (e.g. '0000')
%   design  string      MMN01, MMN02
%   hemi    string      'lhs', 'rhs'
%   mid     string      Model Identifier (e.g. '16')
%   
%   OPTIONAL
%
% OUT
% 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% Return predefined global options
[opt, optionsHandle] = ratmpi_return_global_options(design, hemi); 

% Iterate over all drugs, and invert the DCM for that model, subject and
% starting values
for pharmaCell = opt.drugs
    opt.pharma = char(pharmaCell); 
    
    % Re-evaluate options (stored in the functionHandle fh)
    options = optionsHandle(opt); 
    
    % Get the best starting value
    vbsv = ratmpi_revision_return_winning_sv(id, mid, options);
    
    % Invert
    [DCM] = ratmpi_dcm_invert(id, mid, vbsv, options);
    
    % Get the default starting value
    vbsv = '0';
    
    % Invert
    [DCM] = ratmpi_dcm_invert(id, mid, vbsv, options);
end

end

