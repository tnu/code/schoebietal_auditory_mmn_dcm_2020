function [] = ratmpi_revision_theta_export4R(design)
% [] = ratmpi_revision_theta_export4R(design)
% 
% Based on ratmpi_2ndLevel_theta_export4R
% Exports the BMAs to be later used in R.
%
% IN
%   design  string      MMN01, MMN02
%   
%   OPTIONAL
%
% OUT
% 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% This doesn't have any effect since this part of options will be unused
hemi = 'lhs'; 

% Return predefined global options
[options, optionsHandle] = ratmpi_return_global_options(design, hemi); 

% Export the data
ratmpi_2ndLevel_mfx_theta_export4R(options);

end