function [DCM] = ratmpi_revision_load_winning_model(id, mid, options)
% [ DCM ] = ratmpi_revision_load_winning_model(id, mid, options)
% 
% Loads the winning model, i.e. the model corresponding to starting values
% resulting in the best (negative) free energy. Only useful if the model
% have actually been computed using the multistart approach.
% Note: 
% This is intended for code revision, where the multistart Summary file is
% not available.
% 
% IN
%   id           string           Subject identifier (e.g. '0001') 
%   mid          string/struct    Model identifier or object
%   
%   OPTIONAL:
%   options      struct           Configuration structure as returned
%                                 by ratmpi_set_global_options()
% 
% OUT
%   DCM                 struct           DCM structure
%   path2winningModel   path             Path to file
%
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options(); 
end

vbsv = ratmpi_revision_return_winning_sv(id, mid, options); 

m = struct('mid', mid, 'vbsv', vbsv); 

details = ratmpi_subjects(id, m, options);

load(details.file.dcm, 'DCM'); 


end

