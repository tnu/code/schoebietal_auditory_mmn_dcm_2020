function [] = ratmpi_revision_preproc_diagnostics(design)
% [] = ratmpi_revision_preproc_diagnostics(design)
% 
% Based on ratmpi_diagnostics_2ndLevel_preprocReview.
%
% IN
%   id      string      Subject Identifier (e.g. '0000')
%   design  string      MMN01, MMN02
%   mid     string      Model Identifier (e.g. '16')
%   vbsv    string      starting value identifier (e.g. '0')
%   
%   OPTIONAL
%
% OUT
% 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

% This doesn't have any effect since this part of options will be unused
hemi = 'lhs'; 

% Return predefined global options
[options, optionsHandle] = ratmpi_return_global_options(design, hemi); 

% Run the preproc diagnostics
ratmpi_diagnostics_2ndLevel_preprocReview('group_pharma', 1, options);

end