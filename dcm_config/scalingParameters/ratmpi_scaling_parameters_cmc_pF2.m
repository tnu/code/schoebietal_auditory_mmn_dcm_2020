function [pF] = ratmpi_scaling_parameters_cmc_pF2()

pF = struct(); 
pF.G  = [4 4 8 4 4 2 4 4 2 1]*200;   % intrinsic connections
pF.T  = [2 2 16 28];                 % synaptic time constants
pF.D  = [1 1]; 
pF.E  = [10 1/2 1 1/2]*200;           % extrinsic (forward and backward)
pF.R  = 2/3;                         % slope of sigmoid activation function
pF.B  = 0;                           % deviation from baseline firing

end