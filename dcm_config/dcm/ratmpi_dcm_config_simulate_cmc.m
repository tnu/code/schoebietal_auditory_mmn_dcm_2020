function [ simulate ] = ratmpi_dcm_config_simulate_cmc()
% [ simulate ] = ratmpi_dcm_config_simulate_cmc(id, m, options)
% 
% Configures the neuronal model function handles of the DCM
%
% IN
%   
% OUT 
%       simulate      struct    function handles for the neuronal model
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

simulate = struct(); 

% Specify Generator of ERP
simulate.IS = 'mpdcm_gen_erp_host';

% Specify Integrator
simulate.int = 'mpdcm_int_dde_euler'; 

% Specify fx function
simulate.f = 'mpdcm_fx_cmc_dde_euler';

% Specify forward model
simulate.FS   = 'spm_fy_erp';
simulate.G    = 'ratmpi_lx_erp';

% Specify Input
simulate.U = 'ratmpi_erp_u';

% Specify noise model
simulate.nomo  = 'spm_Q';


end

