function [xU] = ratmpi_dcm_config_xU(id, m, options)
% [ xU ] = ratmpi_dcm_config_xU(id, m, options)
%
%   Configures the design of a DCM
%
%   INPUT:
%       id          string      Subject Identifier (e.g. '0000')
%       options     struct      Structure containing configurations
%   
%   OUTPUT: 
%       xU         struct      DCM.xU
% 
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________


if nargin < 3
    options = ratmpi_set_global_options(); 
end
[mid, vbsv, s] = ratmpi_unwrap_input(m, options);

% -- Prepare the xU structure ---------------------------------------
%   X
xU.X = [0 1]';
xU.name = {'MMN'};

end

