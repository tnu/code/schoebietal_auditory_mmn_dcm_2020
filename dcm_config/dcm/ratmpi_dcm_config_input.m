function [ DCM ] = ratmpi_dcm_config_input( DCM, options )
% [  ] = ratmpi_dcm_config_input(DCM, options)
% 
% Configures the driving input of a DCM
% 
% IN
%   DCM         struct           DCM structure
%
%   OPTIONAL:
% 
% OUT
%   argout       type
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 2
    options = ratmpi_set_global_options(); 
end

switch DCM.M.fu
    case 'ratmpi_erp_u'
        DCM.options.onset = 43.4; %15;%;3; %25 %3;
        DCM.options.dur = 1186.5; %300;%0.5; %300; %0.5;
    case 'ratmpi_erp_u_inverseGamma'
        DCM.options.onset = 3;%;3; %25 %3;
        DCM.options.dur = 0.5;%0.5; %300; %0.5;
    otherwise
        DCM.options.onset = 60;
        DCM.options.dur = 16;
end

end
