function [opt] = ratmpi_dcm_config_options(id, m, options)
% [ opt ] = ratmpi_dcm_config_options(id, m, options)
% 
% Prepares the options structure in DCM.options for the inversion.
%
% IN
%   id          string      Subject Identifier (e.g. '0000')
%   options     struct      Structure containing configurations
%   
% OUT 
%       opt         struct      options structure containing all the settings
%                               for the inversion
% [  ] = ratmpi_dcm_config_input(DCM, options)
% 
% Configures the driving input of a DCM
% 
% IN
%   DCM         struct           DCM structure
%
%   OPTIONAL:
% 
% OUT
%   argout       type
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options(); 
end

[mid, vbsv, s] = ratmpi_unwrap_input(m, options);

% -- Prepare the options structure ---------------------------------------
%   trials
%   analysis
%   model
%   spatial
%   Tdcm
%   Fdcm
%   Rft
%   onset
%   dur
%   Nmodes
%   h
%   han
%   D
%   lock
%   location
%   symmetry

opt = struct();
opt.trials      = [1, 2];
opt.analysis    = 'ERP';
opt.spatial     = 'LFP';
opt.Tdcm        = [1 250];
opt.Fdcm        = [4 48];
opt.Rft         = 5;
opt.onset       = [];
opt.dur         = [];
opt.Nmodes      = 2;
opt.D           = 1;
opt.lock        = 0;
opt.multiC      = 0;
opt.location    = 0;
opt.symmetry    = 0;
opt.symm        = 0;

switch options.dcm.arch
    case 'erp'
        opt.model = 'ERP';
        opt.han = 0;
        opt.h = 1;
    case 'cmc'
        opt.model = 'CMC';
        opt.han = 0;
        opt.h = 1;
    case 'erp_spm'
        opt.model = 'ERP';
        opt.han = 0;
        opt.h = 1;
    case 'cmc_spm'
        opt.model = 'CMC';
        opt.han = 0;
        opt.h = 1;
end

end

