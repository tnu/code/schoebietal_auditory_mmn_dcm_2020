function [M] = ratmpi_dcm_config_M(id, m, options)
% [ M ] = ratmpi_dcm_config_M(id, m, options)
%
% IN
%   id          string      Subject Identifier (e.g. '0000')
%   options     struct      Structure containing configurations
%
% OUT
%    M          struct      options structure containing all the settings
%                               for the inversion
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options();
end

[mid, vbsv, s] = ratmpi_unwrap_input(m, options);


switch options.dcm.simulate
    case 'cmcdde'
        simulate = ratmpi_dcm_config_simulate_cmc();
    case 'cmcdde_nomo'
       simulate = ratmpi_dcm_config_simulate_cmc_nomo();
    case 'cmcspm'
        simulate = ratmpi_dcm_config_simulate_cmcspm();
end

% -- Prepare M structure ---------------------------------------
M.f = simulate.f;

% Specify Generator of ERP
M.IS = simulate.IS;

% Specify Integrator
M.int = simulate.int;

% Specify forward model
M.FS   = simulate.FS;
M.G    = simulate.G;

% Specify input model
M.fu = simulate.U;

% Specify noise model
M.nomo = simulate.nomo;

% Specify number of interations
M.Nmax = options.dcm.optim.Nmax;

% Specify optimization parameters for adaptation in Levenberg-Marquard
% scheme
M.optim = options.dcm.optim;

% Load Priors
[M.pE, M.pC, M.hist.pX] = load_neuronal_priors(mid, options);
[M.gE, M.gC, M.hist.gX] = load_forward_priors(mid, options);
[M.hE, M.hC, M.hist.hX] = load_noise_priors(mid, options);

% Load Scaling Parameters
[M.pF, M.hist.pF] = load_scaling_parameters(mid, options);

% Load VB starting values
[M.P, M.hist.P] = load_vb_starting_values(m, options);

% Prep the model
M.md = prep_model(M, options); 


end


function [pE, pC, f] = load_neuronal_priors(mid, options)

f = ratmpi_return_priors(mid, options);

[pE, pC] = f();

end


function [gE, gC, f] = load_forward_priors(mid, options)


[~, f] = ratmpi_return_priors(mid, options);

[gE, gC] = f();

end


function [hE, hC, f] = load_noise_priors(mid, options)

[~, ~, f] = ratmpi_return_priors(mid, options);

[hE, hC] = f();

end


function [pF, f] = load_scaling_parameters(mid, options)

f = str2func(['ratmpi_scaling_parameters_' ...
    options.dcm.arch '_' options.dcm.pF]);

pF = f();

end


function [P, f] = load_vb_starting_values(m, options)

details = ratmpi_subjects('', m, options); 

if strcmp(options.dcm.optim.multistart, 'pE')
    P = [];
    f = '';
else
    try
        f = details.config.vbsv;
        
        P = load(f);
        P = P.P;
    catch
        P = [];
        f = 'failed loading starting values';
    end
    
end
end

function [ md ] = prep_model(M, options)

md = spm_unvec(spm_vec(M.pC) ~= 0, M.pC);  

end


