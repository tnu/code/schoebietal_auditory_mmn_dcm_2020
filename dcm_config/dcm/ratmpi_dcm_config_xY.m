function [xY] = ratmpi_dcm_config_xY(id, m, options)
% [xY] = ratmpi_dcm_config_xY(id, m, options)
%
% DCM.xY for the inversion of the ratmpi dataset. 
%
%   INPUT:
%       id          string          Subject Identifier (e.g. '0000')
%       m           string/struct   model identifier
%       options     struct          Structure containing configurations
%   
%   OUTPUT: 
%       xY         struct      options structure containing all the settings
%                               for the inversion
% _________________________________________________________________________
% Author: Dario Schöbi
% Copyright (C) 2020 TNU, Institute for Biomedical Engineering, University of Zurich and ETH Zurich.
%
% This file is released under the terms of the GNU General Public
% Licence (GPL), version 3. You can redistribute it and/or modify it under the terms of the GPL
% (either version 3 or, at your option, any later version). 
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
% _________________________________________________________________________

if nargin < 3
    options = ratmpi_set_global_options(); 
end

details = ratmpi_subjects(id, m, options); 

% -- Prepare the xY structure ---------------------------------------
%   Dfile
%   modality
%   name
%   code
xY = struct();
xY.Dfile    = details.file.prep;
xY.name     = {'A1', 'PAF'};
xY.code     = {'standard', 'deviant'};
xY.modality = 'LFP'; 


end

