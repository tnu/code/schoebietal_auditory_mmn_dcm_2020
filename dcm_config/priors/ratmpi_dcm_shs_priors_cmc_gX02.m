function [ gE, gC ] = ratmpi_dcm_shs_priors_cmc_gX02()
%Contains the forward priors (mean and variance for a particular model.

log0 = -32; % Defines the log zero prior, i.e. absence of a connection
scale1 = 1/8;
scale2 = 1/16;
scale3 = 1/32;
scale4 = 1/64;

% Forward Model Prior Means
gE = struct(); 
gE.Lpos = zeros(3, 0); 
gE.L = [-1 0]; 
gE.J = [0 0 4 0 0 0 2 0 ]; 

% Forward Model Prior Variance
gC = struct(); 
gC.Lpos = zeros(3, 0); 
gC.L = [64 scale1]; 
gC.J = scale3 * [0 0 0 0 0 0 1 0 ]; 


end