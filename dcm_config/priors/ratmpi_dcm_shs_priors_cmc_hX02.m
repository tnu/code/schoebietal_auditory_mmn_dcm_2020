function [hE, hC] = ratmpi_dcm_shs_priors_cmc_hX02()
%Contains the noise priors (mean and variance for a particular model.

hE = 4; 
hC = 1/2; 

end

