function [pE, pC] = ratmpi_dcm_m06_shs_priors_cmc_pX04()
%Contains the neural priors (mean and variance for a particular model).
                                                                   
log0 = -32; % Defines the log zero prior, i.e. absence of a connection
scale1 = 1/8;
scale2 = 1/16;
scale3 = 1/32;
scale4 = 1/64;

% Neuronal Prior Means
pE = struct;
pE.M = ...
  [0 0;
   0 0];
pE.A = cell(1, 4);
pE.A{1} = ...
  [log0 log0;
   1.2004 log0];
pE.A{2} = ...
  [log0 log0;
   -1.2733 log0];
pE.A{3} = ...
  [log0 -1.0845;
   log0 log0];
pE.A{4} = ...
  [log0 0.9206;
   log0 log0];
pE.B = cell(1, 1);
pE.B{1} = zeros(2, 2);
pE.N = cell(1, 1);
pE.N{1} = ...
  [0 0;
   0 0];
pE.C = [0.2351; log0];
pE.T = [-0.7969 0.2319 0.0256 0.2331];
pE.G = ...
  [-0.615 0.1045 -0.5926;
   0.349 -0.1418 -0.5631];
pE.D = ...
  [0 0.1667;
   0.1892 0];
pE.S = 1.23;
pE.R = [0.116 0.218];


% Neuronal Prior Variance
pC = struct;
pC.M = ...
  [0 0;
   0 0];
pC.A = cell(1, 4);
pC.A{1} = ...
  [0 0;
   0.417 0];
pC.A{2} = ...
  [0 0;
   0.054 0];
pC.A{3} = ...
  [0 0.2751;
   0 0];
pC.A{4} = ...
  [0 0.2406;
   0 0];
pC.B = cell(1, 1);
pC.B{1} = scale1 * [0 0; 0 1]; 
pC.N = cell(1, 1);
pC.N{1} = ...
  [0 0;
   0 0];
pC.C = [0.0869; 0];
pC.T = [0.0537 0.0643 0.1339 0.18];
pC.G = ...
  [0.1349 0.0528 0.3053;
   0.1255 0.0329 0.0862];
pC.D = ...
  [0 0.0119;
   0.029 0];
pC.S = 0.0987;
pC.R = [0.0077 0.2518];



end
