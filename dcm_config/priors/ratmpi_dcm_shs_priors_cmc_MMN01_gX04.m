function [ gE, gC ] = ratmpi_dcm_shs_priors_cmc_gX04()
%Contains the forward priors (mean and variance for a particular model.

log0 = -32; % Defines the log zero prior, i.e. absence of a connection
scale1 = 1/8;
scale2 = 1/16;
scale3 = 1/32;
scale4 = 1/64;

% Forward Model Prior Means
gE = struct(); 
gE.Lpos = zeros(3, 0); 
gE.L = [-6.6685 1.2378]; 
gE.J = [0 0 4 0 0 0 2.6704 0 ]; 

% Forward Model Prior Variance
gC = struct(); 
gC.Lpos = zeros(3, 0); 
gC.L = [5.0945 0.5269]; 
gC.J = [0 0 0 0 0 0 0.1059 0 ]; 


end