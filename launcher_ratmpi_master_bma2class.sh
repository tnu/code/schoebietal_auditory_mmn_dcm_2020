#! /bin/bash

set -e

# Load the matlab module
module load matlab

# Specify configuration
DESIGN=MMN01
DRUG_CLASS="2mgScopo_6mgPilo 2mgScopo_Vehicle Vehicle_6mgPilo anta_vs_ago all"

# Create path to logfiles
LOGPATH=log_ratmpi_master_bma2class_$DESIGN
mkdir logs/$LOGPATH 

# Run all classification problems, and the permutation test
for pharma in $DRUG_CLASS
do
	echo $pharma

	for permTest in {0..1000}
	do
		bsub -W 240 -J "job_class" -o logs/$LOPATH/"$LOGPATH"_"$pharma" matlab -nodisplay -singleCompThread -r "ratmpi_revision_class_nloso('$DESIGN', '$pharma', $permTest)"
	done
done

# Run the permutation test summary
for pharma in $DRUG_CLASS
do
	bsub -W 60 -w 'ended("job_class")' -o logs/$LOPATH/"$LOGPATH"_"$pharma" matlab -nodisplay -singleCompThread -r "ratmpi_revision_class_permutationTest_summary('$DESIGN', '$pharma')"
done

# Run the support vector discrimination code

bsub -W 60 -w 'ended("job_class")' -o logs/$LOPATH/"$LOGPATH"_"$pharma" matlab -nodisplay -singleCompThread -r "script_ratmpi_class_permTest_supportVectors"
