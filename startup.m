% Get the paths to SPM
SPMROOT = getenv('SPM_ROOT');

% Get the paths to RATMPI
RATMPIROOT = getenv('RATMPI_ROOT');

% Load SPM
addpath(SPMROOT);
spm('defaults', 'eeg');
spm_jobman('initcfg');
spm_get_defaults('cmdline', true);

% Add the required code of RATMPI
addpath(genpath(fullfile(RATMPIROOT, 'code'))); 
addpath(genpath(fullfile(RATMPIROOT, 'dcm_config', 'dcm'))); 
addpath(genpath(fullfile(RATMPIROOT, 'dcm_config', 'priors'))); 
addpath(genpath(fullfile(RATMPIROOT, 'dcm_config', 'scalingParameters'))); 

format shortG

