# schoebietal_auditory_mmn_dcm_2020

This repository contains the code to analyse the data published 

Schöbi et al. (2019); _Model-based prediction of muscarinic receptor function from auditory mismatch negativity responses_

https://doi.org/10.1101/2020.06.08.139550.
