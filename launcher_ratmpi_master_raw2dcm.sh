#! /bin/bash

set -e

# Load the matlab module
module load matlab

# Specify configuration 
ID=27905
DESIGN=MMN01


# Create path to logfiles
LOGPATH="log_ratmpi_master_raw2dcm_"$DESIGN"_m01_16"
mkdir logs/$LOGPATH 

# Iterate over subjects and run the inversion
echo "$ID"

# Run the preprocessing
bsub -W 120 -J "job_preproc" -o logs/$LOPATH/"$LOGPATH"_"$ID" matlab -nodisplay -singleCompThread -r "ratmpi_revision_preproc('$ID', '$DESIGN')"

# Run the DCM inversion for lhs
HEMI=lhs

for model in {01..16}
do 
	for vbsv in $(seq 0 99 )
	do
		bsub -W 240 -w 'ended("job_preproc")' -o logs/$LOGPATH/"$LOGPATH"_"$ID"_"$model" matlab -nodisplay -singleCompThread -r "ratmpi_revision_dcm_invert('$ID', '$DESIGN', '$HEMI', '$model','$vbsv')"
		sleep 1
	done
done


# Run the DCM inversion for rhs
HEMI=rhs

for model in {01..16}
do 
	for vbsv in $(seq 0 99 )
	do
		bsub -W 240 -w 'ended("job_preproc")' -o logs/$LOGPATH/"$LOGPATH"_"$ID"_"$model" matlab -nodisplay -singleCompThread -r "ratmpi_revision_dcm_invert('$ID', '$DESIGN', '$HEMI', '$model','$vbsv')"
		sleep 1
	done
done
